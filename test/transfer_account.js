const riskLimitOpsService = require("../services/RiskLimitOpsService");


let [_, program, account, assigned_new_account] = process.argv;
riskLimitOpsService.transfer_account(account, assigned_new_account).then(() => {
    console.log("done");
    process.exit(0);
});