const FutHelper = require("../services/UniFutSwitch");

let [_, program, account, assigned_account] = process.argv;
FutHelper.switch_account(account, assigned_account).then(() => {
    console.log("done");
    process.exit(0);
});