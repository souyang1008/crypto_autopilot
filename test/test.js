const {getRedis, promisifyRedis} = require("../store/redis");
const rClient = promisifyRedis(getRedis("app"));
const strategyOpsService = require("../services/StrategyOpsService");
const ApiConnector = require("../utils/api_connector");
const strategyRouterService = require("../services/StrategyRouterService");
const accountService = require("../services/AccountService");
const accShift = require("../services/McWorkShift/McfAccShifts");
const assetSwitch = require("../services/BgsMatchAccount/SwitchAssets");
const {get_keys, getRedisJson} = require("../utils/get_methods");
const {handLogger} = require("../services/LogService");

async function test() {
    try {

        let snaps = await get_keys(`CRYPTO_SNAPR#*#trbtf0*`).catch((err) => { handLogger.info(err)});
        for (let k_i = 0; k_i< snaps.length; k_i++) {
            let key = snaps[k_i];
            let snapshot = await getRedisJson(key);
            console.log(key, snapshot['turnover']);
        }
        console.log('res', snaps);
    }catch (e) {
        console.error("error", e);
    }
}

test().then(() => {
    console.log("done");
    process.exit(0);
});;