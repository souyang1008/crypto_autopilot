const strategyRuntimeSerive = require("../services/StrategyRuntimeService");
const {handLogger} = require("../services/LogService");
const accountService = require("../services/AccountService");
const stratLuciferService = require("../services/StrategyLuciferService");


let prdLine = 'MCS';

async function get_accs_stats(prdLine) {
    let candidates = []
    let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate();
    if (prdLine === 'BGS' || prdLine === 'MCS') {
        let tmp_candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
        let available_accs = accountService.get_available_accs(prdLine);
        for (let acc of tmp_candidates) {
            if (available_accs.includes(acc)) {
                candidates.push(acc)
            }
        }
    }
    let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
    let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
    candidates = await accountService.GetCandidateStatsSortedByVol(candidates, accDetail.ttnov);
}

get_accs_stats(prdLine);


