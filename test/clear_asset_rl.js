const stratLuciferService = require("../services/StrategyLuciferService");
const accountService = require("../services/AccountService");
const strategyAdvisorService = require("../services/StrategyAdvisorService");
const strategyRuntimeSerive = require("../services/StrategyRuntimeService");
const strategyOpsService = require("../services/StrategyOpsService");



async function clear_asset(prdLine, asset) {
    let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate()
    let symbol = asset+'USD'
    let candidates = candidateByRiskLimit[`${prdLine}-${symbol}`]
    let exchange = accountService.prdLine2Exchange(prdLine);
    let total = 0
    for (let account of candidates) {
        let curr_limit = await stratLuciferService.GetRiskLimit(account, asset);
        await strategyOpsService.AddPassthrough(symbol, account, exchange)
        await stratLuciferService.ClearRiskLimit(account, asset);
        total += Number(curr_limit)/100
    }
    return total
}

async function clear_risk(account, assets) {
    assets = assets.split(',')
    let prdLine = account.replace(/\d/g, '');
    let exchange = accountService.prdLine2Exchange(prdLine);
    for (let asset of assets) {
        let symbol = asset+'USD';
        await stratLuciferService.ClearRiskLimit(account, asset);
        await strategyOpsService.AddPassthrough(symbol, account, exchange);
    }
}

// let [_, program, account, assets] = process.argv;
// clear_risk(account, assets).then(() => {
//     console.log("done");
//     process.exit(0);
// })
let [_, program, prdLine, asset] = process.argv;
clear_risk(prdLine, asset).then(res => {
    console.log("RL CLEARED: ", asset, res);
    console.log("done");
    process.exit(0);
})


