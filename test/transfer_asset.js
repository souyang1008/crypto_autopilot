const riskLimitOpsService = require("../services/RiskLimitOpsService");


let [_, program, account, asset, assigned_account] = process.argv;
if (asset.length !== 3) {
    console.log('Invalid Asset: ', asset);
    process.exit(0);
}
riskLimitOpsService.transfer_asset(account, asset, assigned_account).then(() => {
    console.log("done");
    process.exit(0);
});