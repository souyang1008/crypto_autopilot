const stratLuciferService = require("../services/StrategyLuciferService");


let [_, program, account, asset, new_limit, new_debt] = process.argv;
new_limit = Number(new_limit)
if (new_debt) {
    new_debt = new_limit
} else {
    new_debt = 0
}
stratLuciferService.AddRiskLimit(account, asset, new_limit, new_debt).then(() => {
    console.log("done");
    process.exit(0);
});