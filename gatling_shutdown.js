const strategyRuntimeSerive = require("./services/StrategyRuntimeService");
const strategyAdvisorService = require("./services/StrategyAdvisorService");
const strategyOpsService = require("./services/StrategyOpsService");
const stratLuciferService = require("./services/StrategyLuciferService");
const accountService = require("./services/AccountService");


async function shutdown_strats(prdLine, pnl_threshold, symbol) {
    console.log("Loading running stats");
    let {cumPnl, stratsBySymbol} = await strategyRuntimeSerive.fetchRunningStats(prdLine, symbol);

    for(let cp of cumPnl) {
        let strats = stratsBySymbol[cp.symbol];
        for(let strat of strats) {
            if(strat.pnl < pnl_threshold) {
                console.log("shutdown", cp.symbol, strat.account, strat.sname, strat.pnl);
                await strategyOpsService.ShutdownStrategies(cp.symbol, strat.account, strat.sname);
            }
        }
    }
}

let [_, program, prdline, thres, symbol] = process.argv;
shutdown_strats(prdline, Number(thres), symbol).then(() => {
    console.log("done");
    process.exit(0);
});
