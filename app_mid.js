const fs = require('fs');
const request = require('request');
const redis = require('redis');
const yaml = require('js-yaml');
const mailer = require('./utils/mail');
const {change_preference, change_dss} = require('./main/helper_functions');
const {update_strategies} = require('./main/mid_freq');
const {list_tops} = require('./main/rank_all');
const get_methods = require('./utils/get_methods');
const accShift = require("./services/McWorkShift/McfAccShifts");
const accountService = require("./services/AccountService");
const strategyRouterService = require("./services/StrategyRouterService");
const databaseConfig = JSON.parse(fs.readFileSync('config/database.json'));

const userConfig = JSON.parse(fs.readFileSync('config/user.json'));
const receps = JSON.parse(fs.readFileSync('config/mail.json'))
const receptions = receps['receivers'];

const redisConfig = databaseConfig['redis'];
const redisConn = redis.createClient({
    host: redisConfig['host'],
    port: redisConfig['port1']
});
const meta = yaml.safeLoad(fs.readFileSync('config/crypto_meta.yaml', 'utf8'));

async function mid_change(redisConn, userConfig, meta) {
    console.log('done')
    await update_strategies(redisConn, userConfig, meta)
}

function Scan() {
    setTimeout(function run() {
        let date = new Date();
        let hour = date.getHours()
        if (hour === 8 || hour === 21 || hour === 3) {
            mid_change(redisConn, userConfig, meta).then(() => {
                return list_tops(10);
            }).finally(() => {
                setTimeout(run, 3600*1000);
            })
        } else {
            setTimeout(run, 3600*1000);
        }
    }, 3600*1000);
}

async function setUp() {
    await accountService.LoadAllRecCfg();
    get_methods.load_avg_timer('default_size');
    // get_methods.load_avg_timer('avg_pnl');
    // strategyRouterService.loadAccountsTimer();
    // strategyRouterService.loadLauncherTimer();
    Scan();
    await list_tops(10);
    // await mid_change(redisConn, userConfig, meta);
    // process.exit(0);
}

setUp();