const strategyRuntimeSerive = require("./services/StrategyRuntimeService");
const strategyAdvisorService = require("./services/StrategyAdvisorService");
const strategyOpsService = require("./services/StrategyOpsService");
const stratLuciferService = require("./services/StrategyLuciferService");
const accountService = require("./services/AccountService");


async function switch_strategies(prdLine, symbol, pnl_threshold, tom, policy) {
    console.log("Loading running stats");
    let {cumPnl, stratsBySymbol} = await strategyRuntimeSerive.fetchRunningStats(prdLine, symbol);
    let exchange = accountService.prdLine2Exchange(prdLine);

    for(let cp of cumPnl) {
        let strats = stratsBySymbol[cp.symbol];
        let need_switch_strats = [];
        for(let strat of strats) {
            if(strat.pnl < pnl_threshold) {
                console.log("switch", cp.symbol, strat.account, strat.sname, strat.pnl);
                need_switch_strats.push(strat);
            }
        }
        let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
        let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
        await strategyOpsService.SwitchStrategies(cp.symbol, need_switch_strats, pb_ranks[`${tom}_${policy}_sorted`], Math.round(ap_size * 0.3));
    }

}


//tom: taker / maker
//policy: rank / batch / pure
let [_, program, prdline, symbol, pnl_thres, tom, policy] = process.argv;
switch_strategies(prdline, symbol, Number(pnl_thres), tom, policy).then(() => {
    console.log("done");
    process.exit(0);
});
