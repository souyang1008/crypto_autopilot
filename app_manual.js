const fs = require('fs');
const redis = require('redis');
const {apLogger} = require('./services/LogService');
const {change_preference, change_dss} = require('./main/helper_functions');
const databaseConfig = JSON.parse(fs.readFileSync('config/database.json'));
const {get_num_accs} = require('./utils/get_methods');
const {detect_active} = require('./main/detect_active')

const mailer = require('./utils/mail');
const userConfig = JSON.parse(fs.readFileSync('config/user.json'));
const receps = JSON.parse(fs.readFileSync('config/mail.json'))
const receptions = receps['receivers'];
const redisConfig = databaseConfig['redis'];
const redisConn = redis.createClient({
    host: redisConfig['host'],
    port: redisConfig['port1']
});


async function hourly_detect(redisConn, userConfig) {
    const res = detect_active(redisConn, userConfig)
    res.then(info => {
        const subject = '[CRYPTO_AP] HOURLY DETECT';
        console.log('res', info, subject, receptions)
        mailer(receptions, subject, info)
    })
}

hourly_detect(redisConn, userConfig)

setInterval(function() {hourly_detect(redisConn, userConfig)}, 60*60*1000);


// get_num_accs(redisConn, 'BNBF', 'btcf0', apLogger).then(res => {
//     console.log(res)
// })
// change_preference(redisConn, 'GTF', 'all', 'm').then(res => {
//     console.log(res)
// }).catch(err => {
//     console.error(err)
// });
// change_dss(redisConn, 'HBF', 'all', 500)

