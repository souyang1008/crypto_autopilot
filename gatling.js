const strategyRuntimeSerive = require("./services/StrategyRuntimeService");
const strategyAdvisorService = require("./services/StrategyAdvisorService");
const strategyOpsService = require("./services/StrategyOpsService");
const symbolClassifyService = require("./services/SymbolClassifyService");
const stratLuciferService = require("./services/StrategyLuciferService");
const accountService = require("./services/AccountService");


let MAX_STRAT_PER_SYMBOL = 6;
let MIN_PNL_TO_SWITCH = -20;
let MIN_PNL_TO_SHUTDOWN = -20;




async function fire(prdLine) {
    console.log("Fetching open stats");
    await strategyAdvisorService.UpdateOpenStats();
    console.log("Loading running stats");
    let stratByAccount = await stratLuciferService.LoadAllRunningStrategy();

    let exchange = accountService.prdLine2Exchange(prdLine);
    let openStatList = strategyAdvisorService.GetOpenStatsByPrdLine(prdLine);
    let prevCumPnlset = await strategyRuntimeSerive.getCumPnl(prdLine);
    let {cumPnl, stratsBySymbol} = await strategyRuntimeSerive.fetchRunningStats(prdLine);
    let accStatsBySymbol = await strategyRuntimeSerive.fetchAccStats(prdLine);
    let [candiatesforCoinbased, candidateByRiskLimit] = [[], []]
    if (prdLine === 'BNBC') {
        candiatesforCoinbased = await accountService.GetCandiatesforCoinbased(prdLine);
    } else {
        candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate();
    }
    let result = symbolClassifyService.ClassifySymbols(cumPnl, openStatList, accStatsBySymbol, prevCumPnlset);

    console.log(result);
    for(let symbol in result.active_win) {
        let candidates = await accountService.GetAccCandidatesForAllTypes(prdLine, symbol, candiatesforCoinbased, candidateByRiskLimit);
        let stats = accountService.GetCandidateStats(candidates, stratByAccount);
        let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
        let pnl_list = await strategyAdvisorService.GetStrategyByPnl(exchange, symbol);
        if(stratsBySymbol[symbol]) {
            let pref = stratsBySymbol[symbol][0].pref === 't' ? 'taker_rank_sorted' : 'maker_rank_sorted';
            if(stratsBySymbol[symbol].length < MAX_STRAT_PER_SYMBOL) {
                console.log(symbol, "add more strats");
                let add_num = MAX_STRAT_PER_SYMBOL - stratsBySymbol[symbol].length;
                let ap_size = stratsBySymbol[symbol][0].apsize;
                let split = Math.ceil(add_num / 2);
                let [leftover, acc_looped] = await strategyOpsService.AddMoreStrategiesByPnl(symbol, stats, split, pnl_list, exchange, ap_size);
                console.log('acc_looped', acc_looped, 'prev_add', split, 'leftover', leftover, 'add_num', add_num, 'next_adds', add_num - split + leftover)
                leftover = add_num - split + leftover
                leftover = await strategyOpsService.AddMoreStrategies(symbol, stats.slice(acc_looped,), leftover, pb_ranks[pref], exchange, ap_size);
                if (leftover > 0 ) {
                    console.log(symbol, `in active win has ${leftover} strategies needed to be added`)
                }
            }
            let pending_accounts = [];
            for(let strat of stratsBySymbol[symbol]) {
                if(strat.pnl < MIN_PNL_TO_SWITCH) {
                    console.log(strat.account, symbol, "need switch better", strat.sname);
                    pending_accounts.push(strat);
                }
            }
            if(pending_accounts.length > 0) {
                let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
                let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
                let leftover = await strategyOpsService.SwitchStrategiesByPnl(symbol, pending_accounts, pnl_list, Math.round(ap_size * 0.3));
                if (leftover.length > 0 ) {
                    leftover = await strategyOpsService.SwitchStrategies(symbol, leftover, pb_ranks[pref], Math.round(ap_size * 0.3));
                }
                if (leftover.length > 0 ) {
                    console.log(symbol, `in active win has ${leftover.length} strategies needed to be switched`)
                }
            }
        }
    }

    for(let symbol in result.active_loss) {
        let pending_accounts = [];
        let pref = 'taker_rank_sorted';
        for(let strat of stratsBySymbol[symbol]) {
            pref = strat.pref === 't' ? 'taker_rank_sorted' : 'maker_rank_sorted';
            if(strat.pnl < MIN_PNL_TO_SWITCH) {
                console.log(strat.account, symbol, "need switch for loss", strat.sname);
                pending_accounts.push(strat);
            }
        }
        if(pending_accounts.length > 0) {
            let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
            let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
            let leftover = await strategyOpsService.SwitchStrategies(symbol, pending_accounts, pb_ranks[pref], Math.round(ap_size * 0.3));
            if (leftover.length > 0 ) {
                console.log(symbol, ` in active loss has ${leftover.length} strategies needed to be switched`)
            }
        }
    }

    for(let symbol in result.inactive_win) {
        let candidates = await accountService.GetAccCandidatesForAllTypes(prdLine, symbol, candiatesforCoinbased, candidateByRiskLimit);
        let stats = accountService.GetCandidateStats(candidates, stratByAccount);
        let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
        let pnl_list = await strategyAdvisorService.GetStrategyByPnl(exchange, symbol);
        if(stratsBySymbol[symbol].length < MAX_STRAT_PER_SYMBOL) {
            console.log(symbol, "add strats for slow win");
            let add_num = MAX_STRAT_PER_SYMBOL - stratsBySymbol[symbol].length;
            let ap_size = stratsBySymbol[symbol][0].apsize;
            let pref = stratsBySymbol[symbol][0].pref === 't' ? 'taker_rank_sorted' : 'maker_rank_sorted';
            let split = Math.ceil(add_num / 2);
            let [leftover, acc_looped] = await strategyOpsService.AddMoreStrategiesByPnl(symbol, stats, split, pnl_list, exchange, ap_size);
            console.log('acc_looped', acc_looped, 'prev_add', split, 'leftover', leftover, 'add_num', add_num, 'next_adds', add_num - split + leftover)
            leftover = add_num - split + leftover
            leftover = await strategyOpsService.AddMoreStrategies(symbol, stats.slice(acc_looped,), leftover, pb_ranks[pref], exchange, ap_size);
            if (leftover > 0 ) {
                console.log(symbol, `in inactive win has ${leftover} strategies needed to be added`)
            }
        }
    }

    for(let symbol in result.inactive_loss) {
        if(stratsBySymbol[symbol]) {
            for(let strat of stratsBySymbol[symbol]) {
                if(strat.pnl < MIN_PNL_TO_SHUTDOWN) {
                    console.log(strat.account, symbol, "need shutdown for loss", strat.sname);
                    await strategyOpsService.ShutdownStrategies(symbol, strat.account, strat.sname);
                }
            }
        }
    }

    for(let symbol in result.recent_active) {
        if(!stratsBySymbol[symbol]) {
            let candidates = await accountService.GetAccCandidatesForAllTypes(prdLine, symbol, candiatesforCoinbased, candidateByRiskLimit);
            let stats = accountService.GetCandidateStats(candidates, stratByAccount);
            let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
            console.log(symbol, "add strats for recent active");
            let add_num = 2;
            let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
            let pref = await strategyRuntimeSerive.getPreference(prdLine, symbol);
            pref = pref === 't' ? 'taker_rank_sorted' : 'maker_rank_sorted';
            let leftover = await strategyOpsService.AddMoreStrategies(symbol, stats, add_num, pb_ranks[pref], exchange, ap_size);
            if (leftover > 0 ) {
                console.log(symbol, `in recent active has ${leftover} strategies needed to be added`)
            }
        }
    }
    for (let symbol in result.need_remove) {
        if(stratsBySymbol[symbol]) {
            let strats = stratsBySymbol[symbol];
            let num_remove = result.need_remove[symbol]['num_remove'];
            for(let strat of strats) {
                if(strat.size === 0 && Date.now() - strat.shutdown_time > 10 * 60 * 1000 && Number(strat.pnl) < 0 && num_remove > 0) {
                    console.log("remove", symbol, strat.account, strat.sname, strat.pnl, strat.size, new Date(strat.shutdown_time).toISOString());
                    await strategyOpsService.RemoveStrategy(symbol, strat.account); //'sid
                    num_remove -= 1;
                }
            }
        }
    }
}

for (let prdLine of ['BNBF', 'GTF', 'BYBF', 'KCF', 'HB']) {
    if (prdLine !== 'GTF') {
        continue;
    }

    fire(prdLine).then((data) => {
        console.log("done", prdLine);
        process.exit(0);
    });
    // process.exit(0);
}

