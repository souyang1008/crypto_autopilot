const strategyRuntimeSerive = require("./services/StrategyRuntimeService");
const strategyOpsService = require("./services/StrategyOpsService");


async function remove_strats(prdLine, symbol) {
    console.log("Loading running stats");
    let {cumPnl, stratsBySymbol} = await strategyRuntimeSerive.fetchRunningStats(prdLine, symbol);

    for(let cp of cumPnl) {
        let strats = stratsBySymbol[cp.symbol];
        for(let strat of strats) {
            if(strat.size === 0 && Date.now() - strat.shutdown_time > 10 * 60 * 1000) {
                console.log("remove", cp.symbol, strat.account, strat.sname, strat.pnl, strat.size, new Date(strat.shutdown_time).toISOString());
                await strategyOpsService.RemoveStrategy(cp.symbol, strat.account); //'sid
            }
        }
    }
}

let [_, program, prdline, symbol] = process.argv;
remove_strats(prdline, symbol).then(() => {
    console.log("done");
    process.exit(0);
});
