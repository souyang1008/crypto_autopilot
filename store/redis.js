const redis = require('redis');
const {promisify} = require("util");

const configs = {
    'raw': {
        host: '127.0.0.1',
        port: 6379,
        db: 0
    },
    'app': {
        host: '127.0.0.1',
        port: 26380,
        db: 0
    }
};

const instances = {};

function getRedis(name, clientKey=undefined) {
    if(!configs[name]) {
        return
    }
    let instance_key = name;
    if(clientKey) {
        instance_key = clientKey;
    }
    if(!instances[instance_key]) {
        instances[instance_key] = redis.createClient({
            host: configs[name]['host'],
            port: configs[name]['port'],
            db: configs[name]['db'],
            retry_strategy: function (options) {
                if (options.error && options.error.code === 'ECONNREFUSED') {
                    // End reconnecting on a specific error and flush all commands with
                    // a individual error
                    return new Error('The server refused the connection');
                }
                if (options.total_retry_time > 1000 * 60 * 2) {
                    // End reconnecting after a specific timeout and flush all commands
                    // with a individual error
                    return new Error('Retry time exhausted');
                }
                if (options.attempt > 5) {
                    // End reconnecting with built in error
                    return new Error('Retry attemp exhausted');
                }
                // reconnect after
                return Math.min(options.attempt * 100, 3000);
            }
        });
        instances[instance_key].on('ready', function () {
            console.info('REDIS_READY@' + instance_key);
        });
        instances[instance_key].on('error', function (err) {
            console.error('REDIS_CLIENT@' + instance_key, err);
        });
        instances[instance_key].on('reconnecting', function (obj) {
            console.info('REDIS_RECONECt@' + instance_key, obj);
        });

    }
    return instances[instance_key];
}

function promisifyRedis(client) {
    return {
        existAsync: promisify(client.exists).bind(client),
        getAsync: promisify(client.get).bind(client),
        setAsync: promisify(client.set).bind(client),
        keysAsync: promisify(client.keys).bind(client),
        hmsetAsync: promisify(client.hmset).bind(client),
        hgetallAsync: promisify(client.hgetall).bind(client),
        saddAsync: promisify(client.sadd).bind(client),
        sismemberAsync: promisify(client.sismember).bind(client),
        smembersAsync: promisify(client.smembers).bind(client)
    }
}

module.exports = {getRedis, promisifyRedis};
