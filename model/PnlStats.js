class PnlStats{
    cumPnl = {};
    cnt = {};
    constructor() {

    }

    Update(symbol, pnl) {
        if(this.cumPnl[symbol]===undefined) {
            this.cumPnl[symbol] = 0;
            this.cnt[symbol] = 0;
        }
        this.cumPnl[symbol] += pnl;
        this.cnt[symbol] += 1
    }

    GetPnlForPrevSave() {
        let prevPnl = JSON.stringify(this.cumPnl)
        prevPnl = JSON.parse(prevPnl)
        prevPnl['time'] = Date.now()
        return prevPnl
    }

    GetSortedCumPnl(){
        let array = [];
        for(let symbol in this.cumPnl) {
            array.push({
                symbol,
                cumPnl: this.cumPnl[symbol],
                avgPnl: this.cumPnl[symbol]/this.cnt[symbol]
            });
        }
        array = array.sort((a, b) => b.cumPnl - a.cumPnl);
        return array;
    }

}

module.exports = PnlStats;