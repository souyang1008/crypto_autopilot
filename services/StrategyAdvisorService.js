const ApiConnector = require("../utils/api_connector");

let open_stats = {};

async function UpdateOpenStats() {
    try {
         open_stats = await ApiConnector.fetch_open_stats();
    } catch (e) {
        console.error("UpdateOpenStats", e);
    }
}

function GetOpenStatsByPrdLine(prdline) {
    return open_stats[prdline];
}


async function GetStrategyByPbRank(exchange, symbol) {
    try{
        return await ApiConnector.get_strategy_by_pb(exchange.toLowerCase(), symbol);
    } catch (e) {
        console.log("GetStrategyByPbRank", e);
    }
}

async function GetStrategyByPnl(exchange, symbol) {
    try{
        return await ApiConnector.get_strategy_by_pnl(exchange.toLowerCase(), symbol);
    } catch (e) {
        console.log("GetStrategyByPnl", e);
    }
}

module.exports = {
    GetOpenStatsByPrdLine,
    UpdateOpenStats,
    GetStrategyByPbRank,
    GetStrategyByPnl
};


