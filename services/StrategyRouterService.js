const fs = require("fs");
const path = require("path");
const lineByLine = require("n-readlines");
const ApiConnector = require("../utils/api_connector");

const SYNC_DIR = path.join("/home/crypto", "lucifer", "sync");
const AEG_LAUNCHER = "launcher.aeg";
const AEG_LINE_DELIMITER = "|";
const AEG_COMMENT = "#";

let accMeta = {};
let accLauncherStatus = {};

function isDir(filepath) {
    return fs.statSync(filepath).isDirectory()
}

function walkDir(dir, fileList) {
    let files = fs.readdirSync(dir);
    fileList = fileList || [];
    files.forEach( file => {
        let filepath = path.join(dir, file);
        if(isDir(filepath)){
            walkDir(filepath, fileList)
        }else{
            fileList.push({dir, file, filepath})
        }
    });
    return fileList
}
function loadLauncherStatus() {
    let accLauncherStatus_tmp = {};
    let launcherFiles = walkDir(SYNC_DIR).filter(file => file.filepath.endsWith(AEG_LAUNCHER));
    for(let file of launcherFiles){
        let launcherFile = fs.readFileSync(file.filepath).toString().split('\n').map(line => line.trim()).filter(line => !line.startsWith(AEG_COMMENT) && line.length !== 0);
        for(let line of launcherFile) {
            let fields = line.split(AEG_LINE_DELIMITER).map(f => f.trim().toUpperCase());
            let account = fields[0];
            let ap_allowed = Number(fields[7]) > 10;
            if (ap_allowed) {
                accLauncherStatus_tmp[account] = fields[2];
            }
        }
    }
    accLauncherStatus = accLauncherStatus_tmp;
}

async function update_accounts() {
    try {
        accMeta = await ApiConnector.fetch_accounts();
    } catch (e) {
        console.error("UpdateOpenStats", e);
    }
}

function update_account_launcher(account, status) {
    accLauncherStatus[account] = status
}

function get_members(account) {
    if (account in accMeta) {
        let info = accMeta[account]
        if (info["router"]) {
            let router = info["router"]
            return accMeta[router]? accMeta[router]["members"] : []
        }
    }
    return []
}

async function is_router(account) {
    if (Object.keys(accMeta).length === 0) {
        console.log('fetch router info1');
        let fetch = await update_accounts();
    }
    if (account in accMeta) {
        if (accMeta[account]['type'] === 'router') {
            return true
        }
    }
    return false
}

async function has_router(account) {
    if (Object.keys(accMeta).length === 0) {
        console.log('fetch router info2');
        let fetch = await update_accounts();
    }
    if (account in accMeta) {
        if (accMeta[account]["router"]) {
            return true
        }
    }
    return false
}

function get_router(account) {
    if (account in accMeta) {
        if (accMeta[account]["router"]) {
            return accMeta[account]["router"]
        }
    }
    return undefined
}

function get_launcher_status_from_lucifer(account) {
    if (account in accLauncherStatus && accLauncherStatus[account] !== 'LOCK') {
        return accLauncherStatus[account] === 'BAN'
    } else {
        return null
    }

}

function loadAccountsTimer() {
    update_accounts();
    setInterval(update_accounts, 12*60*60*1000)
}

function loadLauncherTimer() {
    loadLauncherStatus();
    setInterval(loadLauncherStatus, 30*60*1000)
}


module.exports = {
    get_launcher_status_from_lucifer,
    get_router,
    has_router,
    is_router,
    get_members,
    loadLauncherTimer,
    loadAccountsTimer,
    loadLauncherStatus,
    update_account_launcher
};