const log4js = require('log4js');
const workDir = "/data/crypto_autopilot_logs";
log4js.configure({
    appenders: {
        app: {
            "type": 'dateFile',
            "filename": workDir + '/app',
            "pattern": "yyyy-MM-dd_hh.log",
            "encoding": "utf-8",
            "mode": 0o660,
            "alwaysIncludePattern": true,
            "numBackups": 48,
            "layout": {
                "type": "pattern",
                "pattern": "[%d{ISO8601}][%5p %f{1} %l] %m"
            },
        },
        ap: {
            "type": 'dateFile',
            "filename": workDir + "/ap",
            "pattern": "yyyy-MM-dd_hh.log",
            "encoding": "utf-8",
            "mode": 0o660,
            "alwaysIncludePattern": true,
            "numBackups": 48,
            "layout": {
                "type": "pattern",
                "pattern": "[%d{ISO8601}][%5p %f{1} %l] %m"
            },
        },
        mid: {
            "type": 'dateFile',
            "filename": workDir + "/mid",
            "pattern": "yyyy-MM-dd_hh.log",
            "encoding": "utf-8",
            "mode": 0o660,
            "alwaysIncludePattern": true,
            "numBackups": 48,
            "layout": {
                "type": "pattern",
                "pattern": "[%d{ISO8601}][%5p %f{1} %l] %m"
            },
        },
        hand: {
            "type": 'dateFile',
            "filename": workDir + "/hand",
            "pattern": "yyyy-MM-dd_hh.log",
            "encoding": "utf-8",
            "mode": 0o660,
            "alwaysIncludePattern": true,
            "numBackups": 48,
            "layout": {
                "type": "pattern",
                "pattern": "[%d{ISO8601}][%5p %f{1} %l] %m"
            },
        }
    },
    categories: {
        default: {
            appenders:['app'],
            level: 'info',
            enableCallStack: true
        },
        ap: {
            appenders:['ap'],
            level: 'info',
            enableCallStack: true
        },
        mid: {
            appenders:['mid'],
            level: 'info',
            enableCallStack: true
        },
        hand: {
            appenders:['hand'],
            level: 'info',
            enableCallStack: true
        }
    }
});

const appLogger = log4js.getLogger();
const apLogger = log4js.getLogger('ap');
const midLogger = log4js.getLogger('mid');
const handLogger = log4js.getLogger('hand');

module.exports = {
    appLogger,
    apLogger,
    midLogger,
    handLogger
};