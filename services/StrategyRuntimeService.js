const {getRedis, promisifyRedis} = require("../store/redis");
const rClient = promisifyRedis(getRedis("app"));
const rClient_raw = promisifyRedis(getRedis("raw"));
const AccountService = require("./AccountService");
const PnlStats = require("../model/PnlStats");



async function fetchRunningStats(prdline, symbol=undefined) {
    let regex = `CRYPTO_SNAPSHOT#${prdline}[0-9]*#*`;
    if(symbol) {
        regex = `CRYPTO_SNAPSHOT#${prdline}[0-9]*#${symbol}*`;
    }
    let all_list = await rClient.keysAsync(regex);
    let pnl_stats = new PnlStats();
    let stratsBySymbol = {};
    let pref = null
    for(let k of all_list) {
        let snap = JSON.parse(await rClient.getAsync(k));
        if(AccountService.IsMktMakerAcc(snap.account) || k.includes('NotFound')) {
            continue;
        }

        if(snap.enabled) {
            let symbol = snap.symCont;
            let sname = snap.strategy;
            if (!pref) {
                pref = await getPreference(prdline, symbol)
            }
            let apsize = snap.size_rec;
            let size = snap.size;
            let shutdown_time = snap.shutdown_time || 0;
            let pnl = 0;
            if(snap.is_fut) {
                pnl = snap.netUsdt;
            } else {
                pnl = snap.net;
            }
            pnl_stats.Update(symbol, pnl);

            if(!stratsBySymbol[symbol]) {
                stratsBySymbol[symbol] = []
            }
            stratsBySymbol[symbol].push({
                sid: snap.sid,
                account: snap.account,
                symbol,
                sname,
                apsize,
                pref,
                pnl,
                size,
                shutdown_time
            })
        }
    }
    if (!symbol) {
        await rClient.setAsync(`CRYPTO_CUMPNL#${prdline}`, JSON.stringify(pnl_stats.GetPnlForPrevSave()));
    }
    return {
        cumPnl: pnl_stats.GetSortedCumPnl(),
        stratsBySymbol
    };
}

async function getAllRanks(){
    let ranks = {}
    let all_rkeys = await rClient_raw.keysAsync(`CRYPTO_PLAYBACK_RANK#*`);
    for (let key of all_rkeys){
        let [prdline, symbol] = [key.split('#')[1], key.split('#')[2]]
        let rank_info = await rClient_raw.getAsync(key);
        if (rank_info) {
            if ( !(prdline in ranks) ) {
                ranks[prdline] = {};
            }
            ranks[prdline][symbol] = JSON.parse(rank_info)
        }
    }
    return ranks
}

async function getPreference(prdline, symbol){
    let config = await rClient.getAsync(`AP_REC#${prdline}#${symbol}`);
    if(config) {
        return JSON.parse(config)[1]["preference"];
    } else {
        return 't';
    }
}


async function getApSize(prdline, symbol){
    let config = await rClient.getAsync(`AP_REC#${prdline}#${symbol}`);
    if(config) {
        return JSON.parse(config)[1]["usual"];
    } else {
        return 1;
    }
}

async function getCumPnl(prdline) {
    let cumpnl =  await rClient.getAsync(`CRYPTO_CUMPNL#${prdline}`);
    if (cumpnl) {
        let prevPnl = JSON.parse(cumpnl)
        console.log(prevPnl)
        return prevPnl
    } else {
        return {}
    }
}

async function getLatestPnl() {
    let pnl_summary =  await rClient.getAsync('CRYPTO_PNL');
    if (pnl_summary) {
        return JSON.parse(pnl_summary)
    } else {
        console.log('GET CRYPTO_PNL ERROR')
        return {}
    }
}

async function snapExists(snap_key) {
    return await rClient.existAsync(snap_key);
}

function fetchAccSummary(summary, ex) {
    let prdline = AccountService.acc2guiExchange(ex);
    let accDetail = {}
    for (let acc in summary['account_detail'][prdline]) {
        let info = summary['account_detail'][prdline][acc]
        for (let k in info) {
            accDetail[k] ? accDetail[k][acc] = info[k] : accDetail[k] = {}; accDetail[k][acc] = info[k]
        }
    }
    if (accDetail) {
        return accDetail
    } else {
        console.log('GET accDetail ERROR')
        return {}
    }
}


async function getStrats(prdline) {
    return await rClient.keysAsync(`CRYPTO_SNAPSHOT#${prdline}[0-9]*#*`);
}

async function fetchAccStats(prdline){
    if(prdline === "BNB") {
        prdline += "6"
    } else if(prdline === "HB") {
        prdline += "1"
    } else if(prdline === "OKC") {
        prdline += "5"
    } else if(prdline === "GT") {
        prdline += "1"
    }
    let strats = await rClient.keysAsync(`CRYPTO_SNAPSHOT#${prdline}*#*`);
    let stratsBySymbol = {};
    for (let key of strats) {
        let splits = key.split('#');
        let [acc, symbol, sname] = [splits[1], splits[2], splits[3]];
        if (key.endsWith("NotFound00000") || AccountService.IsMktMakerAcc(acc)) {
            continue;
        }
        if(!stratsBySymbol[symbol]) {
            stratsBySymbol[symbol] = {
                'active_cnt': 0,
                'inactive_cnt': 0,
                'off_strategies': 0,
                'last_strategy_offtime': 0,
                'inactive_info': [],
            };
        }
        let snapshot = JSON.parse(await rClient.getAsync(key));
        if (snapshot.enabled) {
            let net = snapshot.is_fut ? snapshot.netUsdt : snapshot.net;
            if (snapshot['size'] === 0 && parseInt(net) <= 0) {
                stratsBySymbol[symbol].inactive_cnt += 1;
                stratsBySymbol[symbol].inactive_info.push({'acc': acc, 'sname': sname});
            } else {
                stratsBySymbol[symbol].active_cnt += 1;
            }
        } else {
            stratsBySymbol[symbol].off_strategies += 1;
            let strategy_offtime = snapshot.is_fut ? Number(snapshot.updatedTime) : Number(snapshot.time);
            if (strategy_offtime > stratsBySymbol[symbol].last_strategy_offtime) {
                stratsBySymbol[symbol].last_strategy_offtime = strategy_offtime;
            }
        }
    }
    return stratsBySymbol
}


module.exports = {
    fetchRunningStats,
    fetchAccStats,
    fetchAccSummary,
    getApSize,
    getAllRanks,
    getStrats,
    getPreference,
    getCumPnl,
    getLatestPnl,
    snapExists,
};