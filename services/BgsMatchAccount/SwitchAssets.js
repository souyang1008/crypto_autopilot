const accountService = require("../AccountService");
const stratLuciferService = require("../StrategyLuciferService");
const strategyRuntimeSerive = require("../StrategyRuntimeService");
const {handLogger} = require("../LogService");
const strategyOpsService = require("../StrategyOpsService");
const ApiConnector = require("../../utils/api_connector");
const {promisifyRedis, getRedis} = require("../../store/redis");
const rClient = promisifyRedis(getRedis("app"));
const CRYPTO_ACCOUNT_ASSET_BL = 'CRYPTO_ACCOUNT_ASSET_BL'
const sensitive_assets = ['EDU', 'CFX', 'ID0', 'AGX', 'XRP', 'SI2', 'PPE', 'SOL', 'OP0', 'ARB']
const BL_RKEY_PREFIX = 'CRYPTO_ACCOUNT_BGBL_'
const SWITCHED_RKEY_PREFIX = 'CRYPTO_ACCOUNT_BGSW_'
function hasIntersect(arr1, arr2) {
    let set2 = new Set(arr2);
    let has =  arr1.some(element => set2.has(element));
    if (has) {
        return 1
    } else {
        return 0
    }
}

function getIntersect(arr1, arr2) {
    let set2 = new Set(arr2);
    return arr1.filter(element => set2.has(element));
}


async function is_sym_bl(sym, account) {
    return await rClient.sismemberAsync(BL_RKEY_PREFIX+sym, account);
}

async function is_sym_newly_switched(sym, account) {
    return await rClient.sismemberAsync(SWITCHED_RKEY_PREFIX+sym, account);
}

async function is_sym_fit(sym, this_account, to_account, toacc_asset_map) {
    let cond1 = await is_sym_newly_switched(sym, this_account);
    let cond2 = await is_sym_bl(sym, to_account);
    let co_exist = toacc_asset_map.includes(sym.slice(0,3));
    return !cond1 && !cond2 && !co_exist;
}

async function get_valid_accounts(prdLine, symbol, assets_accs, prdLine_accs, acc_asset_map, acc_rebate, riskcontrol){
    let accounts = [];
    let target_acc = assets_accs[`${prdLine}-${symbol}`]
    for (let acc of prdLine_accs) {
        if (['BGS703', 'BGS707', 'BGS405', 'BGS501', 'BGS507', 'BGS508', 'BGS1002'].includes(acc)) {
            continue;
        }
        if (!target_acc.includes(acc)  && riskcontrol[acc] !== true) {
            let is_bl = await is_sym_bl(symbol, acc)
            if (!is_bl) {
                accounts.push(acc);
            }
        }
    }
    let stats = [];
    for (let valid_acc of accounts) {
        let acc_has_asset = acc_asset_map[valid_acc];
        let preferred = hasIntersect(sensitive_assets, acc_has_asset);
        stats = stats.concat({valid_acc, preferred, tRebate: acc_rebate[valid_acc] || 0});
    }
    stats = stats.sort((a, b)=> b.preferred - a.preferred || a.tRebate - b.tRebate);
    return stats;

}
async function smart_asset_switch(account, asset) {
    let prdLine = account.replace(/\d/g, '');
    let exchange = accountService.prdLine2Exchange(prdLine);
    asset = asset.toUpperCase();
    let symbol = asset + 'USD'
    let [candidateByRiskLimit, acc_asset_map] = await stratLuciferService.LoadAllRiskLimitCandidate(true);
    let available_accs = accountService.get_available_accs(prdLine, account);
    let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
    let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
    let candidates_limited = await get_valid_accounts(prdLine, symbol, candidateByRiskLimit, available_accs, acc_asset_map, accDetail.tRbt, accDetail.riskcontrol);
    console.log('good_accs', JSON.stringify(candidates_limited));
    let success = false;
    for (let candidate of candidates_limited) {
        console.log(candidate);
        let new_account = candidate.valid_acc;
        let new_curr_assets = acc_asset_map[new_account] || [];
        let this_curr_assets = acc_asset_map[account] || [];
        let new_asset_candidates = getIntersect(sensitive_assets, new_curr_assets);
        let new_asset = undefined;
        for (let candidate of new_curr_assets) {
            let good = await is_sym_fit(candidate+'USD', new_account, account, this_curr_assets);
            if (good) {
                new_asset = candidate;
                break;
            }
        }
        for (let candidate of new_asset_candidates) {
            let good = await is_sym_fit(candidate+'USD', new_account, account, this_curr_assets);
            if (good) {
                new_asset = candidate;
                break;
            }
        }
        let rl1  = await stratLuciferService.SwitchRiskLimit(account, asset, new_account);
        if (rl1 <= 0) {
            console.log('error skip1', rl1, account, asset, new_account);
            continue;
        }
        await strategyOpsService.AddPassthrough(symbol, new_account, exchange);
        success = true;
        await rClient.saddAsync(BL_RKEY_PREFIX+symbol, account);
        let snap = await accountService.getAccountStrategies(account, false, symbol);
        if (snap) {
            let [sname, stype] = [snap.strategy, snap.stype];
            await strategyOpsService.ShutdownStrategies(symbol, account, sname, stype);
        } else {
            await strategyOpsService.AddPassthrough(symbol, account, exchange);
        }
        let rl2 = 0
        if (new_asset) {
            let new_symbol = new_asset+'USD'
            rl2 = await stratLuciferService.SwitchRiskLimit(new_account, new_asset, account);
            if (rl2 <= 0) {
                console.log('error skip1', rl2, new_account, new_asset, account);
            }
            await strategyOpsService.AddPassthrough(new_symbol, account, exchange);
            await rClient.saddAsync(BL_RKEY_PREFIX+new_symbol, new_account);
            let snap = await accountService.getAccountStrategies(new_account, false, new_symbol);
            if (snap) {
                let [sname, stype] = [snap.strategy, snap.stype];
                await strategyOpsService.ShutdownStrategies(new_symbol, new_account, sname, stype);
            } else {
                await strategyOpsService.AddPassthrough(new_symbol, new_account, exchange);
            }
        }
        if (success) {
            handLogger.info(`success:${Date()}, ${account}-${asset}, ${rl1}, ${new_account}-${new_asset}, ${rl2}`)
            break;
        }
    }
    return success;
    // try {
    //
    // } catch (err) {
    //     throw new Error("Error: " + err)
    // }
}


module.exports = {
    sensitive_assets,
    smart_asset_switch
}