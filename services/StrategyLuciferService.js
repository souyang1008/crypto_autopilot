const fs = require("fs");
const path = require("path");
const lineByLine = require("n-readlines");
const {getRedis, promisifyRedis} = require("../store/redis");
const ApiConnector = require("../utils/api_connector");

const SYNC_DIR = path.join("/home/crypto", "lucifer", "sync");
const AEG_LAUNCHER = "launcher.aeg";
const AEG_RISKLIMIT = "risk_limit.aeg";
const AEG_STRATEGY = "strategy.aeg";
const AEG_STRATEGY_MAP = "strategy_map.aeg";
const AEG_MISSILE_TYPE = 'missileMap.aeg';
const AEG_STRATEGY_MAP_PATH = path.join(SYNC_DIR, AEG_STRATEGY_MAP);
const AEG_MISSLE_TYPE_PATH = path.join(SYNC_DIR, AEG_MISSILE_TYPE);
const AEG_LINE_DELIMITER = "|";
const AEG_LINE_DELIMITER_W_SPACE = " | ";
const AEG_MAP_LINE_DELIMITER = "|";
const AEG_COMMENT = "#";

const rClient = promisifyRedis(getRedis("app"));

function isDir(filepath) {
    return fs.statSync(filepath).isDirectory()
}

function walkDir(dir, fileList) {
    let files = fs.readdirSync(dir);
    fileList = fileList || [];
    files.forEach( file => {
        let filepath = path.join(dir, file);
        if(isDir(filepath)){
            walkDir(filepath, fileList)
        }else{
            fileList.push({dir, file, filepath})
        }
    });
    return fileList
}

function parseMemo(memo) {
    let cols = memo.split(" ");
    let trigger = "UNKNOWN";
    if(cols.length > 2) {
        trigger = cols[2];
    }
    let mode = "";
    if (memo.indexOf('same_period') > -1) {
        mode = 'sp'
    } else if (memo.indexOf('no_spot') > -1) {
        mode = 'ns'
    }
    return {trigger, mode};
}


async function saveMap2Redis() {
    let liner = new lineByLine(AEG_STRATEGY_MAP_PATH);
    let line;
    while(line  = liner.next()) {
        line = line.toString();
        if(line.trim().length === 0 || line.trim().startsWith(AEG_COMMENT))
            continue;
        let fields = line.split(AEG_MAP_LINE_DELIMITER);
        let sid = fields[0].trim();
        let internalName = fields[1].trim();
        let deps = fields[3].trim();
        let sname = internalName.split("torch")[1];
        await rClient.setAsync(`CRYPTO_MAP#${sid}`, sname);
    }
}



let aeg_strategy_name_map = {};
async function mapStrategyId2Name(id) {
    let cacheKey = `${id}`;
    if(cacheKey.split("-").length > 1) {
        cacheKey = cacheKey.split("-")[1];
    }
    if(aeg_strategy_name_map[cacheKey]){
        return aeg_strategy_name_map[cacheKey]
    }
    if(id === "999") {
        return "PASSTHROUGH";
    }
    return await rClient.getAsync(`CRYPTO_MAP#${id}`);
}

async function parseStrategyLine(line) {
    let fields = line.split(AEG_LINE_DELIMITER).map(f => f.trim());
    let [id, type, deps, symbolExchange, account, size, memo] = fields;
    let {trigger, mode} = parseMemo(memo);
    let [symCont, exchange] = symbolExchange.split("-");
    if(size === ""){
        size = "0(empty)"
    }else {
        size = Number(size)
    }
    let isHedge = id.indexOf("-") > 0;
    let hid = "";
    if(isHedge) {
        hid = id.split("-")[0];
        id = id.split("-")[1]
    }
    let name = await mapStrategyId2Name(id);
    type = Number(type);
    return {
        id, type, name, size, deps, account, symCont, isHedge, hid, memo, exchange
    };
}


let StrategyByAccount = {};
async function LoadAllRunningStrategy() {
    let strategyByAccount = {};
    let launcherFiles = walkDir(SYNC_DIR).filter(file => file.filepath.endsWith(AEG_LAUNCHER));
    for(let file of launcherFiles){
        let content = fs.readFileSync(path.join(file.dir, AEG_STRATEGY)).toString().split('\n').map(line => line.trim()).filter(line => !line.startsWith(AEG_COMMENT) && line.length !== 0);

        let strateyList = [];
        for(let line of content) {
            strateyList.push(await parseStrategyLine(line));
        }

        let launcherFile = fs.readFileSync(file.filepath).toString().split('\n').map(line => line.trim()).filter(line => !line.startsWith(AEG_COMMENT) && line.length !== 0);
        for(let line of launcherFile) {
            let fields = line.split(AEG_LINE_DELIMITER).map(f => f.trim().toUpperCase());
            let account = fields[0];
            strategyByAccount[account] = [];
        }
        for(let s of strateyList) {
            if(strategyByAccount[s.account]) {
                strategyByAccount[s.account].push(s);
            }
        }
    }
    StrategyByAccount = strategyByAccount;
    return strategyByAccount
}

async function getAccountSymbolStrategyAeg(account, symbol) {
    let match_strats = []
    for (let s of StrategyByAccount[account] || []) {
        if (s.symCont === symbol) {
            match_strats.push({'sid': s.id, 'sname': s.name});
        }
    }
    return match_strats;
}

async function LoadAllRiskLimitCandidate(asset_map=false) {
    let candidateByRiskLimit = {};
    let acc_asset_map = {}
    let launcherFiles = walkDir(SYNC_DIR).filter(file => file.filepath.endsWith(AEG_RISKLIMIT));
    for(let file of launcherFiles){
        let content = fs.readFileSync(path.join(file.dir, AEG_RISKLIMIT)).toString().split('\n').map(line => line.trim()).filter(line => line.length !== 0);
        let acc_type = {}
        for (let line of content) {
            let fields = line.split(AEG_LINE_DELIMITER).map(f => f.trim().toUpperCase());
            let account = fields[0]
            if (line.includes('USD')) {
                acc_type[account] = 'USD'
                if (!account.startsWith('BGS') && !account.startsWith('MCS')) {
                    continue
                }
            } else if (line.includes('BUD')) {
                acc_type[account] = 'BUD'
                continue
            }
            let exchange = account.replace(/\d/g, '')
            let symbol = fields[1]
            let risklimit = Number(fields[2])
            if (risklimit > 0) {
                acc_asset_map[account] ? acc_asset_map[account].push(symbol) : acc_asset_map[account] = [symbol]
            }
            if (risklimit > 0 || account.startsWith('BGS') || account.startsWith('MCS')) {
                let key = `${exchange}-${symbol}${acc_type[account]}`
                if (key in candidateByRiskLimit) {
                    candidateByRiskLimit[key].push(account)
                } else {
                    candidateByRiskLimit[key] = [account]
                }
            }
        }
    }
    if (asset_map) {
        return [candidateByRiskLimit, acc_asset_map]
    }
    return candidateByRiskLimit
}

async function GetRiskLimit(account, asset) {
    for (let i=0; i<3; i++) {
        try {
            if (i>0) {
                console.log('retrying...')
            }
            return await ApiConnector.get_risklimit(account, asset)
        } catch(e) {
            console.log("GetRiskLimit", e);
        }
    }
}

async function AddRiskLimit(account, asset, new_limit, new_debt=0) {
    for (let i=0; i<3; i++) {
        try{
            if (i>0) {
                console.log('retrying...')
            }
            let resp = await ApiConnector.update_risklimit(account, asset, new_limit, new_debt)
            return resp.success
        } catch (e) {
            console.log("AddRiskLimit", e);
        }
    }
}

async function ClearRiskLimit(account, asset) {
    for (let i=0; i<3; i++) {
        try{
            if (i>0) {
                console.log('retrying...')
            }
            let resp = await ApiConnector.update_risklimit(account, asset, 0)
            return resp.success
        } catch (e) {
            console.log("ClearRiskLimit", e);
        }
    }
}

async function SwitchRiskLimit(account, asset, new_account) {
    try{
        let risklimit = await GetRiskLimit(account, asset);
        if (risklimit) {
            let new_account_old_rl = await GetRiskLimit(new_account, asset);
            if (new_account_old_rl) {
                console.log('ABORT SWITCH With Existing RL in New: ', new_account, asset, new_account_old_rl)
                return 0
            }
            let resp1 = await ClearRiskLimit(account, asset)
            console.log(resp1, `${account} clear ${asset} from ${risklimit} to 0`)
            let resp2 = await AddRiskLimit(new_account, asset, risklimit)
            console.log(resp2, `${new_account} update ${asset} to ${risklimit}`)
            return risklimit
        } else {
            return -1
        }
    } catch (e) {
        console.log("SwitchRiskLimit", e);
    }
}


module.exports = {
    getAccountSymbolStrategyAeg,
    LoadAllRiskLimitCandidate,
    LoadAllRunningStrategy,
    SwitchRiskLimit,
    ClearRiskLimit,
    GetRiskLimit,
    AddRiskLimit,
    saveMap2Redis
};