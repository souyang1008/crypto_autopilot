const strategyRuntimeSerive = require("./StrategyRuntimeService");
const {handLogger} = require("./LogService");
const strategyOpsService = require("./StrategyOpsService");
const strategyAdvisorService = require("./StrategyAdvisorService");
const accountService = require("./AccountService");
const ApiConnector = require("../utils/api_connector");


async function switch_account(account, assigned_account=undefined) {
    try {
        let prdLine = account.replace(/\d/g, '');
        let exchange = accountService.prdLine2Exchange(prdLine);
        // let accounts = await accountService.GetAccountCandidates(prdLine, 'btctf0');
        // let isSwitched = await accountService.isAccountDEALT(account);
        let isFrozen = await accountService.isAccountBL(account);
        if (isFrozen) {
            handLogger.info('Frozen Skip', account);
            console.log('Frozen Skip', account)
            return;
        }
        let curr_snaps = await accountService.getAccountStrategies(account)
        if (curr_snaps.length !== 1) {
            handLogger.info('ERROR: Multiple Strategies in this Account: ', JSON.stringify(curr_snaps));
            console.log('ERROR: Multiple Strategies in this Account: ', JSON.stringify(curr_snaps));
        }
        curr_snaps = curr_snaps[0];
        let [symbol, net, comm, sname, sid, stype] = [curr_snaps.contract, curr_snaps.netUsdt, curr_snaps.commission, curr_snaps.strategy, curr_snaps.sid, curr_snaps.stype];
        let strategy_utilratio = net/comm
        let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
        let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
        let accounts = accountService.GetAccountCandidates(prdLine, symbol, account);
        let candidates = await accountService.GetCandidateStatsSortedByVol(accounts, accDetail.ttnov);
        if (account.startsWith('BG')) {
            if (!['btctf0', 'ethtf0'].includes(symbol)) {
                if (strategy_utilratio < 0.2) {
                    handLogger.info(`HAULT SWITCH Due to low util: ${strategy_utilratio.toFixed(2)}, ${account}#${symbol}`);
                    return;
                }
            }
            candidates = await accountService.GetCandidateStatsForFut(accounts, accDetail.ordCnt, accDetail.dealCnt, accDetail.tRbt);
        }

        let success = false;
        let prefered_strats = 'taker_rank_sorted'
        let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
        let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
        if (pb_ranks[prefered_strats].length===0 || ap_size <= 1) {
            prefered_strats = 'taker_pure_batch'
        }
        let new_account = candidates[0];
        let strats_candidates = pb_ranks[prefered_strats];
        if (account.startsWith('BGF') && !assigned_account) {
            new_account = await accountService.GetLowestPnlAccount(candidates.slice(0,3));
            if (new_account) {
                let new_account_curr_snap = await accountService.getAccountStrategies(new_account);
                if (new_account_curr_snap.length > 1) {
                    handLogger.info('ERROR: Multiple Strategies in New Account: ', JSON.stringify(new_account_curr_snap));
                    console.log('ERROR: Multiple Strategies in New Account: ', JSON.stringify(new_account_curr_snap));
                    return;
                } else if (new_account_curr_snap.length === 1) {
                    new_account_curr_snap = new_account_curr_snap[0];
                    if (new_account_curr_snap.netUsdt > net) {
                        handLogger.info(`HAULT New Account Earning More: ${new_account_curr_snap.account}, ${new_account_curr_snap.contract}`);
                        console.log('HAULT New Account Earning More: ', new_account_curr_snap.account, new_account_curr_snap.contract);
                        return;
                    }
                    await strategyOpsService.ShutdownStrategies(new_account_curr_snap.contract, new_account, new_account_curr_snap.strategy, new_account_curr_snap.stype)
                }
                await strategyOpsService.ShutdownStrategies(symbol, account, sname, stype);
                if (strategy_utilratio > 0.2 && net > 200) {
                    let resp = await strategyOpsService.AddStrategyWithTSID(new_account, symbol, exchange, sid, sname, stype, ap_size, "UNI_SWITCH");
                    console.log(new_account, symbol, "Add", sid, sname, JSON.stringify(resp));
                    handLogger.info(new_account, `${symbol}, "Add", ${sid}, ${sname}, ${JSON.stringify(resp)}`);
                } else {
                    await strategyOpsService.AddStrategy(symbol, new_account, strats_candidates, exchange, ap_size);
                }
                success = true;
            }
        } else {
            for (let new_account of candidates) {
                if (assigned_account) {
                    new_account = assigned_account;
                }
                let ttnov = accDetail.ttnov[new_account];
                if (account.startsWith('MCF')) {
                    let rc = accountService.isMCRiskControl(account, accDetail.ordCnt[new_account], accDetail.dealCnt[new_account], ttnov);
                    if (rc || ttnov > accountService.MCFUT_MAX_VOL * 0.7) {
                        console.log('Skip Heavy/RC Account: ', new_account, ttnov);
                        continue;
                    }
                }

                let new_account_curr_snap = await accountService.getAccountStrategies(new_account);
                new_account_curr_snap = new_account_curr_snap[0];
                await strategyOpsService.ShutdownStrategies(new_account_curr_snap.contract, new_account, new_account_curr_snap.strategy, new_account_curr_snap.stype)
                await strategyOpsService.ShutdownStrategies(symbol, account, sname, stype);
                let resp1 = await strategyOpsService.AddStrategyWithTSID(new_account, symbol, exchange, sid, sname, stype, ap_size, "UNI_SWITCH");
                console.log(new_account, symbol, "Add", sid, sname, JSON.stringify(resp1));
                handLogger.info(new_account, `${symbol}, "Add", ${sid}, ${sname}, ${JSON.stringify(resp1)}`);
                let resp2 = await strategyOpsService.AddStrategyWithTSID(account, new_account_curr_snap.contract, exchange, new_account_curr_snap.sid, new_account_curr_snap.strategy, new_account_curr_snap.stype, new_account_curr_snap.size, "UNI_SWITCH");
                console.log(account, new_account_curr_snap.contract, "Add", new_account_curr_snap.sid, new_account_curr_snap.strategy, JSON.stringify(resp2));
                handLogger.info(new_account, `${new_account_curr_snap.contract}, "Add", ${new_account_curr_snap.strategy}, ${JSON.stringify(resp2)}`);
                success = true;
                break;
            }
        }

        if (success) {
            await accountService.setAccountDEALT(account);
            await accountService.setAccountBL(new_account);
            // let res = await ApiConnector.relaunch('tkc1');
            // handLogger.info(`relaunch success`, res);
            // console.log(`relaunch success`, res)
        }
    } catch (err) {
        handLogger.info("Error: " + err);
        console.log("Error: " + err)
    }
}


module.exports = {
    switch_account
};