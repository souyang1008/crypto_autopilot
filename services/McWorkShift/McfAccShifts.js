const accountService = require("../AccountService");
const ApiConnector = require("../../utils/api_connector");
const strategyRuntimeSerive = require("../StrategyRuntimeService");
const {apLogger} = require("../LogService");


async function getAccsStrategies(accounts){
    let snaps = []
    for (let acc of accounts) {
        let strats = await accountService.getAccountStrategies(acc);
        snaps = snaps.concat(strats);
    }
    return snaps
}

let morning_group =  ['MCF608'];
let night_group = ['MCF509', 'MCF704', 'MCF802']
async function AccShift(hour){
    let open_group = [];
    let close_group = [];
    if (hour === 8) {
        open_group = morning_group;
        close_group = night_group;
    } else if (hour === 21) {
        open_group = night_group;
        close_group = morning_group;
    }
    console.log(hour, open_group, close_group)
    let EnableAccStrats = await getAccsStrategies(open_group);
    for (let snap of EnableAccStrats) {
        let [account, symCont, strategyName, size_before] = [snap.account, snap.contract, snap.strategy, snap.size];
        let ap_size = await strategyRuntimeSerive.getApSize('MCF', symCont);
        let res = await ApiConnector.update_size(account, symCont, strategyName, size_before, ap_size, true)
        console.log(res.success, `OPEN: ${account}#${symCont}#${strategyName} changed from ${size_before} to ${ap_size}`)
    }
    let ShutAccStrats = await getAccsStrategies(close_group);
    for (let snap of ShutAccStrats) {
        let [account, symCont, strategyName, size_before] = [snap.account, snap.contract, snap.strategy, snap.size];
        let res = await ApiConnector.update_size(account, symCont, strategyName, size_before, 0, true);
        console.log(res.success, `SHUT: ${account}#${symCont}#${strategyName} changed from ${size_before} to ${0}`)
    }
}


module.exports = {
    AccShift
};