const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const MktMakerAccList = ['HBF3007', 'BNBF607', 'BNB607', 'BNBC607', 'GTF107', 'GT107'];
const {getRedis, promisifyRedis} = require("../store/redis");
const rRawClient = promisifyRedis(getRedis("raw"));
const rClient = promisifyRedis(getRedis("app"));
const net_mapping = {'fut': 'netUsdt', 'spot': 'net'}
const strategyOpsService = require("../services/StrategyOpsService");
const stratLuciferService = require("./StrategyLuciferService");
const strategyRouterService = require("../services/StrategyRouterService");
const ApiConnector = require("../utils/api_connector");
const {apLogger} = require("./LogService");
const CRYPTO_ACCOUNT_FROZEN = 'CRYPTO_ACCOUNT_FROZEN'
const CRYPTO_ACCOUNT_SWITCHED = 'CRYPTO_ACCOUNT_SWITCHED'
const [CWFUT_MAX_ORDCNT, MCFUT_MAX_ORDCNT, MCSPOT_MAX_ORDCNT, BGFUT_MAX_ORDCNT,
       BGFUT_MAX_DEALCNT, BGFUT_MAX_TRBT] = [1400, 300, 1000, 1200, 600, 330];
const [DCFUT_MAX_ORDCNT, DCFUT_MAX_TRBT] = [45, 100];
const [LBFUT_MAX_ORDCNT, EXFUT_MAX_ORDCNT, EXFUT_MAX_TRBT] = [40, 120, 14];
const [BGFUT_HEAVY_ORDCNT, BGFUT_HEAVY_DEALCNT, BGFUT_HEAVY_TRBT] = [800, 400, 300, 400];
const [BGSPOT_MAX_ORDCNT, BGSPOT_MAX_DEALCNT, BGSPOT_MAX_TRBT] = [2000, 1200, 1000];
const [BGSPOT_HEAVY_ORDCNT, BGSPOT_HEAVY_DEALCNT, BGSPOT_HEAVY_TRBT] = [1000, 600, 1000];
const [MCSPOT_MAX_DEALCNT] = [350];
const [MCSPOT_HOURLY_ORDCNT, MCFUT_HOURLY_ORDCNT, MCFUT_3H_ORDCNT, BGFUT_HOURLY_ORDCNT] = [30, 40, 90, 60];
const [CWFUT_HOURLY_ORDCNT, CWFUT_4H_ORDCNT] = [60, 170];
const [BGFUT_MAX_VOL, MCFUT_MAX_VOL, MCFUT_MAX_TVOL, MCSPOT_MAX_VOL] = [1000000, 1300000, 1400000, 200000];
const MCS_accounts = new Set([]);

const MCS_ACC_SYM_BLK = {
    'MCS013': ['BTCUSD'],
    'MCS009': ['BTCUSD']
}

const BGS_accounts = new Set([
    'BGS405',
    'BGS501', 'BGS507', 'BGS508',
    'BGS703', 'BGS707',
    'BGS1002', 'BGS1006', 'BGS1007',
    'BGS1101', 'BGS1102', 'BGS1104', 'BGS1105', 'BGS1107',
    'BGS1202', 'BGS1204', 'BGS1205', 'BGS1206', 'BGS1207',
    'BGS1301', 'BGS1302', 'BGS1303', 'BGS1304', 'BGS1305', 'BGS1306', 'BGS1307',
    'BGS1402', 'BGS1403', 'BGS1501'
]);


let trbt_limit = {};
let tvol_limit = {};
let vol_limit = {};
let tdeal_limit = {};
let tord_limit = {};
let accGrp = {};
let apRecCfg = {};

function LoadAccGrpConfig() {
    accGrp = JSON.parse(fs.readFileSync(path.join(__dirname, "..", "config", "account_config.json")).toString());
}
let meta = {}
function LoadMeta() {
    meta = yaml.safeLoad(fs.readFileSync(path.join(__dirname, "..", "config", "crypto_meta.yaml"), 'utf8'));
}

async function LoadAllRecCfg() {
    let keys = await rClient.keysAsync('AP_REC#*')
    for (let k_i = 0; k_i < keys.length; k_i ++) {
        let cfg_key = keys[k_i];
        let ap_rec_key = cfg_key.split("#");
        let acc_prefix = ap_rec_key[1];
        let symbol = ap_rec_key[2];
        let key_info = JSON.parse(await rClient.getAsync(cfg_key));
        if (ap_rec_key.length !== 3) {
            console.log('Loading uncommon key: ', cfg_key);
            apRecCfg[cfg_key] = key_info
        } else {
            apRecCfg[`${acc_prefix}#${symbol}`] = key_info
        }

    }
    console.log('Loading All APREC...');
}

function get_apRecCfg() {
    return apRecCfg;
}

function SaveLimit() {
    rClient.setAsync('CRYPTO_REBATE_LIMIT', JSON.stringify(trbt_limit));
    rClient.setAsync('CRYPTO_VOL_LIMIT', JSON.stringify(tvol_limit));
}
LoadAccGrpConfig();
LoadMeta();
setInterval(LoadAccGrpConfig, 60*1000);
setInterval(LoadMeta, 3600*1000);
setInterval(SaveLimit, 3600*1000);

/**
 * @return {boolean}
 */
function IsMktMakerAcc(account) {
    return MktMakerAccList.indexOf(account) > -1;
}

function get_ex(account) {
    let exchange = account.replace(/\d/g, '');
    if (exchange.endsWith('R')) {
        exchange = exchange.slice(0,-1)
    }
    return exchange
}


function prdLine2Exchange(prodline) {
    if(prodline.startsWith("BNB")) {
        return "BINANCE";
    } else if(prodline.startsWith("HB")) {
        return "HUOBI";
    } else if(prodline.startsWith("GT")) {
        return "GATE";
    } else if(prodline.startsWith("OKC")) {
        return "OKCOIN"
    } else if(prodline.startsWith("KC")) {
        return "KUCOIN"
    } else if(prodline.startsWith("BYB")) {
        return "BYBIT"
    } else if(prodline.startsWith("BG")) {
        return "BITGET"
    } else if(prodline.startsWith("MC")) {
        return "MEXC"
    } else if(prodline.startsWith("CW")) {
        return "COINW"
    } else if(prodline.startsWith("WX")) {
        return "WEEX"
    } else if(prodline.startsWith("DC")) {
        return "DEEPCOIN"
    } else if(prodline.startsWith("XT")) {
        return "XT"
    } else if(prodline.startsWith("LB")) {
        return "LBANK"
    } else if(prodline.startsWith("EX")) {
        return "EX3"
    } else if(prodline.startsWith("HC")) {
        return "HOTCOIN"
    } else if(prodline.startsWith("BM")) {
        return "BITMART"
    } else {
        throw new Error("unknown prodline", prodline);
    }
}

function acc2guiExchange(prodline) {
    if(prodline === "BNB") {
        return "bnbspot";
    } else if(prodline === "HB") {
        return "hbspot";
    } else if(prodline === "GT") {
        return "gtspot";
    } else if(prodline === "OKC") {
        return "okcspot"
    } else if(prodline === "KCS") {
        return "kcspot"
    } else if(prodline === "BGS") {
        return "bgspot"
    } else if(prodline === "BYBF") {
        return "bybfut"
    } else if(prodline === "BGF") {
        return "bgfut"
    } else if(prodline === "KCF") {
        return "kcfut"
    } else if(prodline === "OKCF") {
        return "okcfut"
    } else if(prodline === "GTF") {
        return "gtfut"
    } else if(prodline === "BNBF" || prodline === "BNBC") {
        return "bnbfut"
    } else if(prodline === "HBF") {
        return "hbfut"
    } else if(prodline === "MCF") {
        return "mcfut"
    } else if(prodline === "MCS") {
        return "mcspot"
    } else if(prodline === "CWF") {
        return "cwfut"
    } else if(prodline === "WXF") {
        return "wxfut"
    } else if(prodline === "DCF") {
        return "dcfut"
    }else if(prodline === "XTF") {
        return "xtfut"
    }else if(prodline === "LBF") {
        return "lbfut"
    }else if(prodline === "EXF") {
        return "exfut"
    }else if(prodline === "HCF") {
        return "hcfut"
    } else {
        throw new Error("unknown prodline", prodline);
    }
}

function random_limit(limit, range=1.15) {
    return Math.floor(Math.random() * (limit * range - limit * 1.0) + limit * 1.0)
}

function random_order_size(order_size) {
    let random_oz =  Math.floor(Math.random() * (order_size * 1.05 - order_size * 0.95) + order_size * 0.95)
    let pretty_digits = random_oz.toString().length - 2
    let pretty_oz = Math.ceil(random_oz / 10**pretty_digits) * 10 **pretty_digits
    return pretty_oz
}

async function GetAccCandidatesForAllTypes(prdLine, symbol, candiatesforCoinbased, candidateByRiskLimit) {
    let candidates = [];
    try {
        candidates = GetAccountCandidates(prdLine, symbol);
    } catch (err) {
        if (prdLine === 'BNBC') {
            candidates = candiatesforCoinbased[symbol.slice(0,3).toUpperCase()];
        } else {
            candidates = candidateByRiskLimit[`${prdLine}-${symbol}`];
            if (prdLine === 'BGS') {
                candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
            }
            if(!candidates) {
                throw new Error("no account group: " + prdline);
            }
        }
    }
    return candidates;
}

function GetAccountCandidates(prdline, symbol, exclude_acc=undefined) {
    let candidates = [];
    let config = accGrp[prdline];
    if(!config) {
        throw new Error("no account group: " + prdline)
    }
    if(config.symbol_mapping[symbol]) {
        let grpList = config.symbol_mapping[symbol];
        for(let g of grpList) {
            for(let acc of config.account_group[g]) {
                if(!candidates.includes(acc) && (!exclude_acc || acc !== exclude_acc)) {
                    candidates.push(acc)
                }
            }
        }
    } else {
        let grp = config.default_group;
        for(let acc of config.account_group[grp]) {
            if(!candidates.includes(acc) && (!exclude_acc || acc !== exclude_acc)) {
                candidates.push(acc)
            }
        }
    }

    return candidates;
}

function GetCandidateStats(candidates, stratByAccount) {
    let result = [];
    for(let account of candidates) {
        let stats = {account, load: 0, symbols: []};
        let runStrats = stratByAccount[account];
        if(runStrats) {
            stats.load = runStrats.length;
            stats.symbols = runStrats.map(s => s.symCont)
        }
        result.push(stats);
    }
    result = result.sort((a, b)=> a.load - b.load);
    return result;
}


function isBGRiskControl(account, ordCnt, dealCnt, tRebate, ttnov) {
    if (!trbt_limit[account]) {
        trbt_limit[account] = account.startsWith('BGF') ? random_limit(BGFUT_MAX_TRBT) : random_limit(BGSPOT_MAX_TRBT);
    }
    if (account.startsWith('BGF') && !tvol_limit[account]) {
        tvol_limit[account] = random_limit(BGFUT_MAX_VOL)
    }
    let tvol_lim = tvol_limit[account];
    let trbt_lim = trbt_limit[account];
    if (account.startsWith('BGF')) {
        return ordCnt > BGFUT_MAX_ORDCNT || dealCnt > BGFUT_MAX_DEALCNT || tRebate > trbt_lim || ttnov > tvol_lim;
    }
    if (account.startsWith('BGS')) {
        return ordCnt > BGSPOT_MAX_ORDCNT || dealCnt > BGSPOT_MAX_DEALCNT || tRebate > trbt_lim;
    }
    return false
}

function isMCRiskControl(account, ordCnt, ordCnt1h, ordCnt3h, dealCnt, turnover, vol_since_reset) {
    if (!tvol_limit[account]) {
        tvol_limit[account] = account.startsWith('MCF') ? random_limit(MCFUT_MAX_TVOL) : random_limit(MCSPOT_MAX_VOL);
    }
    if (!tord_limit[account]) {
        tord_limit[account] = account.startsWith('MCF') ? random_limit(MCFUT_MAX_ORDCNT) : random_limit(MCSPOT_MAX_ORDCNT);
    }
    let tord_lim = tord_limit[account];
    let tvol_lim = tvol_limit[account];
    if (account.startsWith('MCF')) {
        if (!vol_limit[account]) {
            vol_limit[account] = random_limit(MCFUT_MAX_VOL);
        }
        let vol_lim = vol_limit[account];
        return turnover > tvol_lim || vol_since_reset > vol_lim || ordCnt1h > MCFUT_HOURLY_ORDCNT || ordCnt3h > MCFUT_3H_ORDCNT || ordCnt > tord_lim;
    } else {
        if (!tdeal_limit[account]) {
            tdeal_limit[account] = random_limit(MCSPOT_MAX_DEALCNT);
        }
        let tdeal_lim = tdeal_limit[account];
        return turnover > tvol_lim || dealCnt > tdeal_lim || ordCnt > tord_lim;
    }
}

function isCWRiskControl(account, ordCnt, ordCnt1h, ordCnt4h) {
    if (!tord_limit[account]) {
        tord_limit[account] = random_limit(CWFUT_MAX_ORDCNT);
    }
    let tord_lim = tord_limit[account];
    return ordCnt > tord_lim || ordCnt1h > CWFUT_HOURLY_ORDCNT || ordCnt4h > CWFUT_4H_ORDCNT;
}

function isLBRiskControl(account, ordCnt) {
    if (!tord_limit[account]) {
        let range = 1.15;
        if (['LBF303'].includes(account)) {
            range = 2.1
        }
        tord_limit[account] = random_limit(LBFUT_MAX_ORDCNT, range);
    }
    let tord_lim = tord_limit[account];
    return ordCnt > tord_lim
}

function isEXRiskControl(account, ordCnt, tRebate) {
    if (!tord_limit[account]) {
        tord_limit[account] = random_limit(EXFUT_MAX_ORDCNT, 1.15);
    }
    if (!trbt_limit[account]) {
        trbt_limit[account] = random_limit(EXFUT_MAX_TRBT, 1.1);
    }
    let tord_lim = tord_limit[account];
    let trbt_lim = trbt_limit[account];
    return ordCnt > tord_lim || tRebate > trbt_lim;
}


function isDCRiskControl(account, ordCnt, tRebate) {
    if (!tord_limit[account]) {
        tord_limit[account] =  random_limit(DCFUT_MAX_ORDCNT);
    }
    if (!trbt_limit[account]) {
        trbt_limit[account] = random_limit(DCFUT_MAX_TRBT);
    }
    let tord_lim = tord_limit[account];
    let trbt_lim = trbt_limit[account];
    return ordCnt > tord_lim || tRebate > trbt_lim;
}

function isAccountRC(account, pnl_summary) {
    let ex = acc2guiExchange(get_ex(account));
    let accOrdCnt = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['ordCnt'] : 0;
    let accDealCnt = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['dealCnt'] : 0;
    let accRebate = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['tRbt'] : 0;
    let accTtnov = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['ttnov'] : 0;
    let accvol = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['turnover'] : 0;
    let rc = false;
    if (account.startsWith('BG')) {
        rc = isBGRiskControl(account, accOrdCnt, accDealCnt, accRebate, accTtnov);
    } else if (account.startsWith('MCF')) { // no mcs accounted for right now
        let accOrdCnt1h = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['ordCnt1h'] : 0;
        let accOrdCnt3h = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['ordCnt3h'] : 0;
        rc = isMCRiskControl(account, accOrdCnt, accOrdCnt1h, accOrdCnt3h, accDealCnt, accTtnov, accvol);
    } else if (account.startsWith('CW')) {
        let accOrdCnt1h = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['ordCnt1h'] : 0;
        let accOrdCnt4h = pnl_summary['account_detail'][ex][account] ? pnl_summary['account_detail'][ex][account]['ordCnt4h'] : 0;
        rc = isCWRiskControl(account, accOrdCnt, accOrdCnt1h, accOrdCnt4h);
    } else if (account.startsWith('DC')) {
        rc = isDCRiskControl(account, accOrdCnt, accRebate);
    } else if (account.startsWith('LB')) {
        rc = isLBRiskControl(account, accOrdCnt);
    } else if (account.startsWith('EX')) {
        rc = isEXRiskControl(account, accOrdCnt, accRebate);
    }
    return rc
}

async function LockAccount(account) {
    let res1 = await ApiConnector.update_launcher_status(account, 'LOCK');
    strategyRouterService.update_account_launcher(account, 'LOCK');
    apLogger.info(`ACC_LAUNCHER_LOCK: ${account}: ${res1}`);
    let res2 = await ApiConnector.update_acc_stats(account, true);
    apLogger.info(`ACC_RC Posted: ${account}#true ${res2}`);
}

async function BanAccount(account) {
    let res1 = await ApiConnector.update_launcher_status(account, 'BAN');
    strategyRouterService.update_account_launcher(account, 'BAN');
    apLogger.info(`ACC_LAUNCHER_BAN: ${account}: ${res1}`);
    let res2 = await ApiConnector.update_acc_stats(account, true);
    apLogger.info(`ACC_RC Posted: ${account}#true ${res2}`);
}

async function ActivateAccount(account) {
    let res1 = await ApiConnector.update_launcher_status(account, '7');
    strategyRouterService.update_account_launcher(account, '7');
    apLogger.info(`ACC_LAUNCHER_RELEASE: ${account}: ${res1}`);
    let res2 = await ApiConnector.update_acc_stats(account, false);
    apLogger.info(`ACC_RC Posted: ${account}#false ${res2}`);
}

async function UpdateAccountRCStatus(account, riskcontrol, launcher_status, summary_status, is_router=false) {
    let action = false;
    if (launcher_status !== riskcontrol || summary_status !== riskcontrol) {
        strategyRouterService.loadLauncherStatus();
        let latest_launcher_status = strategyRouterService.get_launcher_status_from_lucifer(account);
        if (latest_launcher_status!== null && latest_launcher_status===launcher_status) {
            action = true;
            if (is_router) {
                if (riskcontrol) {
                    if (account.startsWith('LBF')) {
                        await LockAccount(account);
                    } else {
                        await BanAccount(account);
                    }
                } else {
                    await ActivateAccount(account);
                }
            } else {
                ApiConnector.update_acc_stats(account, riskcontrol).then(() => {
                    apLogger.info(`ACC_RC Posted: ${account}#${riskcontrol}`)
                });
            }
        } else {
            apLogger.info(`ACC_LAUNCHER_MATCH_FAIL: ${account}#${latest_launcher_status}#${launcher_status}#${riskcontrol}`)
        }
    }
    return action;
}

function isMCCoolDown(account, symbol, mkt_share, hourly_vol, hourly_ords) {
    if (account.startsWith('MCF')) {
        if (mkt_share > 0.1 || (['btctf0', 'ethtf0'].includes(symbol) && mkt_share > 0.005 )) {
            return true
        }
    }
    if (account.startsWith('MCS')) {
        if (hourly_ords > MCSPOT_HOURLY_ORDCNT || mkt_share > 0.03 || (['BTCUSD', 'ETHUSD'].includes(symbol) && mkt_share > 0.005 ) || hourly_vol > 15000) {
            return true
        }
    }
    return false
}

function isBGCoolDown(account, symbol, mkt_share) {
    if (account.startsWith('BGF')) {
        if (mkt_share > 0.05 || (['btctf0', 'ethtf0'].includes(symbol) && mkt_share > 0.01 )) {
            return true
        }
    }
    if (account.startsWith('BGS')) {
        if (mkt_share > 0.2 || (['BTCUSD', 'ETHUSD'].includes(symbol) && mkt_share > 0.08 )) {
            return true
        }
    }
    return false
}

function isHeavyLoad(account, ordCnt, dealCnt, tRebate) {
    if (account.startsWith('BGF')) {
        return ordCnt > BGFUT_HEAVY_ORDCNT || dealCnt > BGFUT_HEAVY_DEALCNT || tRebate > BGFUT_HEAVY_TRBT;
    }
    if (account.startsWith('BGS')) {
        return ordCnt > BGSPOT_HEAVY_ORDCNT || dealCnt > BGSPOT_HEAVY_DEALCNT || tRebate > BGSPOT_HEAVY_TRBT;
    }
    return false
}

async function getAccountStrategies(account, risklimit=false, symbol=undefined){
    let regex = `CRYPTO_SNAP[RS][^M]*${account}#*`;
    if (symbol) {
        regex = `CRYPTO_SNAP[RS][^M]*${account}#${symbol}#*`
    }
    let strats = await rClient.keysAsync(regex);
    let current_strats = [];
    for(let k of strats) {
        let snap = JSON.parse(await rClient.getAsync(k));
        if(IsMktMakerAcc(snap.account) || k.includes('NotFound')) {
            continue;
        }
        if(snap.enabled) {
            if (risklimit && snap.riskLimit === 0) {
                // skip zero risklimits
                console.log('skip zero risklimits', snap.symCont);
                continue;
            }
            current_strats.push(snap)
        }
    }
    let is_router = await strategyRouterService.has_router(account);
    if (symbol && !is_router) {
        if (current_strats.length === 1) {
            return current_strats[0]
        } else if (current_strats.length > 1){
            console.log(`ERROR: no snap/multiple active for ${regex}`)
        }
        console.log('snap not found', regex, strats);
        for (let strat of strats) {
            let snap = JSON.parse(await rClient.getAsync(strat));
            console.log(strat, snap.enabled);
        }
        return undefined
    }
    return current_strats
}

async function checkSnapMultiple(account) {
    let has_router = await strategyRouterService.has_router(account);
    if (has_router) {
        return false;
    } else {
        let curr_snaps = await getAccountStrategies(account);
        curr_snaps = curr_snaps.filter(snap => snap.size > 0);
        return curr_snaps.length > 1;
    }
}

async function shutDownAccount(account){
    let current_strats = await getAccountStrategies(account);
    for (let snap of current_strats) {
        let symbol = snap.is_fut ? snap.contract: snap.symbol;
        if (snap.size > 0) {
            await strategyOpsService.ShutdownStrategies(symbol, account, snap.strategy, snap.stype);
        }
    }
}

async function GetCandidateStatsForLimited(candidates, acc_ordcnt, acc_dealcnt, acc_rebate, util_ratio, usd_fornew, rl_notional) {
    let result = [];
    let [max_ordcnt, max_deal, max_rebate] = [0, 0, 0];
    for(let account of candidates) {
        let isFrozen = await isAccountBL(account);
        let isSwitched = await isAccountDEALT(account);
        if (isFrozen || isSwitched) {
            continue;
        }
        let stats = {account, ordCnt: acc_ordcnt[account] || 0, dealCnt: acc_dealcnt[account] || 0,
            tRebate: acc_rebate[account] || 0, utilRatio: util_ratio[account] || 1,
            usdFornew: usd_fornew[account] || 0, rlNotional: rl_notional[account] || 0};
        max_ordcnt = stats.ordCnt > max_ordcnt ? stats.ordCnt : max_ordcnt;
        max_deal = stats.dealCnt > max_deal ? stats.dealCnt : max_deal;
        max_rebate = stats.tRebate > max_rebate ? stats.tRebate : max_rebate;
        let isRC = isBGRiskControl(stats.account, stats.ordCnt, stats.dealCnt, stats.tRebate, 0);
        if (!isRC) {
            result.push(stats);
        }
    }
    for (let info of result) {
        info.norm_ordCnt = info.ordCnt / max_ordcnt;
        info.norm_dealCnt = info.dealCnt / max_deal;
        info.norm_tRebate = info.tRebate / max_rebate;
        info.rank_score = Number((info.norm_ordCnt * 2 + info.norm_dealCnt + info.norm_tRebate + Number(info.utilRatio)).toFixed(2))
    }

    result = result.sort((a, b)=> a.rank_score - b.rank_score);
    console.log(JSON.stringify(result));
    return result;
}

async function GetCandidateStatsSortedByVol(candidates, acc_vol) {
    let result = [];
    for(let account of candidates) {
        let isFrozen = await isAccountBL(account);
        let isSwitched = await isAccountDEALT(account);
        if (isFrozen || isSwitched) {
            continue;
        }
        let stats = {account, turnover: acc_vol[account] || 0};
        result.push(stats);
    }
    result = result.sort((a, b)=> a.turnover - b.turnover);
    console.log(JSON.stringify(result));
    return result;
}

async function GetCandidateStatsForFut(candidates, acc_ordcnt, acc_dealcnt, acc_rebate) {
    let result = [];
    let [max_ordcnt, max_deal, max_rebate] = [BGFUT_MAX_ORDCNT, BGFUT_MAX_DEALCNT, BGFUT_MAX_TRBT];
    for(let account of candidates) {
        let isFrozen = await isAccountBL(account);
        let isSwitched = await isAccountDEALT(account);
        if (isFrozen || isSwitched) {
            continue;
        }
        let curr_snaps = await getAccountStrategies(account);
        curr_snaps = curr_snaps.filter(snap => snap.size === 0 || !['btctf0', 'ethtf0'].includes(snap.contract));
        if (curr_snaps.length < 1) {
            continue;
        }
        let stats = {account, ordCnt: acc_ordcnt[account] || 0, dealCnt: acc_dealcnt[account] || 0,
            tRebate: acc_rebate[account] || 0};
        let isRC = isBGRiskControl(stats.account, stats.ordCnt, stats.dealCnt, stats.tRebate, 0);
        if (!isRC) {
            result.push(stats);
        }
    }
    for (let info of result) {
        info.norm_ordCnt = info.ordCnt / max_ordcnt;
        info.norm_dealCnt = info.dealCnt / max_deal;
        info.norm_tRebate = info.tRebate / max_rebate;
        info.rank_score = Number((info.norm_dealCnt + info.norm_tRebate).toFixed(2))
    }

    result = result.sort((a, b)=> a.rank_score - b.rank_score);
    console.log(JSON.stringify(result));
    return result;
}

async function GetCandiatesforCoinbased(prdline) {
    let candiatesforCoinbased = {};
    let exchange = prdLine2Exchange(prdline)
    let rights = await rRawClient.keysAsync(`CRYPTO_RIGHT#${prdline}[0-9]*`);
    for (let key of rights) {
        let account = key.split('#')[1]
        if(IsMktMakerAcc(account)) {
            continue;
        }
        let right = await rRawClient.getAsync(key)
        right = JSON.parse(right)
        for (let sym in right) {
            if (sym === 'USD') {continue}
            let meta_key = `${sym}USD-${exchange}`
            let price = 0.1
            try {
                if ('price' in meta[meta_key]) {
                    price = meta[meta_key]['price']
                }
            } catch(err) {
                console.log('failed to get price in meta: ', err.message)
            }
            let notional = Number(price) * Number(right[sym]['end'])
            if (notional > 50) {
                candiatesforCoinbased[sym] ? candiatesforCoinbased[sym].push(account) : candiatesforCoinbased[sym] = [account]
            }
        }
    }
    return candiatesforCoinbased
}


async function setAccountBL(acc) {
    let accounts = await rClient.getAsync(CRYPTO_ACCOUNT_FROZEN);
    if (accounts) {
        accounts = JSON.parse(accounts);
        accounts.push(acc);
    } else {
        accounts = [acc];
    }
    console.log('SET ACCOUNT BL: ', acc);
    await rClient.setAsync(CRYPTO_ACCOUNT_FROZEN, JSON.stringify(accounts));
}

async function isAccountBL(acc) {
    let accounts = await rClient.getAsync(CRYPTO_ACCOUNT_FROZEN);
    if (accounts) {
        accounts = JSON.parse(accounts);
        return accounts.includes(acc);
    } else {
        return false;
    }
}

async function setAccountDEALT(acc) {
    let accounts = await rClient.getAsync(CRYPTO_ACCOUNT_SWITCHED);
    if (accounts) {
        accounts = JSON.parse(accounts);
        accounts.push(acc);
    } else {
        accounts = [acc];
    }
    console.log('SET ACCOUNT SWITCHED: ', acc);
    await rClient.setAsync(CRYPTO_ACCOUNT_SWITCHED, JSON.stringify(accounts));
}
async function isAccountDEALT(acc) {
    let accounts = await rClient.getAsync(CRYPTO_ACCOUNT_SWITCHED);
    if (accounts) {
        accounts = JSON.parse(accounts);
        return accounts.includes(acc);
    } else {
        return false;
    }
}

async function getAllAccounts(prdline) {
    let candidates = [];
    if (!prdline.endsWith('F')) {
        let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate();
        if (prdline === 'BGS' || prdline === 'MCS') {
            candidates = get_available_accs(prdline);
        } else {
            candidates = candidateByRiskLimit[`${prdline}-BTCUSD`]
        }
    } else {
        candidates = GetAccountCandidates(prdline, 'btctf0'); //MCF, BGF
    }
    return candidates;
}

async function checkUniqueEnabled(prdline) {
    let candidates = await getAllAccounts(prdline)
    let msg = ''
    let accounts = [];
    for (let candidate of candidates){
        let is_multiple = await checkSnapMultiple(candidate);
        if (is_multiple) {
            msg = `ERROR: Multiple Active Strategies in ${candidate}\n`
            console.log(`ERROR: Multiple Active Strategies in ${candidate}`)
            accounts.push(candidate)
        }
    }
    return [msg, accounts]
}

async function checkDupToDel(prdline) {
    let candidates = await getAllAccounts(prdline);
    for (let candidate of candidates){
        let curr_snaps = await getAccountStrategies(candidate);
        if(curr_snaps.length > 1) {
            curr_snaps = curr_snaps.filter(snap => snap.size === 0);
            for (let snap of curr_snaps) {
                await strategyOpsService.ClearStrategyForSpot(snap);
                console.log(`Clear Strategy ${candidate}`)
            }
        }
    }
}

async function get_router_pnl(account, symbol) {
    let accounts = strategyRouterService.get_members(account)
    let [sum_net, sum_lastpnl] = [0, 0]
    for (let acc of accounts) {
        let snap = await getAccountStrategies(acc, false, symbol);
        if (snap) {
            let k_type = snap['is_fut'] ? 'fut' : 'spot' ;
            sum_net += Number(snap[net_mapping[k_type]]);
            sum_lastpnl += Number(snap['lastPnl']);
        }
    }
    return [sum_net, sum_lastpnl, accounts]
}

async function GetLowestPnlAccount(accounts) {
    let lowest_account = undefined
    let pnl = 9999
    console.log('accs', accounts)
    for (let info of accounts) {
        let account = info.account
        let snap = await getAccountStrategies(account);
        snap = snap.filter(snap => snap.size > 0);
        if (snap.length === 0) {
            return account;
        }
        snap = snap[0]
        let net = snap.netUsdt;
        if (net < pnl) {
            pnl = net;
            lowest_account = account;
        }
    }
    return lowest_account;
}

function get_available_accs(prdline, exclude_account=undefined) {
    let accounts = {}
    if (prdline === 'MCS') {
        accounts = new Set(MCS_accounts);
    } else if (prdline === 'BGS') {
        accounts = new Set(BGS_accounts);
    }
    if (exclude_account) {
        accounts.delete(exclude_account);
    }
    return Array.from(accounts);
}


module.exports = {
    BGFUT_MAX_ORDCNT,
    BGFUT_MAX_DEALCNT,
    BGFUT_MAX_TRBT,
    MCSPOT_MAX_VOL,
    MCFUT_MAX_VOL,
    MCS_accounts,
    BGS_accounts,
    MCS_ACC_SYM_BLK,
    get_apRecCfg,
    LoadAllRecCfg,
    get_available_accs,
    get_ex,
    acc2guiExchange,
    GetAccCandidatesForAllTypes,
    GetAccountCandidates,
    GetCandidateStats,
    GetCandidateStatsForFut,
    GetCandidateStatsSortedByVol,
    GetCandidateStatsForLimited,
    GetCandiatesforCoinbased,
    shutDownAccount,
    IsMktMakerAcc,
    random_order_size,
    prdLine2Exchange,
    isHeavyLoad,
    isAccountRC,
    BanAccount,
    ActivateAccount,
    UpdateAccountRCStatus,
    isBGRiskControl,
    isMCRiskControl,
    isMCCoolDown,
    isBGCoolDown,
    isAccountBL,
    isAccountDEALT,
    setAccountBL,
    setAccountDEALT,
    getAccountStrategies,
    GetLowestPnlAccount,
    checkUniqueEnabled,
    checkDupToDel
};