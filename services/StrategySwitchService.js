const fs = require("fs");
const userConfig = JSON.parse(fs.readFileSync('config/user.json'));

const accountService = require("./AccountService");
const strategyAdvisorService = require("./StrategyAdvisorService");
const strategyRuntimeSerive = require("./StrategyRuntimeService");
const strategyOpsService = require("./StrategyOpsService");
const {host_mapping} = require("../main/helper_functions");
const {apLogger} = require("./LogService");



async function SwitcByPb(exchange, symbol, account, tried_tol=1) {
    let ex_fullname = accountService.prdLine2Exchange(exchange);
    let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(ex_fullname, symbol);
    let ap_size = await strategyRuntimeSerive.getApSize(exchange, symbol);
    let prefered_strats = 'taker_rank_sorted'
    if (ap_size === 1) {
        ap_size = -1
    }
    if (pb_ranks[prefered_strats].length===0 || ap_size <= 1) {
        prefered_strats = 'taker_pure_batch'
    }
    await strategyOpsService.AddStrategy(symbol, account, pb_ranks[prefered_strats], ex_fullname, ap_size, tried_tol);
    return host_mapping(account, exchange, userConfig, apLogger);
}


module.exports = {
    SwitcByPb
};
