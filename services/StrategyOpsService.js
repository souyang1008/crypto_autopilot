const ApiConnector = require("../utils/api_connector");
const strategyRouterService = require("./StrategyRouterService");
const strategyLuciferService = require("./StrategyLuciferService");

let router_line = ["BITGET", "MEXC", "COINW", "WEEX", "DEEPCOIN", "XT", "LBANK", "EX3", "HOTCOIN", "BITMART"];
async function AddStrategyWithTSID(account, symbol, exchange, sid, sname, stype, size, trigger, deps=undefined) {
    let this_strats = await strategyLuciferService.getAccountSymbolStrategyAeg(account, symbol);
    let tsid = undefined;
    if (this_strats.length !== 0) {
        tsid = this_strats[0].sid;
    }
    console.log('tsid', tsid, account, symbol, exchange)
    return await ApiConnector.add_strategy(account, symbol, `${symbol}-${exchange}`, tsid, sid, sname, stype, size, trigger, deps);
}

async function AddPassthrough(symbol, account, exchange) {
    let stype = "80"
    let [sid, sname] = ['999', 'PASSTHROUGH']
    let deps = `${symbol}-${exchange},${symbol}-${exchange}_TRADE`
    let resp = await AddStrategyWithTSID(account, symbol, exchange, sid, sname, stype, 0, 'APPT', deps);
    console.log(account, symbol, "Add", sname, deps, JSON.stringify(resp));
}

async function AddStrategy(symbol, pointed_account, strat_options, exchange, ap_size, tried_tol=1) {
    let sorted_options = strat_options.sort((a, b) => {
        if(a.tried_times < b.tried_times) {
            return a.rank - b.rank;
        } else {
            return a.tried_times - b.tried_times;
        }
    });
    let added = false;
    let stype = "61";
    if (router_line.includes(exchange)) {
        stype = "711";
        let has_router = await strategyRouterService.has_router(pointed_account);
        let is_router = await strategyRouterService.is_router(pointed_account);
        if (has_router || is_router) {
            stype = "42";
        }
    }
    for(let option of sorted_options) {
        if (option.tried_times <= tried_tol) {
            try {
                let sid = "100" + option.id;
                let sname = option.name + "_dtw1";
                let resp = await AddStrategyWithTSID(pointed_account, symbol, exchange, sid, sname, stype, ap_size, 'AP_GATLING');
                added = true;
                console.log(pointed_account, symbol, "Add", option.id, option.name, ap_size, JSON.stringify(resp));
            }catch (e) {
                console.error("add_strategy", e);
            }
            break;
        }
    }
    try {
        if (sorted_options.length > 1 && !added) {
            let option = sorted_options[0];
            let sid = "100" + option.id;
            let sname = option.name + "_dtw1";
            let resp = await AddStrategyWithTSID(pointed_account, symbol, exchange, sid, sname, stype, ap_size, 'AP_GATLING');
            console.log(pointed_account, symbol, "Add", option.id, option.name, JSON.stringify(resp));
        }
    }catch (e) {
        console.error("add_strategy", e);
    }
}


async function AddMoreStrategies(symbol, account_candidates, num, strat_options, exchange, ap_size) {
    let sorted_options = strat_options.sort((a, b) => {
        if(a.tried_times < b.tried_times) {
            return a.rank - b.rank;
        } else {
            return a.tried_times - b.tried_times;
        }
    });
    let stype = "61"
    if (router_line.includes(exchange)) {
        stype = "711"
    }
    let added_sid = [];
    let success_num = 0;
    let remain_cnt = num;
    for(let candidate of account_candidates) {
        if(!candidate.symbols.includes(symbol)) {
            for(let option of sorted_options) {
                if(option.tried_times < 2 && !added_sid.includes(option.id) && remain_cnt > 0) {
                    try {
                        let sid = "100" + option.id;
                        let sname = option.name + "_dtw1";
                        let resp = await AddStrategyWithTSID(candidate.account, symbol, exchange, sid, sname, stype, ap_size, 'AP_GATLING');
                        console.log(candidate.account, symbol, "AddMore", option.id, option.name, JSON.stringify(resp));
                        success_num += 1

                    }catch (e) {
                        console.error("add_strategy", e);
                    }
                    remain_cnt--;
                    added_sid.push(option.id);
                    break;
                }
            }
        }
    }
    return num - success_num
}

async function AddMoreStrategiesByPnl(symbol, account_candidates, num, strat_options, exchange, ap_size) {
    let sorted_options = Object.keys(strat_options).map(function(key) {
        return [key, strat_options[key]]
    });
    sorted_options.sort(function(a, b) {
        return b[1] - a[1]
    })
    let added_sid = [];
    let success_num = 0;
    let remain_cnt = num;
    let acc_looped = 0
    for(let candidate of account_candidates) {
        acc_looped += 1;
        if(!candidate.symbols.includes(symbol)) {
            let added = false
            for(let option of sorted_options) {
                let sid = "100" + option[0].split('#')[0];
                let sname = option[0].split('#')[1] + "_dtw1";
                if(option[1] > 0.5 && !added_sid.includes(sid) && remain_cnt > 0) {
                    try {
                        //let resp = await ApiConnector.add_strategy(candidate.account, symbol, `${symbol}-${exchange}`, sid, sname, "61", ap_size, "AP_GATLING");
                        console.log(candidate.account, symbol, "AddMoreByPnl", sid, sname, JSON.stringify(1));
                        success_num += 1
                        added = true

                    }catch (e) {
                        console.error("add_strategy", e);
                    }
                    remain_cnt--;
                    added_sid.push(sid);
                    break;
                }
            }
            if (!added) {
                acc_looped -= 1;
                break
            }
        }
    }
    return [num - success_num, acc_looped]
}

async function RemoveStrategy(symbol, account, sid) {
    try {
        let resp = await ApiConnector.delete_strategy(account, symbol, sid);
        console.log(account, symbol, "delete", JSON.stringify(1));
    }catch (e) {
        console.error("remove_strategy", e);
    }
}

async function ShutdownStrategies(symbol, account, sname, stype=61) {
    try {
        let resp = await ApiConnector.update_strategy(account, symbol, sname, '', 0, 0, undefined, stype, "AP", true);
        console.log(account, symbol, "shutdown", sname, JSON.stringify(resp.success));
    }catch (e) {
        console.error("shutdown_strategy", e);
    }
}


async function SwitchStrategies(symbol, pending_strats, strat_options, default_size) {
    let sorted_options = strat_options.sort((a, b) => {
        if(a.tried_times < b.tried_times) {
            return a.rank - b.rank;
        } else {
            return a.tried_times - b.tried_times;
        }
    });
    let switched_sid = [];
    let success_num = 0;
    for(let strat of pending_strats) {
        for(let option of sorted_options) {
            if(option.tried_times < 2 && !switched_sid.includes(option.id)) {
                try {
                    //let resp = await ApiConnector.update_strategy(strat.account, symbol, strat.sname, undefined, default_size, "100" + option.id, "61", "AP_SWITCH", true);
                    console.log(strat.account, symbol, "switch", option.id, option.name, JSON.stringify(1));
                    success_num += 1

                }catch (e) {
                    console.error("switch_strategy", e);
                }
                switched_sid.push(option.id);
                break;
            }
        }
    }
    return pending_strats.slice(success_num)
}

async function SwitchStrategiesByPnl(symbol, pending_strats, strat_options, default_size) {
    let sorted_options = Object.keys(strat_options).map(function(key) {
        return [key, strat_options[key]]
    });
    sorted_options.sort(function(a, b) {
        return b[1] - a[1]
    })
    let switched_sid = [];
    let success_num = 0;
    for(let strat of pending_strats) {
        for(let option of sorted_options) {
            let sid = "100" + option[0].split('#')[0];
            let sname = option[0].split('#')[1] + "_dtw1";
            if(option[1] > 0.5 && !switched_sid.includes(sid)) {
                try {
                    //let resp = await ApiConnector.update_strategy(strat.account, symbol, strat.sname, undefined, default_size, sid, "61", "AP_SWITCH", true);
                    console.log(strat.account, symbol, "switchByPnl", sid, sname, JSON.stringify(1)); //resp
                    success_num += 1

                }catch (e) {
                    console.error("switch_strategy", e);
                }
                switched_sid.push(sid);
                break;
            }
        }
    }
    return pending_strats.slice(success_num)
}

async function ClearStrategyForSpot(snapshot) {
    let [symbol, account, sid, sname, stype, size,
        price, curbal, rl] = [snapshot.symbol, snapshot.account, snapshot.sid, snapshot.strategy, snapshot.stype, snapshot.size,
        snapshot.lastTradePrice, snapshot.curbal, snapshot.riskLimit];
    console.log(rl, size, curbal, price, Math.abs(Math.abs(curbal - rl) * price) < 5)
    if (rl == 0 && size == 0 && Math.abs(Math.abs(curbal - rl) * price) < 5) {
        await RemoveStrategy(symbol, account, sid);
    } else {
        await ShutdownStrategies(symbol, account, sname, stype);
    }
}

module.exports = {
    AddStrategyWithTSID,
    AddPassthrough,
    AddStrategy,
    AddMoreStrategies,
    AddMoreStrategiesByPnl,
    RemoveStrategy,
    ShutdownStrategies,
    SwitchStrategies,
    SwitchStrategiesByPnl,
    ClearStrategyForSpot
};


