const accountService = require("./AccountService");
const stratLuciferService = require("./StrategyLuciferService");
const strategyRuntimeSerive = require("./StrategyRuntimeService");
const strategyOpsService = require("./StrategyOpsService");
const {handLogger} = require("./LogService");
const ApiConnector = require("../utils/api_connector");

async function transfer_account_norl(account, assigned_new_account=undefined) {
    let prdLine = account.replace(/\d/g, '');
    let exchange = accountService.prdLine2Exchange(prdLine);
    let candidates = []
    try {
        let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate();
        if (prdLine === 'BGS' || prdLine === 'MCS') {
            let tmp_candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
            let available_accs = accountService.get_available_accs(prdLine, account);
            for (let acc of tmp_candidates) {
                if (available_accs.includes(acc)) {
                    candidates.push(acc)
                }
            }
        }
        let isFrozen = await accountService.isAccountBL(account);
        if (isFrozen && !assigned_new_account) {
            console.log('Frozen Skip', account)
            return;
        }
        let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
        let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
        candidates = await accountService.GetCandidateStatsSortedByVol(candidates, accDetail.ttnov);
        for (let candidate of candidates) {
            let new_account = candidate.account;
            if (assigned_new_account && new_account !== assigned_new_account) {
                console.log('Skip Unassigned Account: ', new_account);
                continue;
            }
            let ttnov = accDetail.ttnov[new_account];
            let new_account_accpnl = accDetail.balance[new_account];
            if (prdLine === 'MCS') {
                let rc = accountService.isMCRiskControl(account, accDetail.ordCnt[new_account], accDetail.dealCnt[new_account], ttnov);
                if (rc || ttnov > accountService.MCSPOT_MAX_VOL * 0.7) {
                    console.log('Skip Heavy/RC Account: ', new_account, ttnov);
                    continue;
                }
            }
            let this_strats = await accountService.getAccountStrategies(account, false);
            let oppo_strats = await accountService.getAccountStrategies(new_account, false);
            console.log('this_strats', JSON.stringify(this_strats.map(obj => [obj.account, obj.symbol])));
            console.log('oppo_strats', JSON.stringify(oppo_strats.map(obj => [obj.account, obj.symbol])));
            let curr_snap = this_strats[0];
            let [symbol, sname, sid, stype, net] = [curr_snap.symbol, curr_snap.strategy, curr_snap.sid, curr_snap.stype, curr_snap.net];
            if (new_account in accountService.MCS_ACC_SYM_BLK) {
                let blocked_syms = accountService.MCS_ACC_SYM_BLK[new_account];
                if (blocked_syms.includes(symbol)) {
                    console.log('Symbol_Restricted in This Account', new_account, symbol);
                    continue;
                }
            }
            if (ttnov > 0 && net < new_account_accpnl) {
                console.log('PNL not good: ', new_account, ttnov, net, new_account_accpnl);
                continue;
            }
            for (let info of this_strats) {
                await strategyOpsService.ClearStrategyForSpot(info);
            }
            let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
            let resp1 = await strategyOpsService.AddStrategyWithTSID(new_account, symbol, exchange, sid, sname, stype, ap_size, "RC_SWITCH");
            console.log(new_account, symbol, "Add", sid, sname, ap_size, JSON.stringify(resp1));
            await accountService.setAccountDEALT(new_account);
            for (let info of oppo_strats) {
                await strategyOpsService.ClearStrategyForSpot(info);
            }
            break;
        }
        await accountService.setAccountBL(account);
    } catch (err) {
        throw new Error("Error: " + err)
    }
    return true
}

async function switch_account(account, assigned_new_account=undefined) {
    console.log("Loading running stats");
    let prdLine = account.replace(/\d/g, '');
    let exchange = accountService.prdLine2Exchange(prdLine);
    let candidates = []
    let success = false;
    try {
        let isSwitched = await accountService.isAccountDEALT(account);
        let isFrozen = await accountService.isAccountBL(account);
        if ((isSwitched || isFrozen) && !assigned_new_account) {
            handLogger.info('Frozen Skip', account, isSwitched, isFrozen);
            console.log('Frozen Skip', account, isSwitched, isFrozen)
            return;
        }
        let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate();
        if (prdLine === 'BGS') {
            let tmp_candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
            let available_accs = accountService.get_available_accs(prdLine, account);
            for (let acc of tmp_candidates) {
                if (available_accs.includes(acc)) {
                    candidates.push(acc)
                }
            }
        }
        let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
        let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
        let ex_gui_name = accountService.acc2guiExchange(prdLine);
        let this_account_stats = pnl_summary['account_detail'][ex_gui_name][account];
        if (assigned_new_account && !candidates.includes(assigned_new_account)) {
            candidates.push(assigned_new_account);
        }
        let candidates_limited = await accountService.GetCandidateStatsForLimited(candidates, accDetail.ordCnt, accDetail.dealCnt,
            accDetail.tRbt, accDetail.utilRatio, accDetail.usdFornew, accDetail.rlNotional);
        for (let candidate of candidates_limited) {
            let new_account = candidate.account;
            if (assigned_new_account && new_account !== assigned_new_account) {
                console.log('Skip Unassigned Account: ', new_account);
                continue;
            }
            if (this_account_stats.rlNotional * 2 > (candidate.rlNotional * 2 + candidate.usdFornew) && !assigned_new_account) {
                console.log('Skip: new account insufficient money for switch', new_account, this_account_stats.rlNotional*2, (candidate.rlNotional*2 + candidate.usdFornew));
                continue;
            }
            let this_strats = await accountService.getAccountStrategies(account, true);
            let oppo_strats = await accountService.getAccountStrategies(new_account, true);
            console.log('this_strats', JSON.stringify(this_strats.map(obj => [obj.account, obj.symbol])));
            console.log('oppo_strats', JSON.stringify(oppo_strats.map(obj => [obj.account, obj.symbol])));
            let this_symbols = []
            for (let info of this_strats) {
                let [symbol, sname, stype, risklimit] = [info.symbol, info.strategy, info.stype, info.riskLimit];
                if (risklimit > 0) {
                    this_symbols.push(symbol);
                    let asset = symbol.slice(0,3);
                    let switched = await stratLuciferService.SwitchRiskLimit(account, asset, new_account);
                    if (switched) {
                        await strategyOpsService.AddPassthrough(symbol, new_account, exchange);
                        await strategyOpsService.ShutdownStrategies(symbol, account, sname, stype);
                        success = true;
                    }
                }
            }
            console.log('this_symbols: ', this_symbols);
            for (let info of oppo_strats) {
                let [symbol, sname, stype, risklimit] = [info.symbol, info.strategy, info.stype, info.riskLimit];
                let asset = symbol.slice(0,3);
                if (this_symbols.includes(symbol)) {
                    console.log(`FAILED TO SWITCH: Symbol Enabled in Both Account. RiskLimit: ${risklimit}`)
                    continue
                }
                if (risklimit > 0) {
                    let switched = await stratLuciferService.SwitchRiskLimit(new_account, asset, account);
                    if (switched) {
                        await strategyOpsService.AddPassthrough(symbol, account, exchange);
                        await strategyOpsService.ShutdownStrategies(symbol, new_account, sname, stype);
                        success = true;
                    }
                }
            }
            if (success) {
                await accountService.setAccountBL(new_account);
            }
            break;
        }
    } catch (err) {
        throw new Error("Error: " + err)
    }
}

async function transfer_account(account, assigned_new_account=undefined) {
    console.log("Loading running stats");
    let prdLine = account.replace(/\d/g, '');
    let exchange = accountService.prdLine2Exchange(prdLine);
    let candidates = []
    let success = false;
    try {
        let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate();
        if (prdLine === 'BGS') {
            let tmp_candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
            let available_accs = accountService.get_available_accs(prdLine, account);
            for (let acc of tmp_candidates) {
                if (available_accs.includes(acc)) {
                    candidates.push(acc)
                }
            }
        }
        let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
        let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
        let ex_gui_name = accountService.acc2guiExchange(prdLine);
        let this_account_stats = pnl_summary['account_detail'][ex_gui_name][account];
        let candidates_limited = await accountService.GetCandidateStatsForLimited(candidates, accDetail.ordCnt, accDetail.dealCnt,
            accDetail.tRbt, accDetail.utilRatio, accDetail.usdFornew, accDetail.rlNotional);
        for (let candidate of candidates_limited) {
            let new_account = candidate.account;
            if (!assigned_new_account) {
                if (this_account_stats.rlNotional * 2 > candidate.usdFornew) {
                    console.log('Skip: new account insufficient money for switch', new_account, this_account_stats.rlNotional, candidate.usdFornew);
                    continue;
                }
            } else {
                if (new_account !== assigned_new_account) {
                    console.log('Skip Unassigned Account: ', new_account);
                    continue;
                }
            }
            let this_strats = await accountService.getAccountStrategies(account, true);
            let oppo_strats = await accountService.getAccountStrategies(new_account, true);
            if (oppo_strats.length + this_strats.length > 3 && !assigned_new_account) { //prevent switch to account with too many strategies
                console.log('Failed to switch with too many strats: ', new_account, oppo_strats.length, this_strats.length)
                continue;
            }
            console.log('this_strats', JSON.stringify(this_strats.map(obj => [obj.account, obj.symbol])));
            for (let info of this_strats) {
                let [symbol, sname, stype, risklimit] = [info.symbol, info.strategy, info.stype, info.riskLimit];
                let asset = symbol.slice(0,3);
                if (risklimit > 0) {
                    let switched = await stratLuciferService.SwitchRiskLimit(account, asset, new_account);
                    if (switched) {
                        await strategyOpsService.AddPassthrough(symbol, new_account, exchange);
                        await strategyOpsService.ShutdownStrategies(symbol, account, sname, stype)
                        success = true;
                    }
                }
            }
            if (success) {
                await accountService.setAccountBL(new_account);
            }
            break;
        }
    } catch (err) {
        throw new Error("Error: " + err)
    }
    return success
}

async function transfer_asset(account, asset, assigned_account) {
    console.log("Loading running stats");
    let prdLine = account.replace(/\d/g, '');
    let exchange = accountService.prdLine2Exchange(prdLine);
    asset = asset.toUpperCase();
    let symbol = asset + 'USD'
    let candidates = []
    try {
        let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate()
        if (prdLine === 'BGS') {
            let tmp_candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
            let available_accs = accountService.get_available_accs(prdLine, account);
            for (let acc of tmp_candidates) {
                if (available_accs.includes(acc)) {
                    candidates.push(acc)
                }
            }
        }
        let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)})
        let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine)
        let candidates_limited = await accountService.GetCandidateStatsForLimited(candidates, accDetail.ordCnt, accDetail.dealCnt,
            accDetail.tRbt, accDetail.utilRatio, accDetail.usdFornew, accDetail.rlNotional);
        for (let candidate of candidates_limited) {
            console.log(candidate);
            let new_account = candidate.account
            if (assigned_account) {
                new_account = assigned_account
            }
            let res = await stratLuciferService.SwitchRiskLimit(account, asset, new_account);
            if (res === 0) {
                continue;
            }
            await strategyOpsService.AddPassthrough(symbol, new_account, exchange);
            let curr_snap = await accountService.getAccountStrategies(account, false, symbol);
            let [sname, stype] = [curr_snap.strategy, curr_snap.stype];
            await strategyOpsService.ShutdownStrategies(symbol, account, sname, stype)
            break;
        }
    } catch (err) {
        throw new Error("Error: " + err)
    }
}



module.exports = {
    transfer_account_norl,
    transfer_asset,
    transfer_account,
    switch_account
};
