function ClassifySymbols(cumPnlList, openStatList, accStats, prevCumPnlset, win_thres = 50, active_thes = 100, inactive_thres = 30) {
    let active_win = {};
    let active_loss = {};
    let inactive_win = {};
    let inactive_loss = {};
    let recent_active = {};
    let need_remove = {}


    let winSet = {};
    let plainSet = {};
    let lossSet = {};
    let allSet = {};


    for(let cp of cumPnlList) {
        if(cp.cumPnl > win_thres) {
            winSet[cp.symbol] = cp;
        } else if(cp.cumPnl < -win_thres / 2) {
            lossSet[cp.symbol] = cp;
        } else {
            plainSet[cp.symbol] = cp;
        }
        allSet[cp.symbol] = cp;
    }

    for(let open of openStatList) {
        let symbol = open.name.split("-")[0];
        let acc_stat = accStats[symbol]
        let active_cnt = acc_stat ? acc_stat.active_cnt : 0
        let inactive_cnt = acc_stat ? acc_stat.inactive_cnt : 0
        let off_strategies = acc_stat ? acc_stat.off_strategies : 0
        let last_strategy_offtime = acc_stat ? acc_stat.last_strategy_offtime : 0
        let no_acc_running = active_cnt + inactive_cnt === 0


        let recent_pnl = prevCumPnlset.hasOwnProperty(symbol) && allSet.hasOwnProperty(symbol) ? allSet[symbol].cumPnl - prevCumPnlset[symbol] : allSet.hasOwnProperty(symbol) ? allSet[symbol].cumPnl : 0

        if(open.daily > active_thes) {
            if(winSet[symbol]) {
                if (off_strategies < active_cnt && recent_pnl > 0) {
                    active_win[symbol] = {
                        cumPnl: winSet[symbol].cumPnl,
                        daily: open.daily,
                        hourly: open.hourly
                    }
                } else {
                    console.log('enough strategies: ', symbol, active_cnt, off_strategies, recent_pnl)
                }

            } else if(lossSet[symbol]){
                if (recent_pnl < 0) {
                    active_loss[symbol] = {
                        cumPnl: lossSet[symbol].cumPnl,
                        daily: open.daily,
                        hourly: open.hourly
                    }
                } else {
                    console.log('recent moneymaker: ', symbol,  recent_pnl)
                }

            }
        } else if(open.daily < inactive_thres){
            if(winSet[symbol]) {
                if (off_strategies < active_cnt ) {
                    inactive_win[symbol] = {
                        cumPnl: winSet[symbol].cumPnl,
                        daily: open.daily,
                        hourly: open.hourly
                    }
                } else {
                    console.log('enough strategies: ', symbol, active_cnt, off_strategies)
                }
            } else if(lossSet[symbol]){
                if (recent_pnl < 0) {
                    inactive_loss[symbol] = {
                        cumPnl: lossSet[symbol].cumPnl,
                        daily: open.daily,
                        hourly: open.hourly
                    }
                }

            }
        } else if(open.hourly > active_thes) {
            let cumPnl = 0;
            if(allSet[symbol]) {
                cumPnl = allSet[symbol].cumPnl;
            }
            recent_active[symbol] = {
                cumPnl,
                daily: open.daily,
                hourly: open.hourly
            }
        }
        if (lossSet[symbol]) {
            if (active_cnt === 0 && inactive_cnt + off_strategies > 6) {
                need_remove[symbol] = {
                    cumPnl: lossSet[symbol].cumPnl,
                    num_remove: Math.ceil(inactive_cnt * 0.7),
                    daily: open.daily,
                    hourly: open.hourly
                }
            } else if (inactive_cnt > active_cnt) {
                need_remove[symbol] = {
                    cumPnl: lossSet[symbol].cumPnl,
                    num_remove: Math.ceil((inactive_cnt) * 0.4),
                    daily: open.daily,
                    hourly: open.hourly
                }
            }
        }
    }

    return {
        active_win, active_loss, inactive_win, inactive_loss, recent_active, need_remove
    }
}


module.exports = {ClassifySymbols};

