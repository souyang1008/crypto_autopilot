const strategyRuntimeSerive = require("./services/StrategyRuntimeService");
const strategyAdvisorService = require("./services/StrategyAdvisorService");
const strategyOpsService = require("./services/StrategyOpsService");
const stratLuciferService = require("./services/StrategyLuciferService");
const accountService = require("./services/AccountService");
const {handLogger} = require("./services/LogService");


async function add_more(prdLine, symbol, tom, add_num, force) {
    console.log("Loading running stats");
    let stratByAccount = await stratLuciferService.LoadAllRunningStrategy();
    let exchange = accountService.prdLine2Exchange(prdLine);
    let candidates = []
    try {
        candidates = accountService.GetAccountCandidates(prdLine, symbol);
    } catch (err) {
        if (prdLine === 'BNBC') {
            let candiatesforCoinbased = await accountService.GetCandiatesforCoinbased(prdLine);
            candidates = candiatesforCoinbased[symbol.slice(0,3).toUpperCase()]
        } else {
            let candidateByRiskLimit = await stratLuciferService.LoadAllRiskLimitCandidate()
            candidates = candidateByRiskLimit[`${prdLine}-${symbol}`]
            if (prdLine === 'BGS' || prdLine === 'MCS') {
                candidates = candidateByRiskLimit[`${prdLine}-USDUSD`]
                if (prdLine === 'MCS') {
                    let available_accs = accountService.get_available_accs(prdLine);
                    let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { handLogger.info(err)});
                    let accDetail = strategyRuntimeSerive.fetchAccSummary(pnl_summary, prdLine);
                    candidates = await accountService.GetCandidateStatsSortedByVol(candidates, accDetail.ttnov);
                    candidates = candidates.filter(info => available_accs.includes(info.account) && info.turnover < 1 ).map(info => info.account);
                }
            }
            if(!candidates) {
                throw new Error("no account group: " + prdline)
            }
        }
    }
    let stats = accountService.GetCandidateStats(candidates, stratByAccount);
    let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);

    let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
    let prefered_strats = `${tom}_rank_sorted`
    if (ap_size === 1 || force) {
        prefered_strats = `${tom}_pure_batch`
        ap_size = -1
    }

    let strats_candidates = pb_ranks[prefered_strats];
    if(strats_candidates.length > 0) {
        await strategyOpsService.AddMoreStrategies(symbol, stats, add_num, pb_ranks[prefered_strats], exchange, ap_size);
    } else {
        console.log("no strats candidates", symbol, prefered_strats);
    }

}

let [_, program, prdline, symbol, tom, add_num, force] = process.argv;
if (tom === 't' || tom === 'taker') {
    tom = 'taker'
} else {
    tom = 'maker'
}
add_more(prdline, symbol, tom, Number(add_num), force).then(() => {
    console.log("done");
    process.exit(0);
});
