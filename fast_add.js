const strategyRuntimeSerive = require("./services/StrategyRuntimeService");
const strategyAdvisorService = require("./services/StrategyAdvisorService");
const strategyOpsService = require("./services/StrategyOpsService");
const stratLuciferService = require("./services/StrategyLuciferService");
const accountService = require("./services/AccountService");


async function add_one(pointed_account, symbol, tom, rank, force, zero) {
    console.log("Loading running stats");
    let prdLine = accountService.get_ex(pointed_account);
    let exchange = accountService.prdLine2Exchange(prdLine);
    let pb_ranks = await strategyAdvisorService.GetStrategyByPbRank(exchange, symbol);
    let ap_size = await strategyRuntimeSerive.getApSize(prdLine, symbol);
    let prefered_strats = `${tom}_rank_sorted`;
    if (force) {
        prefered_strats = `${tom}_pure_batch`;
    }
    if (ap_size === 1 || !ap_size) {
        ap_size = -1
    }
    if (zero !== undefined) {
        ap_size = 0
    }

    let strats_candidates = pb_ranks[prefered_strats];
    if (rank>1) {strats_candidates = strats_candidates.slice(rank)}
    if (zero !== undefined) {
        await strategyOpsService.AddPassthrough(symbol, pointed_account, exchange);
    } else if(strats_candidates.length > 0) {
        await strategyOpsService.AddStrategy(symbol, pointed_account, strats_candidates, exchange, ap_size);
    } else {
        console.log("no strats candidates", symbol, prefered_strats);
    }

}

let [_, program, prdline, symbol, tom, rank, force, zero] = process.argv;
if (tom === 't' || tom === 'taker') {
    tom = 'taker'
} else {
    tom = 'maker'
}
add_one(prdline, symbol, tom, rank, force, zero).then(() => {
    console.log("done");
    process.exit(0);
});
