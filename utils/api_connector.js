const axios = require("axios");
const web_api_address = "http://47.57.5.75:28888";
const app_api_address = "http://47.57.5.75:27778";
const apiConfig = require("../config/user");
const queryString = require("query-string");

const webApiClient = axios.create({
    timeout: 10000,
    baseURL: web_api_address
});

const appApiClient = axios.create({
    timeout: 10000,
    baseURL: app_api_address
});


let headers = {
    'Content-Type': 'application/json'
};

async function fetch_open_stats() {
    let resp = await webApiClient.request({
        method: "GET",
        url: "/report/open_stats"
    });
    return resp.data;
}

async function fetch_accounts() {
    let resp = await webApiClient.request({
        method: "GET",
        url: "/strategy/accounts"
    });
    return resp.data;
}


async function get_risklimit(account, asset) {
    let resp = await appApiClient.request({
        method: "GET",
        url: "/risklimit/account/" + account,
        params: {
            asset,
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function get_acc_risklimit(account) {
    let resp = await appApiClient.request({
        method: "GET",
        url: "/risklimit/account" + account,
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function update_risklimit(account, asset, limit, debt=0) {
    if (asset.length !== 3) {
        throw new Error("Unknown Asset: " + asset);
    }
    account = account.toUpperCase();
    asset = asset.toUpperCase();
    let body = {
        account,
        asset,
        limit,
        debt
    };
    let resp = await appApiClient.request({
        method: "POST",
        url: "/risklimit/account",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}
async function relaunch(host) {
    let body = {
        host,
        user: apiConfig.User,
        ts: Date.now()
    }
    let resp = await appApiClient.request({
        method: "POST",
        url: "/aegis/relaunch",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    console.log('body', JSON.stringify(body))
    return resp.data;
}
async function update_strategy(account, symCont, strategyName, sid, size_before, size_after, strategyIdAfter, missileType, trigger, force) {
    let body = {
        account,
        symCont,
        size_before,
        size_after,
        sid,
        strategyName,
        strategyIdAfter,
        missileType,
        trigger,
        force,
        user: apiConfig.User,
    };
    let resp = await webApiClient.request({
        method: "POST",
        url: "/strategy",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function update_size(account, symCont, strategyName, size_before, size_after, force) {
    let body = {
        account,
        symCont,
        size_before,
        size_after,
        strategyName,
        force,
        user: apiConfig.User,
    };
    let resp = await webApiClient.request({
        method: "POST",
        url: "/strategy",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function add_strategy(account, symbol, target, tsid, sid, sname, stype, size, trigger, deps=undefined) {
    let body = {
        account,
        symbol,
        target,
        tsid,
        sid,
        sname,
        stype,
        size,
        trigger,
        user: apiConfig.User
    };
    if (deps) {
        body.deps = deps
    }
    let resp = await webApiClient.request({
        method: "POST",
        url: "/strategy/new",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}


async function update_acc_stats(account, riskcontrol) {
    let body = {
        account,
        riskcontrol,
        user: apiConfig.User
    };
    let resp = await webApiClient.request({
        method: "POST",
        url: "/strategy/accrl",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function update_launcher_status(account, status) {
    let body = {
        account,
        status,
        user: apiConfig.User
    };
    let resp = await webApiClient.request({
        method: "POST",
        url: "/strategy/launcher",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}


async function delete_strategy(account, symCont, sid) {
    let body = {
        account,
        symCont,
        sid
    };
    let resp = await webApiClient.request({
        method: "POST",
        url: "/strategy/del",
        data: JSON.stringify(body),
        params: {
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function get_strategy_by_pb(exchange, symbol) {
    let resp = await webApiClient.request({
        method: "GET",
        url: "/strategy/pb",
        params: {
            exchange,
            symbol,
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

async function get_strategy_by_pnl(exchange, symbol) {
    let resp = await webApiClient.request({
        method: "GET",
        url: "/strategy/sized_pnl",
        params: {
            exchange,
            symbol,
            jwt_token: apiConfig.Token
        },
        headers
    });
    return resp.data
}

module.exports = {
    fetch_accounts,
    fetch_open_stats,
    add_strategy,
    relaunch,
    delete_strategy,
    update_strategy,
    update_size,
    update_acc_stats,
    update_launcher_status,
    update_risklimit,
    get_risklimit,
    get_acc_risklimit,
    get_strategy_by_pb,
    get_strategy_by_pnl
};