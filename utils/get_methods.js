const ex_mapping = {'OKC': 'okcoin', 'HB': 'huobi', 'BNB': 'binance',
    'OKCS': 'okcoin', 'OKCF': 'okcoin', 'HBF': 'huobi', 'BNBF': 'binance',
    'HBS': 'huobi', 'BNBC': 'binance', 'BYBF': 'bybit', 'BYBS': 'bybit', 'BGF': 'bitget', 'BGS': 'bitget',
    'FTXF': 'ftx', 'FTX': 'ftx', "GTF": 'gate', 'GT': 'gate', 'KCF': 'kucoin', 'KCS': 'kucoin', 'EXF': 'ex3', 'HCF': 'hotcoin',
    'MCF': 'mexc',  'MCS': 'mexc', 'CWF': 'coinw', 'WXF': 'weex', 'DCF': 'deepcoin', 'XTF': 'xt', 'LBF': 'lbank', 'BMF': 'bitmart'
}
const net_mapping = {'fut': 'netUsdt', 'spot': 'net'}
const time_mapping = {'fut': 'updatedTime', 'spot': 'time'}
const mid_mapping = {'fut': 'marketPrice', 'spot': 'mid'}
const comm_mapping = {'fut': 'commission', 'spot': 'comm'}
const making_accs = ['HBF3007', 'BNBF607', 'BNB607', 'BNBC607', 'GTF107', 'GT107']
const yaml = require('js-yaml');
const request = require('request');
const fs = require('fs');
const local_config = yaml.safeLoad(fs.readFileSync('config/rules.yml', 'utf8'));
const userConfig = require("../config/user");
const {midLogger} = require('../services/LogService');
const {promisifyRedis, getRedis} = require("../store/redis");
const rClient = promisifyRedis(getRedis("app"));
const rRawClient = promisifyRedis(getRedis("raw"));

let avg_pnl = {}
let avg_size = {}
let cum_pnl = {}

function get_ex(account) {
    let exchange = account.replace(/\d/g, '');
    if (exchange.endsWith('R')) {
        exchange = exchange.slice(0,-1)
    }
    return exchange
}

function get_face_value(size, meta, symbol, account, midPrice, last, logger) {
    let face_value = size;
    let exchange =get_ex(account);
    let meta_key = `${symbol}-${ex_mapping[exchange].toUpperCase()}`
    let price = midPrice ? midPrice : last;
    if(meta[meta_key]) {
        if (! price) {
            if ('price' in meta[meta_key]) {
                price = meta[meta_key]['price']
            } else {
                logger.info("Unable to get price", meta_key);
                return 0
            }
        }
        face_value = face_value * meta[meta_key]['unit'] / (meta[meta_key]['scale'] || 1);
        if (!meta[meta_key]['inverse']) {
            face_value = face_value * meta[meta_key]['min_deal_size'] * Number(price);
        }
    } else {
        logger.info('meta_key error', symbol, account, size, meta_key)
        logger.info("UNKNOWN_META_KEY", meta_key);
    }
    if (face_value || face_value === 0) {
        return Number(face_value)
    } else {
        logger.info("FAILED TO GET FV", meta_key, face_value);
        return 0
    }
}

function get_size_from_face_value(meta, symbol, exchange, mid_price, last_price, face_value, logger) {
    let size = face_value
    let meta_key = `${symbol}-${ex_mapping[exchange].toUpperCase()}`
    let price = mid_price ? mid_price : last_price;
    size = size / meta[meta_key]['unit'] * (meta[meta_key]['scale'] || 1);
    if (meta[meta_key]) {
        if (! price) {
            if ('price' in meta[meta_key]) {
                price = meta[meta_key]['price']
            } else {
                logger.info("Unable to get price", meta_key);
                return 0
            }
        }
        if (! meta[meta_key]['inverse']) {
            size = size / Number(price) / meta[meta_key]['min_deal_size']
        }
    } else {
        logger.info('meta_key error2', symbol, exchange, size, meta_key)
        logger.info("UNKNOWN_META_KEY", meta_key);
    }
    return Math.ceil(size)
}

function get_sym(acc_key){
    let [acc, sym] = acc_key.split('#')
    acc = acc.replace(/\d/g, '');
    return acc + '#' + sym
}

async function get_keys(key_p, logger=midLogger) {
    let res = await rClient.keysAsync(key_p);
    if (res.length === 0) {
        logger.info(`ERROR: pattern ${key_p} not found in redis`)
    }
    return res
}

async function getRedisJson(key, logger=midLogger) {
    let res = await rClient.getAsync(key);
    if(res) {
        return JSON.parse(res);
    } else {
        if (key.startsWith('AP_REC')) {
            let [_, exchange, symbol] = key.split('#')
            let save_info = JSON.stringify(local_config[exchange][symbol])
            if (save_info) {
                await rClient.setAsync(key, save_info);
            } else {
                logger.info(`save info undefined for REDIS: ${exchange}-${symbol}`)
            }
        }
    }
    return undefined
}

async function get_num_accs(ex, sym, logger) {
    let match_p = `CRYPTO_SNAP[RS][^M]*${ex}*#${sym}*`
    let alive_cnt = 0
    let inactive_cnt = 0
    let inactive_info = []
    let off_strategies = 0
    let last_strategy_offtime = 0
    let sym_strats = await rClient.keysAsync(match_p);
    for (let k_i=0; k_i< sym_strats.length; k_i++) {
        let key = sym_strats[k_i]
        let [acc, sname] = [key.split('#')[1], key.split('#')[3]]
        if (key.endsWith("NotFound00000") || making_accs.includes(acc)) {
            continue
        }
        try {
            let snapshot = JSON.parse(await rClient.getAsync(key));
            if (snapshot['enabled'] === true) {
                alive_cnt += 1
                let net = snapshot['is_fut'] ? snapshot[net_mapping['fut']] : snapshot[net_mapping['spot']]
                if (snapshot['size'] === 0 && parseInt(net) === 0) {
                    inactive_cnt +=1
                    inactive_info.push({'acc': acc, 'sname': sname})
                }
            } else {
                off_strategies += 1
                let strategy_offtime = snapshot['is_fut'] ? Number(snapshot[time_mapping['fut']]) : Number(snapshot[time_mapping['spot']]);
                if (strategy_offtime > last_strategy_offtime) {
                    last_strategy_offtime = strategy_offtime
                }
            }
        } catch(err) {
            logger.info(`get_num_accs: KEY ERROR: ${key}, skipped with msg: ${err.message}`)
        }
    }
    return [alive_cnt, [off_strategies, last_strategy_offtime], [inactive_cnt, inactive_info]]
}


function get_dw_key(base_key, exchange, out_type) {
    let res = base_key
    if (! base_key.includes('dtw')) {
        if (ex_mapping[exchange].toUpperCase()==='BINANCE') {
            res =  '400' + base_key + '_dtw05'
        } else {
            res =  '100' + base_key + '_dtw1'
        }
    }
    if (out_type === 'name') {
        return res.split('#')[1]
    } else if (out_type === 'id') {
        return res.split('#')[0]
    } else {
        return res
    }
}

function get_raw_key(base_key, out_type) {
    let res = base_key
    if (out_type === 'name') {
        if (base_key.includes('dtw')) {
            res = res.split('_dtw')[0]
            return res
        }
    } else if (out_type === 'id') {
        if (base_key.length > 9 && (base_key.startsWith('100') || base_key.startsWith('400'))) {
            res = res.slice(3)
            return res
        }
    } else {
        return ''
    }
}

function get_threshold(thresholdInfo, scales, group, face_value, logger) {
    try {
        thresholdInfo['points'][group]
    } catch(err) {
        logger.info('ERROR: unable to get group in ', thresholdInfo['points'], scales, group, face_value)
    }
    let points = thresholdInfo['points'][group]
    let mi = thresholdInfo['mi']
    let scale = scales[group]
    let upper = points[1]
    let lower = points[0]
    let upper_scale = scale[1]
    let lower_scale = scale[0]
    let a = (upper_scale - lower_scale) / (upper**mi - lower**mi)

    let b = lower_scale - lower**mi*a
    return face_value * (face_value**mi*a + b)
}


async function load_avg(objective) {
    let obj_api = 'default_size'
    if (objective === 'pnl') {
        obj_api = 'avg_pnl'
    }
    const get_avg_obj =  new Promise((resolve, reject) => {
        request(userConfig.Url  + '/strategy/' + obj_api, function(err, res, body) {
            if (body) {
                body = JSON.parse(body)
                resolve(body)
            } else {
                resolve(0)
            }
        })
    })
    let obj_res = await get_avg_obj
    if (obj_res) {
        if (obj_api === 'avg_pnl') {
            avg_pnl = obj_res
        } else if (obj_api === 'default_size') {
            avg_size = obj_res
        }
    }
}

function load_avg_timer(objective) {
    setInterval(() => load_avg(objective), 60*60*1000);
}

async function load_cumpnl() {
    const get_cumpnl = new Promise((resolve, reject) => {
        request(userConfig.Url + '/strategy/cum_pnl', function (err, res, body) {
            if (body) {
                body = JSON.parse(body)
                resolve(body)
            } else {
                resolve(0)
            }
        })
    })
    let obj_res = await get_cumpnl
    if (obj_res) {
        cum_pnl = obj_res
    }
}

async function get_avg_size(ex,sym) {
    if (!(ex in avg_size)) {
        console.log('Requesting default_size...')
        await load_avg('default_size')
    }
    if (avg_size[ex] !== undefined && avg_size[ex][sym] !== undefined) {
        return parseInt(avg_size[ex][sym])
    } else {
        return 0
    }

}

async function get_avg_pnl(ex,sym) {
    if (!(ex in avg_pnl) || !(sym in avg_pnl[ex])) {
        await load_avg('pnl')
    }
    try {
        return parseInt(avg_pnl[ex][sym])
    } catch(e) {
        console.log('No avg pnl: return -999', ex, sym)
        return -999
    }
}

async function get_cumpnl(ex,sym) {
    if (!(ex in cum_pnl) || !(sym in cum_pnl[ex])) {
        await load_cumpnl()
    }
    try {
        return parseInt(cum_pnl[ex][sym])
    } catch(e) {
        console.log('No cum_pnl: return -999', ex, sym)
        return -999
    }
}

async function get_open_sigs(userConfig) { //ex, is_fut,
    const get_sig_cnts =  new Promise((resolve, reject) => {
        request(userConfig['Url']  + '/report/open_stats', function(err, res, body) {
            if (!body) {
                resolve([])
            } else {
                body = JSON.parse(body)
                resolve(body)
            }
            // if (is_fut) {
            //     ex = ex.toUpperCase() + '-FUT'
            // } else {
            //     ex = ex.toUpperCase() + '-SPOT'
            // }
            // if (ex in body) {
            //     resolve(body[ex])
            // } else {
            //     resolve({})
            // }
        })
    })
    let obj_res = await get_sig_cnts
    return obj_res
}

async function getStrategyStartTime(symbol, account, strategyName, userConfig) {
    const get_stime =  new Promise((resolve, reject) => {
        request(userConfig['Url']  + '/strategy/stime?symbol='+
            symbol +'&account='+ account +'&strategy='+ strategyName +'&jwt_token=' +
            userConfig['Token'], function(err, res, body) {
            if (body) {
                resolve(JSON.parse(body))
            } else {
                resolve(0)
            }
        })
    })
    let stime = await get_stime
    return parseInt(stime)
}

async function get_pnl_rank(exchange ,symbol, userConfig) {
    try {
        const get_sorted_pnl =  new Promise((resolve, reject) => {
            request(userConfig['Url']  + '/strategy/pnl?exchange='+
                ex_mapping[exchange] +'&symbol='+ symbol +'&jwt_token=' +
                userConfig['Token'], function(err, res, body) {
                if (!body) {
                    resolve([])
                } else {
                    body = JSON.parse(body)
                    let aegis_all_list = Object.keys(body).map(function(key) {
                        return [key, body[key]]
                    });
                    aegis_all_list.sort(function(a, b) {
                        return b[1] - a[1]
                    })
                    resolve(aegis_all_list)
                }

            })
        })
        let sorted_pnls = await get_sorted_pnl
        return sorted_pnls
    } catch (e) {
        console.log(`FAILED to get_pnl_rank: ${e.message} and stack: ${e.stack}`)
        return []
    }
}


const get_batch_sort_list = (exchange ,symbol, userConfig, maker) => {
    return new Promise((resolve, reject) => {
        request(userConfig['Url']  + '/strategy/pb?exchange='+
            ex_mapping[exchange] +'&symbol='+ symbol +'&jwt_token=' +
            userConfig['Token'], function(err, res, body) {
            if (!body) {
                resolve([])
            } else {
                body = JSON.parse(body)
                let output_list = undefined
                if (maker) {
                    output_list = 'maker_pure_batch' // does not require IR score to be in the list
                } else {
                    output_list = 'taker_pure_batch'
                }
                resolve(body[output_list])
            }
        })
    })
}

async function get_newest_batch(exchange ,symbol, userConfig, maker) {
    let sorted_batches = await get_batch_sort_list(exchange ,symbol, userConfig, maker)
    for (let s of sorted_batches) {
        if (s['tried_times'] === 0) {
            return s
        }
    }
    return undefined
}

async function get_newest_good_batch(exchange ,symbol, userConfig, maker) {
    let sorted_batches = await get_batch_sort_list(exchange ,symbol, userConfig, maker)
    for (let s of sorted_batches) {
        if (s['tried_times'] === 0 && s['ir'] > 0) {
            return s
        }
    }
    return undefined
}


async function get_pb_rank(exchange ,symbol, userConfig, maker, sort_method, logger) {
    const get_pb_ranks =  new Promise((resolve, reject) => {
        request(userConfig['Url']  + '/strategy/pb?exchange='+
            ex_mapping[exchange] +'&symbol='+ symbol +'&jwt_token=' +
            userConfig['Token'], function(err, res, body) {
            logger.info(exchange, symbol, 'pb_res: ', body)
            if (!body) {
                resolve([])
            } else {
                body = JSON.parse(body)
                let output_list = undefined
                if (sort_method === 'algo') {
                    output_list = 'mid_candidate_agg'
                }
                else if (sort_method === 'batch') {
                    if (maker) {
                        output_list = 'maker_batch_sorted'
                    } else {
                        output_list = 'taker_batch_sorted'
                    }
                } else {
                    if (maker) {
                        output_list = 'maker_rank_sorted'
                    } else {
                        output_list = 'taker_rank_sorted'
                    }
                }
                logger.info('pb_selected: ', output_list, JSON.stringify(body[output_list]))
                resolve(body[output_list])
            }
        })
    })
    let sorted_pbs = await get_pb_ranks
    return sorted_pbs
}

async function get_symbol_rank(exchange, symbol) {
    let pb_score_map  = {}
    let ir_list = JSON.parse(await rRawClient.getAsync(`CRYPTO_PLAYBACK_RANK#${exchange}#${symbol}`));
    if (ir_list) {
        for (let info of ir_list) {
            pb_score_map[info['name']] = info['ir'];
        }
    }
    return pb_score_map
}

function get_lowest_pb_strategy(snaps, pb_score_map) {
    let snap_scores = []
    for (let key of snaps) {
        let strat = key.split('#')[3].split('_').slice(0,3).join('_')
        let score = pb_score_map[strat] || 999;
        snap_scores.push({key, score});
    }
    snap_scores.sort((a, b) => {
        return a.score - b.score
    })
    return snap_scores[0]['key']
}

module.exports = {
    ex_mapping,
    net_mapping,
    time_mapping,
    mid_mapping,
    comm_mapping,
    making_accs,
    get_ex,
    get_avg_size,
    get_avg_pnl,
    get_pnl_rank,
    get_pb_rank,
    get_newest_batch,
    get_newest_good_batch,
    get_dw_key,
    get_sym,
    get_threshold,
    get_face_value,
    getStrategyStartTime,
    get_size_from_face_value,
    getRedisJson,
    get_keys,
    get_num_accs,
    get_cumpnl,
    get_raw_key,
    load_avg_timer,
    get_open_sigs,
    get_symbol_rank,
    get_lowest_pb_strategy
    }