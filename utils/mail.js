const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
    host: "smtp.exmail.qq.com",
    port: "465",
    secure: true,
    auth: {
        user: 'report@berkeleybrothers.com',
        pass: '2201Full'
    }
});

function sendMail(receptions, subject, text, from_name='Reporter') {
    const mailOptions = {
        from: `"${from_name}" <report@berkeleybrothers.com>`,
        to: receptions,
        subject,
        text
    };
    return transporter.sendMail(mailOptions)
}

module.exports = sendMail;