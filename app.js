const fs = require('fs');
const request = require('request');
const redis = require('redis');
const yaml = require('js-yaml');

const dbService = require('./services/dbService');
const accountService = require("./services/AccountService");

// const database = dbService.database;

const mailer = require('./utils/mail');
const functions = require('./main/main_functions');
const strategyRouterService = require("./services/StrategyRouterService");
const update_sizes = functions.update
const reset_acc = functions.reset_acc
// const even_sizes = functions.even
const alert = functions.alert

const databaseConfig = JSON.parse(fs.readFileSync('config/database.json'));

const userConfig = JSON.parse(fs.readFileSync('config/user.json'));
const receps = JSON.parse(fs.readFileSync('config/mail.json'))

const {getRedis, promisifyRedis} = require("./store/redis");
const rClient = promisifyRedis(getRedis("app"));

const meta = yaml.safeLoad(fs.readFileSync('config/crypto_meta.yaml', 'utf8'));


const receptions = receps['receivers'];
const key_pattern = 'CRYPTO_SNAP[RS][^M]*';


function even_refresh(mailer, userConfig, settingConfig) {
    const m = even_sizes(userConfig, settingConfig, redisConn, 1.8)
    if (m === undefined) {
        console.log('update_sizes function return null')
    }
    m.then(res => {
        const subject = `[CRYPTO] WARNING: EVEN Sizes Need Attention!`;
        alert(mailer, res, receptions, subject, userConfig)
    })
}

function hb_config_init() {
    console.log('INIT HBFUT AP REC')
    const config = yaml.safeLoad(fs.readFileSync('config/rules.yml', 'utf8'));
    for (let exchange in config) {
        if (exchange.startsWith('HBS') || exchange.startsWith('HBF')) {
            for (let symbol in config[exchange]) {
                let cfg_key = `AP_REC#${exchange}#${symbol}`
                let save_info = JSON.stringify(config[exchange][symbol])
                if (save_info) {
                    redisConn.set(cfg_key, save_info, function(err, res) {
                        if (err) {
                            console.log(`FAILED TO SAVE ${cfg_key} IN REDIS`)
                        }
                    })
                } else {
                    console.log(`save info undefined for REDIS: ${exchange}-${symbol}`)
                }
            }
        }
    }
}





function Scan() {
    setTimeout(function run() {
        refresh(userConfig, meta, update_sizes, mailer).finally(() => {
            setTimeout(run, 10*1000);
        })
    }, 10*1000);
}

async function refresh(userConfig, meta, update_sizes, mailer) {
    const rec_pattern = 'AP_REC*'
    let ap_rec_keys = await rClient.keysAsync(rec_pattern);
    // console.log('ap_rec_keys: ', ap_rec_keys.length)
    if (ap_rec_keys.length < 140) {
        console.log('SKIPPING AP FOR CONFIG SETTING')
        const config = yaml.safeLoad(fs.readFileSync('config/rules.yml', 'utf8'));
        let save_info = null
        for (let exchange in config) {
            if (exchange.startsWith('BNB') || exchange.startsWith('HB') || exchange.startsWith('OKC') || exchange.startsWith('GT') || exchange.startsWith('KC')
                || exchange.startsWith('BYB') || exchange.startsWith('FTX') || exchange.startsWith('thresholdInfo')) {
                if (exchange.startsWith('thresholdInfo')) {
                    save_info = JSON.stringify(config[exchange])
                    // console.log(save_info)
                    if (save_info) {
                        redisConn.set('AP_REC#THRESHOLD_INFO', save_info, function(err, res) {
                            if (err) {
                                console.log('FAILED TO SAVE THRESHOLD_INFO IN REDIS')
                            }
                        })
                    } else {
                        console.log('save info undefined for REDIS: THRESHOLD_INFO')
                    }
                } else {
                    for (let symbol in config[exchange]) {
                        let cfg_key = `AP_REC#${exchange}#${symbol}`
                        save_info = JSON.stringify(config[exchange][symbol])
                        if (save_info) {
                            redisConn.set(cfg_key, save_info, function(err, res) {
                                if (err) {
                                    console.log(`FAILED TO SAVE ${cfg_key} IN REDIS`)
                                }
                            })
                        } else {
                            console.log(`save info undefined for REDIS: ${exchange}-${symbol}`)
                        }
                    }
                }
            }
        }
    } else {
        reset_acc();
        let error_msg = '';
        let accounts_to_shut = [];
        // let [mcs_error_msg, mcs_accounts_to_shut] = await accountService.checkUniqueEnabled('MCS');
        await accountService.checkDupToDel('MCS');
        // error_msg = mcs_error_msg;
        // accounts_to_shut = accounts_to_shut.concat(mcs_accounts_to_shut)
        let res = await update_sizes(key_pattern, userConfig, meta)
        if (res === undefined) {
            console.log('update_sizes function return null')
        }
        const subject = `[CRYPTO] WARNING: SHUTDOWN/LARGE LOSS Needs Attention!`;
        if (error_msg.length > 0) {
            res[0] = error_msg + res[0]
            for (let acc of accounts_to_shut) {
                await accountService.shutDownAccount(acc);
            }
        }
        let res2 = await alert(meta, mailer, res, receptions, subject, userConfig)
    }
}



async function setUp() {
    await accountService.LoadAllRecCfg();
    strategyRouterService.loadAccountsTimer();
    strategyRouterService.loadLauncherTimer();
    Scan();
}

setUp();

// hb_config_init()

