const fs = require("fs");
const path = require("path");

const acc_config = JSON.parse(fs.readFileSync('../config/account_config.json'));
const SYNC_DIR = path.join("/home/crypto", "lucifer", "sync");
const AEG_STRATEGY = "strategy.aeg";
const AEG_LINE_DELIMITER = "|";
const AEG_COMMENT = "#";


function loadAccCapacity(market) {
    let acc_map = {}
    walkDir(SYNC_DIR).filter(file => file.filepath.endsWith(AEG_STRATEGY)).map(file => {
        fs.readFile(path.join(file.filepath), (err, data) => {
            if (err) {
                console.error(err);
                return;
            }
            const strategy_file = data.toString().split('\n').map(line => line.trim())
                .filter(line => !line.startsWith(AEG_COMMENT) && line.length !== 0 && (!line.startsWith('999 ') || line.indexOf("999 | 8") > -1)).map(line => {
                    let fields = line.split(AEG_LINE_DELIMITER).map(f => f.trim());
                    const account = fields[4];
                    const symCont = fields[3].split('-')[0];
                    const size = Number(fields[5] || 0);
                    const id = fields[0];
                    const type = Number(fields[1]);
                    if (acc_map.hasOwnProperty(account)) {
                        acc_map[account]['sym'].push(symCont)
                        acc_map[account]['cnt'] += 1
                    } else {
                        acc_map[account] = {}
                        acc_map[account]['sym'] = [symCont]
                        acc_map[account]['cnt'] = 1
                    }
                    return {
                        id,
                        name,
                        size,
                        deps: fields[2],
                        account,
                        symCont,
                        type
                    };
                });
        });
    });
    return acc_map
}

function select_account(acc_map, acc_type, symbol, traffic='low') {
    if (acc_config.hasOwnProperty(acc_type)) {
        const symbol_mapping = acc_config[acc_type]['symbol_mapping']
        const acc_groups = acc_config[acc_type]['account_group']
        const dgroup = acc_config[acc_type]['default_group']
        let available_accs = []
        if (symbol_mapping.hasOwnProperty(symbol)) {
            for (let group of acc_config[acc_type]) {
                for (let acc of acc_groups[group]) {
                    available_accs.push(acc)
                }
            }
        } else {
            for (let acc of acc_groups[dgroup]) {
                available_accs.push(acc)
            }
        }
        let acc_map_list = Object.keys(acc_map).map(function(key) {
            return {'key': key, 'value': acc_map[key]}
        });
        let sorted_acc_map_list = acc_map_list.filter(acc_map_list => available_accs.includes(acc_map_list.key)).sort((a, b) => {
            return a.value.cnt - b.value.cnt
        })
        if (traffic === 'low') {
            return sorted_acc_map_list[0]['key']
        } else {
            return sorted_acc_map_list[Object.keys(sorted_acc_map_list).length - 1]['key']
        }
    }
}

function walkDir(dir, fileList) {
    let files = fs.readdirSync(dir);
    fileList = fileList || [];
    files.forEach( file => {
        let filepath = path.join(dir, file);
        if(isDir(filepath)){
            walkDir(filepath, fileList)
        }else{
            fileList.push({dir, file, filepath})
        }
    });
    return fileList
}

function isDir(filepath) {
    return fs.statSync(filepath).isDirectory()
}
