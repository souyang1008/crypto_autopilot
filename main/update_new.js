const fs = require('fs');
const request = require('request');
const redis = require('redis');
const strategyRuntimeSerive = require("../services/StrategyRuntimeService");
// const yaml = require('js-yaml');
// const local_config = yaml.safeLoad(fs.readFileSync('config/rules.yml', 'utf8'));

const {handLogger} = require('../services/LogService');
const {host_mapping, change_strategy, change_size} = require('./helper_functions');
const {ex_mapping, net_mapping, time_mapping, mid_mapping, making_accs,
    get_keys, getRedisJson, get_avg_size, get_avg_pnl,
    get_newest_batch, get_newest_good_batch, getStrategyStartTime,
    get_dw_key, get_face_value, get_size_from_face_value} = require('../utils/get_methods');

const userConfig = JSON.parse(fs.readFileSync('config/user.json'));
const databaseConfig = JSON.parse(fs.readFileSync('config/database.json'));
const redisConfig = databaseConfig['redis'];
const redisConn = redis.createClient({
    host: redisConfig['host'],
    port: redisConfig['port1']
});
const receps = JSON.parse(fs.readFileSync('config/mail.json'))
const receptions = receps['receivers'];

const args = process.argv;
const exchange = args[2];
let good = false
let force = false
if (args[3] === '1') {
    good = true
}
if (args.length > 4) {
    force = true
}
let batch_specific = '0'
let shut = '0'
if (args.length > 5) {
    [batch_specific, shut] = args[5].split(',')
}

//args redisConn, userConfig, meta
async function update_new_batch(good) {
    let all_texts = '';
    let symbols = await get_keys(`AP_REC#${exchange.toUpperCase()}#*`).catch((err) => { handLogger.info(err)})
    symbols.sort()

    let need_relaunch  = []
    for (let s_i = 0; s_i < symbols.length; s_i ++) {
        const cfg_key = symbols[s_i]
        const ap_rec_key = cfg_key.split("#");
        if (ap_rec_key.length !== 3) {
            handLogger.info('skip invalid ap_rec_key: ', cfg_key)
            continue
        }
        const acc_prefix = ap_rec_key[1];
        const symbol = ap_rec_key[2];
        let key_p = `CRYPTO_SNAPSHOT#${acc_prefix}*#${symbol}*`;
        let snaps = await get_keys(key_p).catch((err) => { handLogger.info(err)});
        if (snaps.length === 0) {
            console.log('unable to get snaps: ', key_p)
            continue;
        }
        let avg_size = await get_avg_size(ex_mapping[acc_prefix].toUpperCase(), symbol);
        let avg_pnl = await get_avg_pnl(ex_mapping[acc_prefix].toUpperCase(), symbol);
        let rec_size = await strategyRuntimeSerive.getApSize(acc_prefix.toUpperCase(), symbol);
        let config = await getRedisJson(cfg_key).catch((err) => handLogger.info(err));
        let pref = config[1]['preference'] === 'm';
        let lowest = 9999;
        let lowest_info = undefined;
        let new_strategy_info = await get_newest_batch(exchange ,symbol, userConfig, pref);
        if (good) {
            new_strategy_info = await get_newest_good_batch(exchange ,symbol, userConfig, pref);
        }
        if (new_strategy_info === undefined) {
            console.log('NO BATCH AVAIALBLE: ', cfg_key)
            continue;

        }
        let newest_batch = new_strategy_info['batch'];
        for (let k_i = 0; k_i< snaps.length; k_i++) {
            let key = snaps[k_i];
            if (key.endsWith("NotFound00000") || key.endsWith('PASSTHROUGH')) {continue}
            const key_splits = key.split("#");
            const account = key_splits[1];
            const strategyName = key_splits[3];
            let this_batch = strategyName.split('_')[0]
            if (batch_specific !== '0' && !good) {
                if (this_batch === batch_specific) {
                    // skip
                    console.log('SKIP Latest Batch', key)
                    continue
                }
            }
            if (making_accs.includes(account)) {
                // skip making
                console.log('skip making', account)
                continue
            }
            if (symbol.endsWith('p0')) {
                // skip p0
                console.log('skip p0', symbol)
                continue
            }

            let candidate_key = await select_lowest_key(key, account, symbol, strategyName, avg_size, avg_pnl, newest_batch, redisConn, lowest, force)
            if (candidate_key !== undefined) {
                lowest = candidate_key[0]
                lowest_info = [lowest, candidate_key[1], candidate_key[2], candidate_key[3], strategyName, account] // stype, sname
            }
        }
        if (lowest_info !== undefined) {
            let [old_net, old_size, stype, sid, strategyName, account]  = lowest_info;
            let new_size = Math.ceil(avg_size * 0.6)
            if (new_size === 0) {
                new_size = Math.ceil(rec_size * 0.4)
            }
            let strategyNameAfter = get_dw_key(new_strategy_info['key'], exchange, 'name')
            let strategyIdAfter = get_dw_key(new_strategy_info['key'], exchange, 'id')
            if (!strategyNameAfter.includes(batch_specific)) {
                console.log('No latest batch available ', lowest_info, strategyNameAfter)
                if (shut !== '0') {
                    console.log('shutdown: ', `${account}-${symbol}-${strategyName}`, old_net, old_size)
                    change_size(userConfig, account, symbol, strategyName, old_size, 0, -1 ,old_net, force, handLogger)
                }
                continue
            }
            console.log(`\n ${exchange}-${account}-${symbol} changed from ${strategyName} to ${strategyNameAfter} with size ${new_size} and avg_pnl ${avg_pnl} and old_net ${old_net}`);
            need_relaunch = await change_strategy(userConfig, exchange, symbol, account, stype, sid,
                strategyName, strategyNameAfter, strategyIdAfter, new_size, need_relaunch, host_mapping, 'aoe_update', handLogger);
        }
    }

    await new Promise(resolve => setTimeout(resolve, 30*1000));
    for (let host of need_relaunch) {
        request.post({
            url: userConfig['RelaunchUrl'] + '/aegis/relaunch?jwt_token=' + userConfig['Token'],
            form: {'host': host, 'user': userConfig['User']}
        }, function (error, response, body) {
            console.log(host, body)
            body = JSON.parse(body)
            if (!error && response.statusCode === 200 && body['status'] === 'info') {
                handLogger.info(`Successfully Relaunched <${host}>`)
                console.log((`Successfully Relaunched <${host}>`))
            } else {
                handLogger.info(`RELAUNCH FAILED ${host}, ${error}, ${body['status']}, ${body['data']}`)
                console.log((`RELAUNCH FAILED ${host}, ${error}, ${body['status']}, ${body['data']}`))
            }
        })
    }

    return 'ok'
}

async function select_lowest_key(key, account, symbol, strategyName, avg_size, avg_pnl, newest_batch, redisConn, lowest, force){
    let snapshot = await getRedisJson(key)
    if (snapshot === undefined || snapshot['enabled'] !== true) {
        return undefined
    }
    let sid = snapshot.sid;
    const old_size = parseInt(snapshot['size'])
    let net = net_mapping['spot']
    if (snapshot['is_fut'] === true) {
        net = net_mapping['fut']
    }
    const old_net = parseInt(snapshot[net])
    if (! force) {
        let startTime = await getStrategyStartTime(symbol, account, strategyName, userConfig)
        let time_past = Date.now() - startTime
        if ((old_net > avg_pnl && old_size > 0.8 * avg_size) || time_past < 1800 *1000 || (strategyName.includes(newest_batch) && old_net > 0)) {
            // do not change good strategy or new strategy
            console.log('skip good or new strategy: ', key)
            return undefined
        }
    }

    if (old_net < lowest) {
        return [old_net, old_size, snapshot['stype'], sid]
    }

    return undefined
}


update_new_batch(good).then(res => {
    console.log(res)
}).catch(err => {
    console.error(err)
});