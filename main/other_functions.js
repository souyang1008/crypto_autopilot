async function even_sizes(userConfig, settingConfig, redisConn, threshold) {
    const exs = ['BNBF', 'HBF', 'OKCF', 'OKCS', 'BNB', 'HB', 'OKC']
    let email_txt = ''
    let need_relaunch = []
    for (let i = 0; i < exs.length; i++) {
        // if (exs[i] == 'HBF') {continue}
        let syms = Object.keys(settingConfig[exs[i]])
        for (let j = 0; j < syms.length; j++) {
            if (syms[j] !== 'TOLERANCE') {
                let key_pattern = `CRYPTO_SNAPSHOT#${exs[i]}*#${syms[j]}#*`
                const get_keys = new Promise((resolve, reject) => {
                    redisConn.keys(key_pattern, function(err, crypto_keys){
                        if (!err) {
                            resolve(crypto_keys)
                        } else {
                            reject(`ERROR: pattern ${key_pattern} not found in redis`)
                        }
                    })
                })
                const crypto_keys = await get_keys.catch((err) => { console.log(err) })
                if (crypto_keys.length === 0) {
                    console.log(`WARNING: ${key_pattern} has no match in redis`)
                    continue
                }
                let sizes = []
                let snaps = []
                for (let ii =0; ii< crypto_keys.length; ii++) {
                    let k = crypto_keys[ii]
                    const get_snap = new Promise((resolve, reject) => {
                        redisConn.get(k, function(err, dic) {
                            if (err) {
                                reject(`ERROR: ${crypto_keys[ii]} NOT FOUND IN REDIS`)
                            }
                            resolve(JSON.parse(dic));
                        })
                    })
                    const snap = await get_snap.catch((err) => {console.log(err)})
                    if (snap['enabled'] === true && snap['size'] !== undefined && !snap['strategy'].startsWith('NotFound')) {
                        sizes.push(parseInt(snap['size']))
                        snaps.push(snap)
                    }
                }

                let max = null
                let max_ind = null

                for (let t =0; t< sizes.length; t++) {
                    if (sizes[t] > max) {
                        max = sizes[t]
                        max_ind = t
                    }
                }

                const sorted_sizes = sizes.concat().sort(function(a, b){ return a - b })
                if (sorted_sizes[0] * threshold < sorted_sizes[sizes.length-1] && sizes.length >0) {
                    let mean = Math.round(sizes.reduce((prev,curr) => curr += prev) / sizes.length)
                    for (let s=0; s< sizes.length; s++) {
                        if (s === max_ind) {
                            if (sizes[s] !== max) {
                                console.log(`crypto keys ${crypto_keys} max does not match`)
                            }
                            continue
                        }
                        if (sizes[s] * threshold < max && sizes[s] !== 0) {
                            let curr_snap = snaps[s]
                            // console.log('imbalance', mean, sizes[s], max)
                            const change_size = new Promise((resolve, reject) => {
                                let form = {'account': curr_snap['account'], 'symCont': curr_snap['symCont'],
                                    'strategyName': curr_snap['strategy'], 'size_before': curr_snap['size'],
                                    'size_after': mean, 'user': userConfig['User']}
                                request.post({
                                    url: userConfig['Url'] + '/strategy?jwt_token=' + userConfig['Token'],
                                    form: form
                                }, function(error, response, body) {
                                    if (!error && response.statusCode ===200) {
                                        resolve('size_changed')
                                        let msg = `\n POST Successfully Sent: <${curr_snap['account']}>` +
                                            `<${curr_snap['symCont']}> changed from ${curr_snap['size']} to ${mean} with sizes ${sizes}`
                                        console.log(msg)
                                        email_txt += msg
                                    } else　{
                                        reject(`Failed to change size: ${JSON.stringify(form)}`)
                                        console.log(`POST FAILED ${error}`)
                                    }
                                })
                            })
                            const change_size_message = await change_size.catch((err) => { console.log(err) })
                            if (change_size_message === 'size_changed') {
                                let is_fut = curr_snap['is_fut']
                                let net = net_mapping['spot']
                                if (is_fut === true) {
                                    net = net_mapping['fut']
                                }
                                if (curr_snap[net] < 0) {
                                    // let msg  = await changeStra(userConfig, exs[i], curr_snap['symCont'],
                                    //     curr_snap['account'], curr_snap['strategy'], is_fut, '')

                                    let changed_straName = msg[0]
                                    let relaunches = msg[1]
                                    email_txt += msg[2] + ` with sizes: ${sizes} \n`
                                    if (relaunches.length !== 0) {
                                        need_relaunch = need_relaunch.concat(relaunches)
                                    }

                                }
                            } else {
                                email_txt += `\n ${change_size_message}`
                            }
                        }
                    }
                }

            }
        }
    }
    return [email_txt, need_relaunch]
}


// function alert2(mailer, res, receptions, subject, userConfig, config) {
// let email = res[0]
// let relaunches = res[1]
// if (email !== '') {
//     console.log('email: ', email)
//     if (database['time_posted'] === false) {
//         console.log('Time Now: ', Date(), Date.now())
//     }
//     mailer(receptions, subject, email)
// }
// database['time_posted'] = false
// let relaunched = []
// if ('await_launch' in database && database['await_launch'].size > 0) {
//     for (let await_host of database['await_launch']) {
//         relaunches.push(await_host)
//     }
// }
// for (let i =0; i < relaunches.length; i++) {
//     console.log('relaunch', relaunches[i])
//     let host = relaunches[i]
//     if (host === undefined) {
//         console.log('encountered NAN: ', relaunches)
//     }
//     if (!relaunched.includes(host)) {
//         if (!(host in database) || (Date.now() - database[host] > 1800 * 1000) ) { // prevent constant relaunch
//             request.post({
//                 url: userConfig['RelaunchUrl'] + '/aegis/relaunch?jwt_token=' + userConfig['Token'],
//                 form: {'host': host, 'user': userConfig['User']}
//             }, function (error, response, body) {
//                 if (!error && response.statusCode === 200) {
//                     console.log('database snapshot: ', database)
//                     console.log(`Successfully Relaunched <${host}>`)
//                     database[host] = Date.now()
//                     if ('await_launch' in database) {
//                         database['await_launch'].delete(host)
//                     }
//                 } else {
//                     console.log(`RELAUNCH FAILED ${error}`)
//                 }
//             })
//         } else {
//             console.log('delayed info here: ', host)
//             database['await_launch'] ? database['await_launch'].add(host) : database['await_launch'] = new Set([host])
//             console.log(`RELAUNCH DELAYED ${host}`)
//             console.log('database snapshot2: ', database)
//         }
//     }
//     relaunched.push(host)
// }
// }