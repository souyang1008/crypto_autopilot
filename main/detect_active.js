const request = require('request');
// const fs = require('fs');
// const yaml = require('js-yaml');
const {midLogger} = require('../services/LogService');
const {host_mapping, change_strategy} = require('./helper_functions');
const {ex_mapping, net_mapping, time_mapping, mid_mapping, making_accs, get_avg_pnl, get_keys, get_num_accs,
    get_dw_key, get_face_value, get_size_from_face_value, get_pb_rank, get_pnl_rank, getRedisJson, get_open_sigs, get_cumpnl} = require('../utils/get_methods');


async function detect_active(redisConn, userConfig) {
    let email = ''
    let open_stats = await get_open_sigs(userConfig).catch((err) => { midLogger.info(err)})
    let longer = 30
    let shorter = 20
    let sig_coef = 2
    for (let ex_key in open_stats) {
        let selected_syms = open_stats[ex_key].slice(0, longer)
        if (ex_key !== 'BNBF') {
            selected_syms = open_stats[ex_key].slice(0, shorter)
        }
        let acc_prefix = ex_key
        for (let element of selected_syms) {
            let symbol = element['name'].split('-')[0]
            let ex = element['name'].split('-')[1]
            let signal_spike = false
            if (element.hourly > element.daily * sig_coef) {
                signal_spike = true
            }
            let [acc_alive_cnt, [off_strats, last_off_time], [inactive_cnt, inactive_info]] = await get_num_accs(redisConn, acc_prefix, symbol, midLogger)
            let cfg_key = `AP_REC#${acc_prefix}#${symbol}` //AP_REC#BNBF#zilf0
            let config = await getRedisJson(cfg_key, midLogger).catch((err) => midLogger.info(err))
            let pref = config[1]['preference'] === 'm'
            let pb_list = await get_pb_rank(acc_prefix, symbol, userConfig, pref, 'rank', midLogger)
            let pnl_list = await get_pnl_rank(acc_prefix ,symbol, userConfig)
            let [pnl_id, pnl_name] = [null, null]
            if (pnl_list.length > 0 && pnl_list[0][1] > 0) {
                let best_pnl_skey = pnl_list[0][0]
                pnl_id = get_dw_key(best_pnl_skey, acc_prefix, 'id')
                pnl_name = get_dw_key(best_pnl_skey, acc_prefix, 'name')
            }
            let [pb_sid, pb_sname] = [null, null]
            if (pb_list) {
                for (let pb_res of pb_list) {
                    if (pb_res.tried_times === 0 && pb_res.ir > 0.5) {
                        midLogger.info('case 1 found new strategy: ', pb_res)
                        pb_sid = '100'+pb_res.id
                        pb_sname = pb_res.name+'_dtw1'
                    }
                }
            }
            let avg_pnl = await get_avg_pnl(ex_mapping[acc_prefix].toUpperCase(), symbol);
            let cum_pnl = await get_cumpnl(ex_mapping[acc_prefix].toUpperCase(), symbol);
            if (inactive_cnt === 0 && acc_alive_cnt === 0) { //init strats
                if (pb_sname) {
                    email += `${acc_prefix}#${symbol} needs to open strategies with recommendation ${pb_sname}\n `
                } else {
                    email += `${acc_prefix}#${symbol} has no positive IR strategy and no alive acc, but top n signals\n `
                }

                // console.log(`${acc_prefix}#${symbol} needs to open strategies with recommendation ${pb_sname}`)
            } else if (inactive_cnt > 0) { //start inactive strats
                let [start_acc, start_sname] = [inactive_info[0]['acc'], inactive_info[0]['sname']]
                if (inactive_cnt === acc_alive_cnt) {
                    if (off_strats === 0) { //no strats shutdown before
                        if (pb_sname) {
                            email += `${acc_prefix}#${symbol} needs to start strategy: ${pb_sname} with acc: ${start_acc} and old sname: ${start_sname} with no active try\n`
                        } else {
                            email += `${acc_prefix}#${symbol} has no positive IR strategy with no active trys\n`
                        }

                    } else { //strats shutdown already no active current
                        if (signal_spike) {
                            if (pb_sname) {
                                email += `${acc_prefix}#${symbol} needs to start strategy: ${pb_sname} with acc: ${start_acc} and old sname: ${start_sname} with signal spike and no active strats\n`
                            } else {
                                email += `${acc_prefix}#${symbol} has no positive IR strategy with signal spike and no active strats\n`
                            }
                        }
                    }
                } else {
                    if (cum_pnl > 0) {
                        if (pb_sname) {
                            email += `${acc_prefix}#${symbol} needs to start strategy: ${pb_sname} with acc: ${start_acc} and old sname: ${start_sname} with cumpnl: ${cum_pnl} and avgpnl: ${avg_pnl} with some inactive\n`
                        } else {
                            email += `${acc_prefix}#${symbol} has no positive IR strategy with cumpnl: ${cum_pnl} and avgpnl: ${avg_pnl} with some inactive\n`
                        }

                    }
                }
            } else {
                let new_sname = pb_sname
                if (pnl_name) {
                    new_sname = pnl_name
                }
                if (acc_alive_cnt < 3) {
                    if (signal_spike && cum_pnl > 20) {
                        if (new_sname) {
                            email += `${acc_prefix}#${symbol} needs to add more strategy: ${new_sname} with cumpnl: ${cum_pnl} and avgpnl: ${avg_pnl} with signal spike\n`
                        } else {
                            email += `${acc_prefix}#${symbol} needs to add more strategy: with no pb/pnl sname with cumpnl: ${cum_pnl} and avgpnl: ${avg_pnl} with signal spike\n`
                        }

                    }
                } else if (acc_alive_cnt < 7) {
                    if (signal_spike && cum_pnl > 50 && avg_pnl > 10) { //better add recent pnl
                        if (new_sname) {
                            email += `${acc_prefix}#${symbol} needs to go all out with strategy: ${new_sname} with cumpnl: ${cum_pnl} and avgpnl: ${avg_pnl} with signal spike\n`
                        } else {
                            email += `${acc_prefix}#${symbol} needs to go all out with strategy: no pb/pnl sname with cumpnl: ${cum_pnl} and avgpnl: ${avg_pnl} with signal spike\n`
                        }

                    }
                }
            }
        }
    }
    return email

    // // alternate code
    // let need_relaunch  = []
    // console.log('all actions: ', all_actions)
    // for (let exchange in all_actions) {
    //     for (let action of all_actions[exchange]) {
    //         let [snap_info, exchange, symbol, account, stype, avg_size, strategyName, strategyIdAfter, strategyNameAfter] = action
    //         let least_reopen_size = Math.max(1 ,
    //             get_size_from_face_value(meta, symbol, exchange, snap_info['mid'], snap_info['last'], 50, midLogger));
    //         let new_size = Math.ceil(avg_size * 0.6)
    //         new_size =  Math.ceil(Math.max(new_size, least_reopen_size))
    //         all_texts += `\n ${exchange}-${account}-${symbol} changed from ${strategyName} to ${strategyNameAfter} with size ${new_size}`
    //         need_relaunch = await change_strategy(userConfig, exchange, symbol, account, stype,
    //             strategyName, strategyNameAfter, strategyIdAfter, new_size, need_relaunch, host_mapping, 'mid_update', midLogger)
    //     }
    // }
    //
    // await new Promise(resolve => setTimeout(resolve, 60*1000));
    //
    // for (let host of need_relaunch) {
    //     request.post({
    //         url: userConfig['RelaunchUrl'] + '/aegis/relaunch?jwt_token=' + userConfig['Token'],
    //         form: {'host': host, 'user': userConfig['User']}
    //     }, function (error, response, body) {
    //         console.log(host, body, error)
    //         try {
    //             body = JSON.parse(body)
    //             if (!error && response.statusCode === 200 && body['status'] === 'info') {
    //                 midLogger.info(`Successfully Relaunched <${host}>`)
    //             } else {
    //                 midLogger.info(`RELAUNCH FAILED ${host}, ${error} with ${body['status']} and ${body['data']}`)
    //             }
    //         } catch (err) {
    //             midLogger.info(`RELAUNCH FAILED ${host}, ${err}, ${error} with body ${body}, ${JSON.stringify(body)} and response ${response}`)
    //         }
    //     })
    // }
    //
    // return [all_texts, JSON.stringify(all_actions)]
}

module.exports = {detect_active: detect_active}