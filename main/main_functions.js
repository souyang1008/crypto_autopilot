const fs = require('fs');
const request = require('request');
const yaml = require('js-yaml');
const ap_db = "/data/crypto_autopilot_logs/ap_db.json";
const {apLogger} = require('../services/LogService');
const ApiConnector = require("../utils/api_connector");
const FutUniHelper = require("../services/UniFutSwitch");
const riskLimitOpsService = require("../services/RiskLimitOpsService");
const {change_strategy, host_mapping, isFloat, isInt} = require('./helper_functions');
const strategyRuntimeSerive = require("../services/StrategyRuntimeService");
const strategyOpsService = require("../services/StrategyOpsService");
const strategyAdvisorService = require("../services/StrategyAdvisorService");
const strategyLuciferService = require("../services/StrategyLuciferService");
const strategyRouterService = require("../services/StrategyRouterService");
const strategySwitchService = require("../services/StrategySwitchService");

const {ex_mapping, net_mapping, time_mapping, mid_mapping, comm_mapping, making_accs, get_ex,
    get_sym, get_threshold, get_face_value, get_size_from_face_value, get_num_accs, get_pnl_rank, get_dw_key, get_raw_key, getRedisJson, get_keys
} = require('../utils/get_methods');
const accountService = require("../services/AccountService");
const assetSwitch = require("../services/BgsMatchAccount/SwitchAssets");
const sleep = (secs=1) => new Promise(resolve => setTimeout(resolve, secs * 1000));
const local_config = yaml.safeLoad(fs.readFileSync('config/rules.yml', 'utf8'));
let acc_notional = {}
let acc_ordcnt = {}
let acc_dealcnt = {}
let acc_rebate = {}
let acc_ttnov = {}
let acc_tvol = {}
let pnl_summary = {}
let acc_rc_status = {}
let launch_buffer = {}
let mkt_share_sym_map = {}
let hourly_turnovers = {}
let hourly_ordcnts = {}
let dealt_router = new Set();

function reset_acc() {
    acc_notional = {}
    acc_ordcnt = {}
    acc_dealcnt = {}
    acc_rebate = {}
    acc_ttnov = {}
    acc_tvol = {}
    pnl_summary = {}
    acc_rc_status = {}
    mkt_share_sym_map = {}
    hourly_turnovers = {}
    hourly_ordcnts = {}
    dealt_router = new Set();
}

let apRecCfg = accountService.get_apRecCfg();


// if (account_rc_status !== riskcontrol) {
//     if (riskcontrol) {
//         if (!('tka1' in launch_buffer) || launch_buffer['tka1']['time'] < Date.now() - 1800*1000) {
//             apLogger.info(`${account}#${symbol}#${riskcontrol} launch buffer: ${JSON.stringify(launch_buffer)}`)
//             ApiConnector.update_acc_stats(account, symbol, riskcontrol).then(() => {
//                 apLogger.info(`ACC_RC Posted: ${account}#${symbol}#${riskcontrol}`)
//             });
//             if (!('tka1' in launch_buffer)) {
//                 launch_buffer['tka1'] = {
//                     'time': Date.now(),
//                     'relaunch': true
//                 }
//             } else {
//                 launch_buffer['tka1']['relaunch'] = true;
//             }
//             riskLimitOpsService.transfer_account_norl(account).then(() => {
//                 apLogger.info(`mcspot Switch Account: ${account}#${symbol}`);
//             });
//         }
//     } else {
//         ApiConnector.update_acc_stats(account, symbol, riskcontrol).then(() => {
//             apLogger.info(`ACC_RC Posted: ${account}#${symbol}#${riskcontrol}`)
//         });
//     }
// }

function getRCSize(account, symbol, exchange, strategyName, riskcontrol, account_rc_status, usual_size, mkt_share, hourly_vol, hourly_ords, netPnl, comm, dic) {
    if (exchange.startsWith('MCS')) {
        let need_cooloff = accountService.isMCCoolDown(account, symbol, mkt_share, hourly_vol, hourly_ords);
        if (dic['size'] > 1) {
            if (riskcontrol) {
                return 0
            }
            if (need_cooloff) {
                apLogger.info(`COOLOFF: ${account}#${symbol} ${mkt_share}/${hourly_vol}/${hourly_ords} with snap: ${JSON.stringify(dic)}`)
                return 1
            }
        } else if (!riskcontrol && !need_cooloff) {
            if ((dic['size'] === 1 && netPnl > -1) || account_rc_status) { //dic['acc_rc']
                let reopen_size = Math.floor(usual_size * 0.7);
                reopen_size = accountService.random_order_size(reopen_size);
                apLogger.info(`REOPEN PREV_COOLOFF ${account}#${symbol} with market share: ${mkt_share} and size ${reopen_size}`)
                return reopen_size;
            }
        }
    }
    let shut_msg = ''
    if (exchange.startsWith('BG')) {
        let new_size = null;
        if (exchange === 'BGS') {
            if (comm > 200) {
                if (netPnl / comm < 0.1) {
                    shut_msg += `${account}#${symbol}#${strategyName} SHUTDOWN For FeesRatio less than 0.1\n`
                    new_size = 0;
                } else if (netPnl / comm < 0.2) {
                    if (comm > 1000) {
                        shut_msg += `${account}#${symbol}#${strategyName} SHUTDOWN For FeesRatio less than 0.2\n`
                        new_size = 0;
                    }
                }
            }
        } else if (exchange === 'BGF') {
            if (netPnl / comm < 0.06 && comm > 100 && !(['btctf0', 'ethtf0'].includes(symbol))) {
                shut_msg += `${account}#${symbol}#${strategyName} SHUTDOWN For FEES less than 0.1\n`
                new_size = 0;
            }
        }
        if (shut_msg.length > 0 && dic['size'] !== 0) {
            apLogger.info(shut_msg);
        }
        if (riskcontrol || new_size === 0) {
            acc_rc_status[account] = true;
            return 0
        }

        let need_cooloff = accountService.isBGCoolDown(account, symbol, mkt_share);


        if (need_cooloff && dic['size'] > 1) {
            apLogger.info(`COOLOFF: ${account}#${symbol} ${mkt_share}/${hourly_vol}/${hourly_ords} with snap: ${JSON.stringify(dic)}`)
            return 1
        } else if (!riskcontrol && !need_cooloff) {
            if (dic['size'] === 1 && netPnl > -50) {
                let reopen_size = Math.floor(usual_size* 0.8);
                if (netPnl > 100) {
                    reopen_size = Math.floor(usual_size* 1.5);
                }
                reopen_size = accountService.random_order_size(reopen_size);
                apLogger.info(`REOPEN PREV_COOLOFF ${account}#${symbol} with market share: ${mkt_share} and size ${reopen_size}`)
                return reopen_size;
            }
        }
        if (!riskcontrol && dic['size'] === 0 && dic['acc_rc']) {
            if ( !('riskLimit' in dic) || dic['riskLimit'] > 0) { //
                let reactivate_size = Math.floor(usual_size * 0.7);
                apLogger.info(`ACC_RiskControl Cleared: ${account}#${symbol}#${riskcontrol} with size: ${reactivate_size}`)
                return reactivate_size
            }
        }
    }
    return null
}

function get_newSize(account, exchange, symbol, strategyName, config, meta, dic, netPnl, lastPnl, midPrice, database) {
    let threshold_key = 'AP_REC#THRESHOLD_INFO'
    let thresholdInfo = apRecCfg[threshold_key];
    if (!thresholdInfo) {
        console.log('Error without thresholdInfo');
        return null
    }
    const actions = config
    let alert = account + '#' + symbol + '#' + dic['strategy'] + '#' +'warning'
    if (actions === undefined) {
        apLogger.info(`ERROR: ${symbol} not found in mapping`);
        return null
    }

    let face_value = get_face_value(dic['size'], meta, symbol, account, dic[midPrice], dic['lastTradePrice'], apLogger)
    if (face_value===0 && dic['size'] !==0 ) {
        if (! account.startsWith('OKC')) {
            apLogger.info(`${account}#${symbol}#${strategyName} has no face value`) // with \n ${JSON.stringify(dic)}`)
        }
        return dic['size']
    }
    const g_cut = thresholdInfo['group'][0]
    let group = face_value < g_cut ? 'S' : 'L';

    // shutdown for nonzero bgs fees or max commission



    let lsp = 0
    let lsp_warning = false
    try {
        lsp = database['shutdowns'][alert.split('#').slice(0,3).join('#')]['last_shutdown_pnl']
    } catch(err) {
        lsp_warning = true
    }
    if (lsp) {
        lsp = Number(lsp)
        apLogger.info(`got lsp for ${alert} with value: ${lsp}`)
    }

    //om special
    if (dic['strategy'].includes('om') && account.startsWith('BNBC')) {
        if (database[alert]) {
            let max_pnl = Math.max(...database[alert]['past_pnls'])
            if (max_pnl - netPnl > 30) {
                apLogger.info(`${alert} shutdown for om strategy: with max pnl: ${max_pnl} and current pnl: ${netPnl}`)
                return 0
            }
        }
        else if (lsp) {
            if (lsp - netPnl > 30) {
                apLogger.info(`${alert} shutdown for om strategy: with lsp: ${lsp} and current pnl: ${netPnl}`)
                return 0
            }
        }
    }

    for (let i =0; i < actions.length; i++) {
        let threshold = get_threshold(thresholdInfo, actions[i]['threshold'], group, face_value, apLogger)

        let dss = get_size_from_face_value(meta, symbol, exchange, dic[midPrice], dic['lastTradePrice'], Number(actions[1]['dss']), apLogger);
        if (threshold>0) {
            if (lastPnl > threshold && dic['size'] !== 1) {
                if (['MCF', 'BGF', 'CWF', 'WXF', 'DCF', 'XTF', 'LBF', 'EXF', 'HCF', 'BMF'].includes(exchange)) {
                    let addone = dic['size'];
                    let hit = dic['totalFilled'] / dic['totalVol'] * 100;
                    if (['btctf0', 'ethtf0'].includes(symbol)) {
                        if (face_value > 5000) {
                            addone =  dic['size'] + 1;
                        }
                    } else if (symbol === 'ftttf0') {
                        if (face_value > 2000) {
                            addone = dic['size'] + 1;
                        }
                    } else if (hit < 70) {
                        if (face_value > 2200 || (symbol === 'mbitf0' && face_value > 1500)) {
                            addone =  dic['size'] + 1;
                        }
                        if (face_value > 2500 && !['btctf0', 'ethtf0'].includes(symbol)) {
                            addone = dic['size'] -1;
                        }
                    } else {
                        if (face_value > 3500) {
                            addone =  dic['size'] + 1;
                        } else if (face_value > 4000) {
                            addone =  dic['size'] - 1;
                        }
                    }

                    if (addone !== dic['size']) {
                        return addone;
                    }
                }
                if (Number(face_value) > 20000 && (symbol===symbol.toUpperCase())) {
                    apLogger.info(`${account}#${symbol}#${strategyName} has face value ${face_value}, hence no change`)
                    return dic['size']
                }
                if (Number(face_value) > 20000 && exchange === 'BGS') {
                    apLogger.info(`${account}#${symbol}#${strategyName} has face value ${face_value}, hence no change`)
                    return dic['size']
                }

                if (making_accs.includes(account)){
                    apLogger.info(`SKIP INCREASE-MAKING: ${exchange} ${symbol}`)
                    return dic['size']
                }

                if (database[alert]) {
                    (database[alert]['past_pnls'].slice(-1)[0] !== netPnl ?
                        database[alert]['past_pnls'].push(netPnl) : apLogger.info(`skip redundant alert ${alert}`))
                    database[alert]['increase_cnt'] += 1
                    database[alert]['decrease_cnt'] = 0
                } else {
                    database[alert] = {'past_pnls': [netPnl], 'decrease_cnt': 0, 'increase_cnt': 1}
                }
                // delete database[alert] //&& !(account.startsWith('HBF3'))
                apLogger.info(`FaceValue of ${account}#${symbol}#${strategyName}: ${face_value}, ${alert} \
                 ${database[alert]['past_pnls'].slice(-10)}, ${netPnl}, ${lsp}`)

                if ((dic['size'] < actions[i]['usual'] * actions[i]['min'])  && dic['type'] > face_value * 0.03 ) { //quickly turn up
                    apLogger.info(`${account}#${symbol}#${strategyName} trigger BOOST102 with last pnl: ${dic['type']}`)
                    return Math.ceil(Math.max(actions[i]['usual']*0.5, dic['size'] * 1.5))
                }

                if ((dic['size'] < actions[i]['usual'] * 0.5)  && database[alert]['increase_cnt'] >= 5 ) { //quickly turn up
                    apLogger.info(`${account}#${symbol}#${strategyName} trigger BOOST103 with last pnl: ${dic['type']}`)
                    return Math.ceil(Math.max(actions[i]['usual']*0.5, dic['size'] * 1.5))
                }

                let new_size = Math.ceil(dic['size'] * actions[i]['action'])
                // let new_facevalue = get_face_value(new_size, meta, symbol, account, dic[midPrice], dic['lastTradePrice'], apLogger)

                if (Number(netPnl) - lsp > face_value * 0.05 || database[alert]['increase_cnt'] >= 5) {
                    apLogger.info(`${account}#${symbol}#${strategyName} trigger BOOST101 with net: ${netPnl}, lsp: ${lsp}, and alert_info: ${JSON.stringify(database[alert])} and snap: ${JSON.stringify(dic)}`)
                    return Math.ceil(Math.max(Math.min(dss, Math.ceil(actions[i]['usual']*0.3)), new_size))
                }

                if (new_size > actions[i]['usual'] * actions[i]['max']) {
                    if ('vol_threshold' in actions[i] && dic['cashvol_pct'] < actions[i]['vol_threshold']) {
                        // keep normal increasing
                    } else {
                        apLogger.info(`${account}#${symbol} prevent from increase too much ${new_size}/${dic['size']}/${actions[i]}`)
                        new_size = Math.ceil((new_size+dic['size'])/2) //prevent from increase too much if exceeds usual max
                    }
                }

                if ('vol_threshold' in actions[i] && dic['cashvol_pct'] > actions[i]['vol_threshold']) {
                    new_size = Math.ceil(dic['size']* 1.1) //avoid increasing too much (15%) if vol threshold is big
                }

                if (dic['size'] < actions[i]['usual'] * actions[i]['min']) {
                    new_size = Math.ceil(dic['size'] * (actions[i]['action']*1.2))
                }

                return new_size
            }
        } else {
            if (lastPnl < threshold) {
                // if (making_accs.includes(account)) {
                //     if (Number(netPnl) < -100) {
                //         apLogger.info(`DECREASE TRIGGERED SHUTDOWN: ${exchange} ${symbol}`)
                //         return 0
                //     } else {
                //         apLogger.info(`DECREASE TRIGGERED REMAIN: ${exchange} ${symbol}`)
                //         return dic['size']
                //     }
                // }
                if (database[alert]) {
                    (database[alert]['past_pnls'].slice(-1)[0] !== netPnl ?
                        database[alert]['past_pnls'].push(netPnl) : apLogger.info(`skip redundant alert ${alert}`))
                    database[alert]['decrease_cnt'] += 1
                    database[alert]['increase_cnt'] = 0
                } else {
                    database[alert] = {'past_pnls': [netPnl], 'decrease_cnt': 1, 'increase_cnt': 0}
                }
                let previous_pnl = Number(database[alert]['past_pnls'].slice(-10).reduce(function(a,b) {return Math.max(a,b)}))

                apLogger.info(`FaceValue of ${account}#${symbol}#${strategyName}: ${face_value}, ${alert} \
                 ${database[alert]['past_pnls'].slice(-10)}, ${netPnl}, ${lsp}, ${previous_pnl}`)

                let new_size = Math.floor(dic['size'] * actions[i]['action']) //changed
                if (new_size === 0) {apLogger.info(`SHUTDOWN_REASON: BASE_RULE ${account}#${symbol}#${strategyName}: ${i}, ${actions[i]['action']}, ${dic['size']}`)}

                // let slice_len = 15
                // database[alert]['past_pnls'] = database[alert]['past_pnls'].slice(-slice_len);
                // let cumsum_pnl = database[alert]['past_pnls'].reduce((a, b) => a+b, 0);

                if (database[alert]['past_pnls'].length < 1) {
                    previous_pnl = 0
                }

                if (netPnl < - Math.min(100, Math.max(face_value * 0.03, 2))) { //Math.max(face_value * 0.03, 20) losing too much so change
                    apLogger.info(`SHUTDOWN_REASON: STOPLOSS1 ${account}#${symbol}#${strategyName}: ${netPnl}, ${- Math.min(100, Math.max(face_value * 0.03, 2))}`)
                    return 0
                }
                let drawdown_pnl_threshold = 40
                if (account.startsWith('GT') || account.startsWith('KC')) {
                    drawdown_pnl_threshold = 20
                }
                if (netPnl - previous_pnl < - Math.min(200,
                    Math.max(drawdown_pnl_threshold, Math.max(previous_pnl - lsp, face_value * 0.15) * 0.2))) {
                    if (netPnl < -10 || lastPnl < -1) {
                        apLogger.info(`SHUTDOWN_REASON: STOPLOSS2 ${account}#${symbol}#${strategyName}: ${netPnl - previous_pnl}, ${- Math.min(200,
                            Math.max(drawdown_pnl_threshold, Math.max(previous_pnl - lsp, face_value * 0.15) * 0.2))}`)
                        return 0
                    }
                }

                if (Number(dic['size']) > actions[i]['usual'] * actions[i]['max']){
                    new_size = Math.floor(new_size * 0.8) //large decrease if size exceeds usual max
                }


                if (new_size === 0 || new_size === 1) {
                    apLogger.info(`CAREFUL SIZE HERE: NEWSIZE: ${new_size}, OLDSIZE: ${dic['size']}, ${lastPnl}, ${face_value}, ${group}, ${threshold},${account}#${symbol}#${strategyName}`)
                }


                // prevent size too small
                let new_facevalue = get_face_value(new_size, meta, symbol, account, dic[midPrice], dic['lastTradePrice'], apLogger)
                if ((new_facevalue < 11 || new_size < 5) && (Number.isFinite(new_facevalue)) && exchange !== 'MCF') {
                    apLogger.info(`${account}#${symbol} shutdown for too small of the new size`)
                    new_size = 0
                }
                return new_size
            }
        }
    }
    return null
}

async function alert(meta, mailer, res, receptions, subject, userConfig) {
    let email = res[0]
    let database = res[2]
    let final_subject = subject
    let this_loop_changed = {}

    if (database['shutdowns']) {
        for (let shutdown_key in database['shutdowns']) {
            let shutdown_record = database['shutdowns'][shutdown_key]
            let [cnt, last_time, zero_status, mid_price, last_price, is_fut, last_shut, last_net, bad_shutdowns] = [shutdown_record['cnt'],
                shutdown_record['last_time'], shutdown_record['zero'], shutdown_record['mid'], shutdown_record['last'],
                shutdown_record['is_fut'], shutdown_record['last_shut'], shutdown_record['last_shutdown_pnl'], shutdown_record['bad_shutdowns']]
            if (zero_status && (mid_price || last_price)) { //cnt <= 5 &&
                let time_past = Date.now() - last_time
                let good_tbase = 300 * 1000 // 5 minutes
                let bad_tbase = 1200 * 1000 // 20 minutes
                let [account, symbol, strategyName] = shutdown_key.split('#')
                let exchange = get_ex(account);
                let cfg_key = `AP_REC#${exchange}#${symbol}`
                let actions = await getRedisJson(cfg_key, apLogger).catch((err) => apLogger.info(err))
                let dss = get_size_from_face_value(meta, symbol, exchange, mid_price, last_price, Number(actions[1]['dss']), apLogger);
                let least_reopen_size = Math.max(1 ,
                    get_size_from_face_value(meta, symbol, exchange, mid_price, last_price, 15, apLogger));
                // om special
                if ((strategyName).includes('om') && (time_past > 120 * 1000) && account.startsWith('BNBC')) { // 2 minutes
                    if (cnt > 3) {
                        apLogger.info('shutdown too many times so smallest reopen size NOW')
                    } else if (last_shut === 'nice' && cnt === 1) {
                        least_reopen_size = Number(actions[1]['usual'])
                    } else {
                        least_reopen_size = Math.floor(Number(actions[1]['usual']) / 2)
                    }
                    let size_posted = await post_newsize_async(userConfig['Url'], userConfig['Token'], account, symbol,
                        strategyName, 0, least_reopen_size, 0, last_net, userConfig['User'], false)
                    apLogger.info('om_size_posted: ', size_posted)
                    database['shutdowns'][shutdown_key]['zero'] = false
                    apLogger.info( `\n REOPEN3: ${shutdown_key} has reopened to ${least_reopen_size} with ${JSON.stringify(database['shutdowns'][shutdown_key])}`)
                }

                if ((time_past > good_tbase && last_shut === 'nice')) {
                    let reopen_size = Math.ceil(Math.max(Math.min(dss, Number(actions[1]['usual']) * Number(actions[1]['min'])),
                                                least_reopen_size))
                    let size_posted = await post_newsize_async(userConfig['Url'], userConfig['Token'], account, symbol,
                        strategyName, 0, reopen_size, 0, last_net, userConfig['User'], false)
                    apLogger.info('size_posted1: ', size_posted)
                    database['shutdowns'][shutdown_key]['zero'] = false
                    apLogger.info( `\n REOPEN1: ${shutdown_key} has reopened to ${reopen_size} with ${JSON.stringify(database['shutdowns'][shutdown_key])}`)
                } else if (time_past > bad_tbase * bad_shutdowns && last_shut === 'bad') {
                    let size_posted = await post_newsize_async(userConfig['Url'], userConfig['Token'], account, symbol,
                        strategyName, 0, least_reopen_size, 0, last_net, userConfig['User'], false)
                    apLogger.info('size_posted2: ', size_posted)
                    database['shutdowns'][shutdown_key]['zero'] = false
                    apLogger.info( `\n REOPEN2: ${shutdown_key} has reopened to ${least_reopen_size} with ${JSON.stringify(database['shutdowns'][shutdown_key])}`)
                }
                    // email += `\n REOPEN: ${shutdown_key} has reopened to ${reopen_size} with ${JSON.stringify(database['shutdowns'][shutdown_key])}`
            }
            // else if (cnt > 5) {
            //     delete database['shutdowns'][shutdown_key];
            //     email += `\n PERMANENT SHUTDOWN: ${shutdown_key}`
            // }
        }
    }

    for (let host in database['lines']) {
        try {
            if ((Object.keys(database['lines'][host]).length > 0) && (!(host in database) || (Date.now() - database[host] > 1800 * 1000) )) {
                let changed = false
                for (let acc_key in database['lines'][host]) {
                    let info = database['lines'][host][acc_key]
                    let [exchange, symbol, account, sid,
                        strategyName, old_size, lastPnl] = [info['ex'], info['sym'], info['acc'], info['sid'],
                        info['strat'], info['size'], info['lastPnl']]

                    let alert = account + '#' + symbol + '#' + strategyName + '#' +'warning'
                    let ex_sym = exchange + '_' + symbol
                    if (ex_sym in this_loop_changed) {
                        continue
                    }

                    let sym_k = get_sym(acc_key)
                    let sym_k_changed = sym_k + '#changed'
                    let online_sym_k = sym_k + '#online'
                    if (database[sym_k] && Date.now() - database[sym_k][1] < 7200 * 1000 && database[sym_k][0] > 2) {
                        apLogger.info(`prevent changing too much on sym level: ${acc_key} : ${JSON.stringify(info)}`)
                        continue
                    }

                    delete database[alert]
                    delete database['lines'][host][acc_key]

                    if (making_accs.includes(account)) {continue}

                    if (database[acc_key] > 2) {
                        email += `\n TOO MANY CHANGES: SHUTDOWN TO 0 FOR ${account}#${symbol}#${strategyName}`
                        final_subject = `[CRYPTO] FREQUENT CHANGES OF STRATEGIES: SHUTDOWN NEEDS ATTENTION!`;
                        let napped = await power_nap(database, account, symbol, strategyName, null)
                        apLogger.info('napped1: ', napped)
                        continue
                    }

                    let msg_conds = `\n conditions: host_exists: ${(Object.keys(database['lines'][host]).length > 0)}, host_not_in_db: ${!(host in database)}, 
                        last_relaunch_longer_1800: ${(Date.now() - database[host] > 1800 * 1000)}, last_2_either_true: ${(!(host in database) || (Date.now() - database[host] > 1800 * 1000) )}, 
                         action_lists: ${JSON.stringify(database['lines'][host])},  ${database[host]}, ${Date.now()}`
                    apLogger.info(msg_conds)

                    let cfg_key = `AP_REC#${exchange}#${symbol}`
                    let config = await getRedisJson(cfg_key, apLogger).catch((err) => apLogger.info(err))
                    let new_size = Math.floor(config[1]['usual'] * 0.3)
                    // if (database[acc_]) per symbol limit
                    // let dss = get_size_from_face_value(meta, symbol, exchange, mid_price, last_price, Number(config[1]['dss'], apLogger));
                    // let least_reopen_size = Math.max(new_size,
                    //     get_size_from_face_value(meta, symbol, exchange, mid_price, last_price, 11, apLogger));
                    let msg  = await changeStra(userConfig, config, exchange, symbol, account, sid, strategyName, new_size, email)
                    let changed_straName = msg[0]
                    email = msg[2]
                    if (changed_straName === null || msg === undefined || changed_straName === undefined) {
                        email += `\n RUN OUT STRATEGIES: SHUTDOWN TO 0 FOR ${account}#${symbol}#${strategyName}`
                        final_subject = `[CRYPTO] RAN OUT STRATEGIES: SHUTDOWN NEEDS ATTENTION!`;
                        let napped = await power_nap(database, account, symbol, strategyName, null)
                        apLogger.info('napped2: ', napped)
                    } else {
                        changed = true
                        email += `\n STRATEGIES CHANGED: ${account}#${symbol}#${strategyName} changed to ${changed_straName}`
                        let alert_new = account + '#' + symbol + '#' + changed_straName + '#' +'warning'
                        delete database[alert_new]
                        database[sym_k_changed] = true
                        this_loop_changed[ex_sym]  = 1
                    }
                    let relaunches = msg[1]
                }
                if (changed) {
                    database[host] = Date.now()
                    apLogger.info('host time: ', database[host])
                    await new Promise(resolve => setTimeout(resolve, 5*1000));
                    request.post({
                        url: userConfig['RelaunchUrl'] + '/aegis/relaunch?jwt_token=' + userConfig['Token'],
                        form: {'host': host, 'user': userConfig['User'], 'ts': Date.now()}
                    }, function (error, response, body) {
                        try {
                            body = JSON.parse(body)
                            if (!error && response.statusCode === 200 && body['status'] === 'info') {
                                apLogger.info(`RELAUNCH SUCCESS <${host}>`)
                                email += `\n RELAUNCH SUCCESS <${host}>`
                            } else {
                                apLogger.info(`RELAUNCH FAILED ${host}, ${error} with ${body['status']} and ${body['data']}`)
                                email += `\n RELAUNCH FAILED ${host}, ${error} ${body}`
                            }
                        } catch (err) {
                            apLogger.info(`RELAUNCH FAILED ${host}, ${err}, ${error} with body ${body} and response ${response}`)
                            email += `\n RELAUNCH FAILED ${host}, ${error} ${body}, ${response}`
                        }
                    })
                }
            } else {
                apLogger.info(`RELAUNCH DELAYED ${host}, ${JSON.stringify(database['lines'][host])}`)
            }
        } catch(e) {
            apLogger.info(`FAILED TO CHANGE FOR ${host} with error_msg: ${e}`)
        }
        if ((Object.keys(database['lines'][host]).length < 1)) {
            delete database['lines'][host]
        }
    }
    try {
        if (Object.keys(launch_buffer).length > 0) {
            await sleep(1);
            for (let host in launch_buffer) {
                if (launch_buffer[host]['relaunch']) {
                    let res = await ApiConnector.relaunch(host);
                    launch_buffer[host] = {
                        'time': Date.now(),
                        'relaunch': false
                    }
                    apLogger.info(`RELAUNCH SUCCESS <${host}>`, res)
                }
            }
        }
    } catch(e) {
        apLogger.info(`FAILED TO RELAUNCH: ${e}`)
    }

    if (email !== '') {
        apLogger.info('email: ', email)
        if (email !== database['last_email']) {
            database['last_email'] = email
            email += ` \n Time Now: , ${Date()}, ${Date.now()}`
            mailer(receptions, final_subject, email)
        }
    }
    database['time_posted'] = false
    fs.writeFile(ap_db, JSON.stringify(database), (err) => {
        if (err) {
            apLogger.info('FAILED TO SAVE DATABASE IN REDIS')
        }
    })
}

async function changeStra(userConfig, config, exchange, symbol, account, sid, strategyName, new_size, email_texts){
    let changed_straName = null
    let need_relaunch = []
    const new_strategy_id =  new Promise((resolve, reject) => {
        request(userConfig['Url']  + '/strategy/advice?exchange='+
            ex_mapping[exchange] +'&symbol='+ symbol +'&jwt_token=' +
            userConfig['Token'], function(err, res, body) {
            if (body) {
                body = JSON.parse(body)
                apLogger.info('all_recommends: ', body)
                let best_id = body['pb_all_recommends']['id'] || null
                let best_straName = body['pb_all_recommends']['name'] || null
                let best_stype = body['pb_all_recommends']['stype'] || null
                let prefer = config[1]['preference']
                if (prefer === 'a') {
                    if ('score' in body['taker_recommends'] || 'score' in body['maker_recommends']) {
                        let maker_score = body['maker_recommends']['score'] || 0
                        let taker_score = body['taker_recommends']['score'] || 0
                        if (maker_score >= taker_score && maker_score > 0) {
                            best_id = body['maker_recommends']['id'];
                            best_straName = body['maker_recommends']['name'];
                            best_stype = body['maker_recommends']['stype'];
                        } else if ((taker_score > maker_score && taker_score > 0) ) {
                            best_id = body['taker_recommends']['id'];
                            best_straName = body['taker_recommends']['name'];
                            best_stype = body['taker_recommends']['stype'];
                        }
                    }
                } else if (prefer === 'm') {
                    if (Object.keys(body['maker_recommends'].length !== 0)) {
                        best_id = body['maker_recommends']['id'];
                        best_straName = body['maker_recommends']['name'];
                        best_stype = body['maker_recommends']['stype'];
                    } else {
                        best_id = null
                        best_straName = null
                        best_stype = null
                    }
                } else {
                    if (Object.keys(body['taker_recommends'].length !== 0)) {
                        best_id = body['taker_recommends']['id'];
                        best_straName = body['taker_recommends']['name'];
                        best_stype = body['taker_recommends']['stype'];
                    } else {
                        best_id = null
                        best_straName = null
                        best_stype = null
                    }
                }

                if (best_id === null) {
                    email_texts = `Failed to changed Strategy ${account}#${symbol}#${strategyName} \n` + email_texts
                }
                resolve([best_id, best_straName, best_stype, body])
            } else {
                email_texts = `Failed to query advice ${account}#${symbol}#${strategyName} \n` + email_texts
                resolve([null, null, null, null])
            }

        })
    })
    let best = await new_strategy_id
    let best_id = best[0]
    let best_straName = best[1]
    let best_stype = best[2]
    let body_before = best[3]

    let pnl_list = await get_pnl_rank(exchange ,symbol, userConfig)
    if (exchange === 'BGS') {
        for (let pnl_rec of pnl_list) {
            let best_pnl_skey = pnl_rec[0]
            if (pnl_rec[1] > 0.5 && !best_pnl_skey.includes('_m_')) {
                best_id = get_dw_key(best_pnl_skey, exchange, 'id')
                best_straName = get_dw_key(best_pnl_skey, exchange, 'name')
                apLogger.info('BGS pnl chosen: ', `${account}#${symbol}#${strategyName}`, best_id, best_straName, 'before', best[0], best[1])
                break;
            }
        }
    }
    if (best_straName && exchange === 'BGS' && Number(best_straName.split('_')[0]) < 230117) {
        best_id = null
    }
    if (best_id !== undefined && best_id !== null && (best_id.toString().includes('400400')  || best_id.toString().includes('100400') || best_id.toString().includes('400100') || best_id.toString().includes('100100') ||
        best_id.toString().length > 13 )) {
        best_id = null
        email_texts += `\n WRONG STRATEGY DTW ERROR <${account}#${symbol}> ${JSON.stringify(body_before)}, best_id: ${best_id}`
    }
    if (best_id === null || best_id === undefined || strategyName === best_straName) {
        changed_straName = null
        email_texts += `\n <${account}#${symbol}> FAILED TO CHANGE STRATEGY`
    } else {
        let exists = await strategyRuntimeSerive.snapExists(`CRYPTO_SNAPSHOT#${account}#${symbol}#${best_straName}`);
        if (exists) {
            apLogger.info('KEY Exists Switch to Base: ', `CRYPTO_SNAPSHOT#${account}#${symbol}#${best_straName}`)
            best_straName = get_raw_key(best_straName, 'name');
            best_id = get_raw_key(best_straName, 'id');
        }
        if (!exchange.startsWith('BG')) {
            need_relaunch = await change_strategy(userConfig, exchange, symbol, account, best_stype,
                sid, strategyName, best_straName, best_id, new_size,
                need_relaunch, host_mapping, 'fast_shutdown_update', apLogger)
            changed_straName = best_straName
        }
    }
    return [changed_straName, need_relaunch, email_texts]
}

async function loop_key(key, account, symbol, strategyName, exchange, userConfig, config, meta, database) {

    let snapshot = await getRedisJson(key, apLogger)

    if (snapshot === undefined || snapshot['enabled'] !== true) {
        return undefined
    }
    snapshot['size'] = Number(snapshot['size'])
    const old_size = parseInt(snapshot['size'])
    let sid =  snapshot['sid'];
    let ap_size = config[1]['usual']

    if (exchange === 'BGS') {
        //delete zero risklimit strategies for BGS
        if (snapshot.enabled) {
            let meta_key = `${symbol}-${ex_mapping[exchange].toUpperCase()}`
            let price = snapshot.lastTradePrice ? snapshot.lastTradePrice : meta[meta_key]['price']||999;
            if (snapshot.riskLimit === 0 && snapshot.size === 0) {
                if (Math.abs(snapshot.curbal * price) < 5) {
                    await strategyOpsService.RemoveStrategy(symbol, account, sid);
                    return undefined;
                }
            }
            //switch passthrough back to normal strategies
            if (snapshot.strategy === "PASSTHROUGH" && !snapshot.acc_rc) {
                if (Math.abs(Math.abs(snapshot.curbal - snapshot.riskLimit) * price) < 5 || snapshot.size > 0) {
                    let host = await strategySwitchService.SwitcByPb(exchange, symbol, account, 1)
                    launch_buffer[host] = {
                        'time': Date.now(),
                        'relaunch': true
                    }
                    return;
                }
            }
            let comm = snapshot['is_fut'] === true ? parseFloat(snapshot[comm_mapping['fut']]) : parseFloat(snapshot[comm_mapping['spot']]);
            let asset = symbol.slice(0,3)
            if (assetSwitch.sensitive_assets.includes(asset) && (comm > 280 || (comm > 280 && acc_rebate[account] > 300))) {
                let success = await assetSwitch.smart_asset_switch(account, symbol.slice(0,3));
                if (success) {
                    let host = host_mapping(account, exchange, userConfig, apLogger);
                    launch_buffer[host] = {
                        'time': Date.now(),
                        'relaunch': true
                    }
                    return;
                }

            }
        }
    }

    let updated_size = null;
    let net = snapshot['is_fut'] ? net_mapping['fut'] : net_mapping['spot']
    let [netPnl, lastPnl, accounts] = [Number(snapshot[net]), Number(snapshot['lastPnl']), []]
    let comm = snapshot['is_fut'] === true ? parseFloat(snapshot[comm_mapping['fut']]) : parseFloat(snapshot[comm_mapping['spot']]);
    let [hourly_vol, hourly_ords, dealCnt] = [snapshot['turnover_in_hour'] || 0, snapshot['ordCnt_in_hour'] || 0, snapshot['dealCnt'] || 0];
    let mkt_share = snapshot['cashvol_pct'] / 100 || 0;   //dic['turnover_in_hour'] / dic['turnover_hourly']
    let is_router = await strategyRouterService.is_router(account);
    if (is_router) {
        let sub_snaps = snapshot.members;
        let accounts = Object.keys(sub_snaps);
        for (let sub_acc of accounts) {
            let router_sub_sym = `${sub_acc}#${symbol}`;
            let sub_snap = sub_snaps[sub_acc]
            if (['BG', 'MC', 'CW', 'WX', 'DC', 'LB', 'EX'].includes(exchange.slice(0,2))) {
                hourly_turnovers = {}
                hourly_ordcnts = {}
                let [hourly_vol, hourly_ords, dealCnt] = [sub_snap['turnover_in_hour'] || 0 + hourly_turnovers[sub_acc] ||0,
                                                                            sub_snap['ordCnt_in_hour'] || 0 + hourly_ordcnts[sub_acc] ||0,
                                                                            sub_snap['dealCnt'] || 0];
                let mkt_share = sub_snap['cashvol_pct'] / 100 || 0 + mkt_share_sym_map[router_sub_sym] || 0;
                mkt_share_sym_map[router_sub_sym] = mkt_share;
                hourly_turnovers[sub_acc] = hourly_vol;
                hourly_ordcnts[sub_acc] = hourly_ords;
                if (dealt_router.has(sub_acc)) {
                    continue
                }
                //RISKCONTROL
                let rc = accountService.isAccountRC(sub_acc, pnl_summary);
                if (rc && sub_acc.startsWith('MCF')) {
                    apLogger.info(`MCF_BAN: ${sub_acc}: ${JSON.stringify(pnl_summary['account_detail']['mcfut'][sub_acc])}`);
                }
                let launcher_status = strategyRouterService.get_launcher_status_from_lucifer(sub_acc);
                if (launcher_status !== null) {
                    let summary_status = acc_rc_status[sub_acc];
                    let action_taken = await accountService.UpdateAccountRCStatus(sub_acc, rc, launcher_status, summary_status, true);
                    if (!action_taken) {
                        //COOLDOWN For Router Subacc
                        let rc_size = getRCSize(account, symbol, exchange, strategyName, rc, false, ap_size,
                            mkt_share, hourly_vol, hourly_ords, 0, 0, sub_snap);
                        if (rc_size === 1) {
                            await accountService.BanAccount(sub_acc);
                            apLogger.info(`COOLDOWN: ${sub_acc}: ${mkt_share}/${hourly_vol}/${hourly_ords}`);
                        } else if (rc_size > 1) {
                            await accountService.ActivateAccount(sub_acc);
                            apLogger.info(`REACTIVATE: ${sub_acc}: ${mkt_share}`);
                        }
                    }
                }
            }
            dealt_router.add(sub_acc);
        }
    } else {
        if (exchange.startsWith('MC') || exchange.startsWith('BG')) {
            let riskcontrol = accountService.isAccountRC(account, pnl_summary);
            let account_rc_status = acc_rc_status[account];

            updated_size = getRCSize(account, symbol, exchange, strategyName, riskcontrol, account_rc_status, ap_size,
                mkt_share, hourly_vol, hourly_ords, netPnl, comm, snapshot);
            if (account_rc_status !== riskcontrol) {
                if (snapshot['size'] !== 0) {
                    ApiConnector.update_acc_stats(account, riskcontrol).then((res) => {
                        apLogger.info(`ACC_RC Posted: ${account}#${riskcontrol}#${res}`)
                    });
                }
                let host = exchange.startsWith('BG') ? 'tkc1' : 'tka1'
                if (!(host in launch_buffer) || launch_buffer[host]['time'] < Date.now() - 1800*1000) {
                    if (!(host in launch_buffer)) {
                        launch_buffer[host] = {
                            'time': Date.now(),
                            'relaunch': false
                        }
                    }
                    if (riskcontrol) {
                        if (exchange === 'BGS123') { //BGS
                            launch_buffer[host]['relaunch'] = true;
                            riskLimitOpsService.transfer_account(account).then(success => {
                                apLogger.info(`Bgspot transfer Account: ${account}#${symbol} status: ${success}`);
                            })
                        }
                    }
                }
            }
        }
    }
    let email_texts = '';
    let time =  snapshot['is_fut'] ? time_mapping['fut'] : time_mapping['spot']
    let mid = snapshot['is_fut'] ? mid_mapping['fut'] : mid_mapping['spot']
    if (Date.now() - parseInt(time) > 3.6e+6) {
        apLogger.info('PASS: Newly changed strategy')
        return undefined
    }
    // reopen at midnight
    // if (Date.now() - new Date().setHours(0,0,0,0) < 30 * 1000 && exchange.startsWith('MCF') && old_size === 0) {
    //     if (['trbtf0', 'injtf0', 'wldtf0', 'lnktf0'].includes(symbol)) {
    //         // updated_size = ap_size;
    //         apLogger.info(`REOPEN: ${account}#${symbol}#${strategyName} opened to ${updated_size}`)
    //     }
    // }
    if (snapshot['dv_rc'] === true) {
        updated_size = 0
        apLogger.info(`DV_RC_${account}#${symbol}#${snapshot['dv_rc_last_ms']}`)
    }
    // else if (snapshot['size'] === 0
    //         && Date.now() - snapshot['dv_rc_last_ms'] > 3*60*1000
    //         && Date.now() - snapshot['dv_rc_last_ms'] < 10*60*1000
    //         && snapshot['size_ms'] - snapshot['drawdown_rc_ms'] < 5*60*1000
    //         && netPnl > 0 && snapshot['eff'] > 10) { //dv_rc=false
    //     updated_size = Math.round(Number(ap_size) * 0.6);
    //     apLogger.info(`DV_REOPEN_${account}#${symbol}#${strategyName}#${updated_size}#${netPnl}`);
    // }
    if (snapshot['drawdown_rc_ms'] > 0 //frozen until time
        && Date.now() - snapshot['drawdown_rc_ms'] > 2*60*1000
        && snapshot['size_ms'] - snapshot['drawdown_rc_ms'] < 2*60*1000 && netPnl > 0) {
        if (lastPnl < -10) {
            updated_size = Math.round(old_size * 0.8);
        } else if (old_size > 1) {
            updated_size = old_size+1;
        }
        apLogger.info(`HARD_FROZEN_REOPEN_${account}#${symbol}${strategyName}#${updated_size}#${netPnl}${lastPnl}`);
    }

    let face_value = get_face_value(old_size, meta, symbol, account, snapshot[mid], snapshot['lastTradePrice'], apLogger)
    account in acc_notional ? acc_notional[account] += face_value :acc_notional[account] = face_value
    if (updated_size === null && old_size > 1) {
        updated_size = get_newSize(account, exchange, symbol, strategyName, config, meta, snapshot, netPnl, lastPnl, mid, database);

        if (updated_size === 0 && (exchange === 'MCF' || exchange === 'CWF' || exchange === 'DCF' || exchange === 'XTF')) {
            if (netPnl >= -10 && Math.abs(lastPnl) < netPnl) { //in case of v shape pnl
                updated_size = snapshot['size'];
            }
        }
        //kill low eff strategy
        if (snapshot['eff'] < 9) {
            let ordCnt = 0;
            for (let m of Object.keys(snapshot['members'])) {
                ordCnt += snapshot['members'][m]['ordCnt'];
            }
            if (ordCnt > 2000) {
                apLogger.info(`LOWEFF_SHUTDOWN,${account}#${symbol}#${strategyName}: ${ordCnt} ordcnts and ${snapshot['eff']} eff`);
                updated_size = 0;
            } else if (exchange === 'BGF' && snapshot['eff'] < 2 && ordCnt > 800) {
                apLogger.info(`LOWEFF_SHUTDOWN,${account}#${symbol}#${strategyName}: ${ordCnt} ordcnts and ${snapshot['eff']} eff`);
                updated_size = 0;
            } else if (exchange !== 'BGF' && snapshot['eff'] < 5 && ordCnt > 300) {
                apLogger.info(`LOWEFF_SHUTDOWN,${account}#${symbol}#${strategyName}: ${ordCnt} ordcnts and ${snapshot['eff']} eff`);
                updated_size = 0;
            }
        }

    }
    if (updated_size != null) {
        if (snapshot['lastTradePrice'] === 0 && exchange !== 'BGS' && !exchange.startsWith('MC')) { //prevent accidental shutdown or false increase
            updated_size = old_size
        } else {
            if (symbol !== 'BNBUSD') {
                if (updated_size === 0 && old_size > 1) {
                    email_texts += `\n SHUTDOWN ${account}#${symbol}#${strategyName}  \
                                            \n \t --- net: ${snapshot[net].toFixed(2)}, last: ${lastPnl}`
                } else if (lastPnl < config[0]['TOLERANCE']) {
                    email_texts += `\n LARGE LOSS ${account}#${symbol}#${strategyName} \
                                            \n \t --- net: ${snapshot[net].toFixed(2)}, last: ${lastPnl} \
                                            \n \t --- size changed from ${old_size} to ${updated_size}`
                } else if (updated_size <= config[1]['usual'] * config[1]['min'] && snapshot[net] < -100 && old_size !== updated_size) {
                    email_texts += `\n HITTING MIN ${account}#${symbol}#${strategyName} \
                                            \n \t --- net: ${snapshot[net].toFixed(2)}, last: ${lastPnl} \
                                            \n \t --- size changed from ${old_size} to ${updated_size}`
                }
                if (email_texts !== '') {
                    email_texts += `\n SNAPSHOT: ${JSON.stringify(snapshot)}`
                }
            }
        }
    } else {
        return undefined
    }

    // let updated_size = extracted[0]
    // let email_texts = extracted[1]
    if (updated_size === 0 && old_size > 1) {
        if ( (symbol !== symbol.toUpperCase()) ) {
            apLogger.info('future does not change strategy')
        } else {
            if (symbol !== 'BNBUSD' && ! making_accs.includes(account) && acc_rc_status[account] !== true) {
                let host = host_mapping(account, exchange, userConfig, apLogger);
                let change_info = {'ex': exchange, 'sym': symbol,'acc': account, 'sid': sid, 'strat': strategyName, 'size': old_size, 'lastPnl': lastPnl};
                let acc_key = `${account}#${symbol}`;
                let match_p = 'CRYPTO_SNAP[RS][^M]*' + acc_key + '*';
                let this_acc_sym_strats = await get_keys(match_p).catch((err) => { apLogger.info(err)});
                database[acc_key] = this_acc_sym_strats.length;

                database['lines'] ? (database['lines'][host] ? (database['lines'][host][acc_key] = change_info) :
                    database['lines'][host] = {}, database['lines'][host][acc_key] = change_info) :
                    (database['lines'] = {}, database['lines'][host] = {}, database['lines'][host][acc_key] = change_info)
            }
        }
        return [snapshot, old_size, updated_size, lastPnl, [], email_texts, null]
    } else {
        return [snapshot, old_size, updated_size, lastPnl, [], email_texts, null]
    }
}

async function update_sizes(key_pattern, userConfig, meta) {
    let now = new Date();
    let now_epoch = Date.now();
    let database = {
        "time_posted": false
    };
    fs.readFile(ap_db, (err, data) => {
        if (err) {
            apLogger.info(`use default database in dbService`);
        } else {
            database = JSON.parse(data);
        }

    })
    await strategyLuciferService.LoadAllRunningStrategy();
    pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { apLogger.info(err)})
    for (let ex in pnl_summary['account_detail']) {
        for (let acc in pnl_summary['account_detail'][ex]) {
            acc_ordcnt[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['ordCnt'] : 0;
            acc_dealcnt[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['dealCnt'] : 0;
            acc_rebate[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['tRbt'] : 0;
            acc_ttnov[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['ttnov'] : 0;
            acc_tvol[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['turnover'] : 0;
            acc_rc_status[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['riskcontrol'] : false;
        }
    }
    let crypto_keys = await get_keys(key_pattern).catch((err) => { apLogger.info(err)})
    apLogger.info('Time Now: ', now, now_epoch, 'keys obtained: ', crypto_keys.length)
    let all_texts = '';
    let need_relaunch = [];
    let k_i = 0;
    let apRecCfg = accountService.get_apRecCfg();
    for (; k_i< crypto_keys.length; k_i++) {
        let key = crypto_keys[k_i]
        if (key.endsWith("NotFound00000")) {
            continue
        }
        try {
            const key_splits = key.split("#");
            const account = key_splits[1];
            const symbol = key_splits[2];
            let strategyName = key_splits[3];
            let router_sub = account;
            if (dealt_router.has(router_sub)) {
                continue
            }
            const exchange = get_ex(account);
            let sym_k = exchange + '#' + symbol
            let sym_k_changed = sym_k + '#changed'
            let online_sym_k = sym_k + '#online'
            if (! database[online_sym_k] || database[sym_k_changed]) {
                let [accounts_alive, sym_k_info, inactive_info] = await get_num_accs(exchange, symbol, apLogger).catch((err) => apLogger.info(err))
                database[online_sym_k] = accounts_alive
                database[sym_k] = sym_k_info
                database[sym_k_changed] = false
            }

            // skip OK
            // if (exchange.startsWith('GT')) {continue}
            let config = apRecCfg[`${exchange}#${symbol}`]
            if (!config) {
                let cfg_key = `${exchange}#${symbol}`
                config = await getRedisJson(cfg_key, apLogger).catch((err) => apLogger.info(err))
                console.log('getting ap config from redis', cfg_key);
                await accountService.LoadAllRecCfg();
            }
            let values = await loop_key(key, account, symbol, strategyName, exchange, userConfig, config, meta, database)
            if (values !== undefined) {
                if (database['time_posted'] === false) {
                    apLogger.info('Time Now: ', now, now_epoch)
                    database['time_posted'] = true
                }
                let snap = values[0]
                let old_size = values[1]
                let updated_size = values[2]
                let lastPnl = values[3]
                let relaunches = values[4]
                let email_texts = values[5]
                let changed_straName = values[6]
                if (relaunches.length !== 0) {
                    need_relaunch = need_relaunch.concat(relaunches)
                }
                if (changed_straName !== null) {
                    strategyName = changed_straName
                }
                if (old_size !== updated_size || (updated_size === 0 && old_size === 0 && Number(lastPnl) !== 0)) { //|| (updated_size === 0 && Number(lastPnl) !== 0)
                    let mid = mid_mapping['spot']
                    let net = net_mapping['spot']
                    if (snap['is_fut'] === true) {
                        mid = mid_mapping['fut']
                        net = net_mapping['fut']
                    }
                    if (old_size === 0 && updated_size === 0) {
                        email_texts = `ERROR FAILED TO SHUTDOWN ${key} \n` + email_texts
                    }
                    post_newsize(userConfig['Url'], userConfig['Token'], account, symbol,
                        strategyName, old_size, updated_size, lastPnl, snap[net], userConfig['User'], snap, true)

                    // power nap            // in case manual close and reopen by ap
                    if (updated_size ===0 && old_size !== 0 && symbol !== symbol.toUpperCase()) { //  dealt with in power_nap
                        //
                        if (making_accs.includes(account) || ['BG', 'MC', 'CW', 'WX', 'DC'].includes(account.slice(0,2))) {
                            continue
                        } else {
                            let napped = await power_nap(database, account, symbol, strategyName, snap)
                            apLogger.info('napped3: ', napped)
                        }
                    }
                }
                all_texts += email_texts
            }
        } catch(err) {
            all_texts += `KEY ERROR: ${key}, skipped with msg: ${err.message} and stack: ${err.stack}`
        }
    }
    if (k_i < 50) {
        if (k_i === 0) {
            all_texts = `ERROR: NO keys were obtained from pattern \n` + all_texts
            apLogger.info(`ERROR: NO keys were obtained from pattern`)
        } else {
            all_texts = `ERROR: ${crypto_keys[k_i]} ended early \n` + all_texts
            apLogger.info(`ERROR: ${crypto_keys[k_i]} ended early`)
        }
        database = {
            "time_posted": false
        };
    }
    if (now.getHours() === 17 && now.getMinutes() === 30 && now.getSeconds() > 30) {  //rewrite databse at 5:30 pm
        database = {
            "time_posted": false
        };
    }
    return [all_texts, need_relaunch, database]
}


function post_newsize(url, token, account, symbol, strategyName, old_size, updated_size, lastPnl, netPnl, user, snap, force) {
    if (old_size === 1 && updated_size === 0 && (lastPnl > -5 || (lastPnl > -50 && account.startsWith('BGS')))) {
        apLogger.info(`SKIP_FALSE_SHUT ${account}#${symbol}`);
        return old_size
    }
    if (Math.abs(updated_size-old_size)/old_size > 0.5 && old_size !==0 && old_size > 10) {
        apLogger.info(`WEIRD SIZE UPDATE:  ${account}#${symbol}#${strategyName} changed from ${old_size} to ${updated_size} with last PNL: ${lastPnl} and last net: ${netPnl} and snap ${JSON.stringify(snap)}`)
    }
    if (updated_size && isInt(Number(updated_size)) || updated_size === 0) {
        request.post({
            url: url + '/strategy?jwt_token=' + token,
            form: {'account': account, 'symCont': symbol,
                'strategyName': strategyName, 'size_before': old_size, 'size_after': updated_size, 'user': user, 'force': force}
        }, function(error, response, body) {
            if (!error && response.statusCode ===200) {
                apLogger.info(`POST Successfully Sent: ${account}#${symbol}#${strategyName} changed from ${old_size} to ${updated_size} with lastPNL: ${lastPnl} and last net: ${netPnl}`)
                return 1
            } else　{
                apLogger.info(`POST FAILED ${error}`)
                return 0
            }
        })
    }
}

async function post_newsize_async(url, token, account, symbol, strategyName, old_size, updated_size, lastPnl, netPnl, user, force) {
    if (Math.abs(updated_size-old_size)/old_size > 0.5 && old_size !==0 && old_size > 10) {
        apLogger.info(`WEIRD SIZE UPDATE:  ${account}#${symbol}#${strategyName} changed from ${old_size} to ${updated_size} with last PNL: ${lastPnl} and last net: ${netPnl}`)
    }
    const getRes =  new Promise( (resolve, reject) => {
        if (updated_size && isInt(Number(updated_size)) || updated_size === 0) {
            request.post({
                url: url + '/strategy?jwt_token=' + token,
                form: {'account': account, 'symCont': symbol,
                    'strategyName': strategyName, 'size_before': old_size, 'size_after': updated_size, 'user': user, 'force': force}
            }, function(error, response, body) {
                if (!error && response.statusCode ===200) {
                    apLogger.info(`POST Successfully Sent: ${account}#${symbol}#${strategyName} changed from ${old_size} to ${updated_size} with lastPNL: ${lastPnl} and last net: ${netPnl}`)
                    resolve(1)
                } else　{
                    apLogger.info(`POST FAILED ${error}`)
                    resolve(0)
                }
            })
        } else {
            apLogger.info('updated size not valid')
            resolve(0)
        }
    })
    let post_res = await getRes.catch((err) => {apLogger.info(err)})
    return post_res
}


async function power_nap(database, account, symbol, strategyName, snap) {
    let shutdown_key = `${account}#${symbol}#${strategyName}`
    let alert = `${shutdown_key}#warning`
    let redis_key = `CRYPTO_SNAP[RS][^M]*${shutdown_key}`
    if (! snap) {
        snap = await getRedisJson(redis_key, apLogger)
    }
    let mid = mid_mapping['spot']
    let net = net_mapping['spot']
    if (snap['is_fut'] === true) {
        mid = mid_mapping['fut']
        net = net_mapping['fut']
    }
    delete database[alert]

    if (database['shutdowns']) {
        if (database['shutdowns'][shutdown_key]) {
            let last_shut_var = 'nice';
            let bad_shutdown_var = 0;
            if (Math.abs(Number(snap[net]) - Number(database['shutdowns'][shutdown_key]['last_shutdown_pnl'])) <  0.001){
                return false
            }
            if (Number(snap[net]) - Number(database['shutdowns'][shutdown_key]['last_shutdown_pnl']) < 0) {
                last_shut_var = 'bad'
                bad_shutdown_var = 1
            }
            database['shutdowns'][shutdown_key]['cnt'] += 1;
            database['shutdowns'][shutdown_key]['last_time'] = Date.now();
            database['shutdowns'][shutdown_key]['zero'] = true;
            database['shutdowns'][shutdown_key]['last'] = snap['lastTradePrice'];
            database['shutdowns'][shutdown_key]['last_shutdown_pnl'] = snap[net];
            database['shutdowns'][shutdown_key]['last_shut'] = last_shut_var;
            database['shutdowns'][shutdown_key]['bad_shutdowns'] += bad_shutdown_var;
        } else {
            database['shutdowns'][shutdown_key] = {'cnt': 1, 'last_time': Date.now(), 'zero': true,
                'mid': snap[mid], 'last': snap['lastTradePrice'], 'is_fut': snap['is_fut'],
                'last_shutdown_pnl': snap[net], 'last_shut': 'nice', 'bad_shutdowns': 0}
        }
    } else {
        database['shutdowns'] = {};
        database['shutdowns'][shutdown_key] = {'cnt': 1, 'last_time': Date.now(), 'zero': true,
            'mid': snap[mid], 'last': snap['lastTradePrice'], 'is_fut': snap['is_fut'],
            'last_shutdown_pnl': snap[net], 'last_shut': 'nice', 'bad_shutdowns': 0}
    }
    apLogger.info('shutdown here', shutdown_key, JSON.stringify(database['shutdowns'][shutdown_key]))

    if (! snap) {
        return false
    } else {
        return true
    }
    // did database really change???
}

module.exports = {update: update_sizes, alert: alert, reset_acc: reset_acc} // even: even_sizes,
