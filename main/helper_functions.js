const request = require('request');
const ApiConnector = require("../utils/api_connector");
const get_methods = require('../utils/get_methods');

function host_mapping(account, exchange, userConfig, logger) {
    let host = userConfig['Hosts'][exchange]
    let ending = account.slice(-1)
    if (exchange === 'KCF') {
        ending = account.slice(-2)
        if (['01', '02', '03', '04', '05', '06'].includes(ending)) {
            host = userConfig['Hosts']['KCF1']
        } else {
            host = userConfig['Hosts']['KCF2']
        }
        logger.info(account, ' ending with ', ending)
    }
    if (exchange === 'BNBF') {
        ending = account.slice(-2)
        if (['01', '02', '03', '11', '21', '22', '23'].includes(ending)) {
            host = userConfig['Hosts']['BNBF1']
        } else if (['04', '05', '06', '07', '12', '13', '24', '25', '26'].includes(ending)) {
            host = userConfig['Hosts']['BNBF1']
        } else {
            host = userConfig['Hosts']['BNBF2']
        }
        logger.info(account, ' ending with ', ending)
    }
    return host
}

function isInt(n) {
    return Number(n) === n && n % 1 === 0
}

function isFloat(n) {
    return Number(n) === n && n % 1 !== 0
}

// async function getRedisJson(key) {
//     const getRJ =  new Promise( (resolve, reject) => {
//         redisConn.get(key, function(err, res) {
//             if (err) {
//                 reject(`ERROR: ${key} NOT FOUND IN REDIS`)
//             } else if (res) {
//                 resolve(JSON.parse(res));
//             } else {
//                 if (key.startsWith('AP_REC')) {
//                     let [_, exchange, symbol] = key.split('#')
//                     let save_info = JSON.stringify(local_config[exchange][symbol])
//                     if (save_info) {
//                         redisConn.set(key, save_info, function(err, res) {
//                             if (err) {
//                                 console.log(`FAILED TO SAVE ${key} IN REDIS`)
//                             }
//                         })
//                     } else {
//                         console.log(`save info undefined for REDIS: ${exchange}-${symbol}`)
//                     }
//                 }
//                 reject(`EMPTY VALUE IN ${key} with val: ${res}`)
//             }
//
//         })
//     })
//     let redisRes = await getRJ.catch((err) => {console.log(err)})
//     return redisRes
// }


async function change_preference(redisConn, exchange, symbol, new_preference) {
    let key_pattern = `AP_REC#${exchange}#*`
    if (symbol !== 'all') {
        key_pattern = `AP_REC#${exchange}#${symbol}`
    }
    const get_keys = new Promise((resolve, reject) => {
        redisConn.keys(key_pattern, function(err, crypto_keys){
            if (!err) {
                resolve(crypto_keys)
            } else {
                reject(`ERROR: pattern ${key_pattern} not found in redis`)
            }
        })
    })
    let crypto_keys = await get_keys.catch((err) => { console.log(err)})
    for (let cfg_key of crypto_keys) {
        let config = await get_methods.getRedisJson(cfg_key).catch((err) => console.log(err))
        for (let action of config) {
            action['preference'] = new_preference
        }
        redisConn.set(cfg_key, JSON.stringify(config), function(err, res) {
            if (err) {
                console.log(`FAILED TO RESET PREFERENCE ${cfg_key} ${JSON.stringify(config)} IN REDIS`)
            } else {
                console.log(` ${cfg_key} reset preference to ${new_preference}`)
            }
        })
    }
}


function change_size(userConfig, account, symbol, strategyName, old_size, updated_size, lastPnl, netPnl, force, logger) {
    if (Math.abs(updated_size-old_size)/old_size > 0.5 && old_size !==0 && old_size > 10) {
        logger.info(`WEIRD SIZE UPDATE:  ${account}#${symbol}#${strategyName} changed from ${old_size} to ${updated_size} with last PNL: ${lastPnl} and last net: ${netPnl}`)
    }
    if (updated_size && isInt(Number(updated_size)) || updated_size === 0) {
        request.post({
            url: userConfig['Url'] + '/strategy?jwt_token=' + userConfig['Token'],
            form: {'account': account, 'symCont': symbol,
                'strategyName': strategyName, 'size_before': old_size, 'size_after': updated_size, 'user': userConfig['User'], 'force': force}
        }, function(error, response, body) {
            if (!error && response.statusCode ===200) {
                logger.info(`POST Successfully Sent: ${account}#${symbol}#${strategyName} changed from ${old_size} to ${updated_size} with lastPNL: ${lastPnl} and last net: ${netPnl}`)
                return 1
            } else　{
                logger.info(`POST FAILED ${error}`)
                return 0
            }
        })
    }
}

async function change_dss(redisConn, exchange, symbol, new_dss) {
    let key_pattern = `AP_REC#${exchange}#*`
    if (symbol !== 'all') {
        key_pattern = `AP_REC#${exchange}#${symbol}`
    }
    const get_keys = new Promise((resolve, reject) => {
        redisConn.keys(key_pattern, function(err, crypto_keys){
            if (!err) {
                resolve(crypto_keys)
            } else {
                reject(`ERROR: pattern ${key_pattern} not found in redis`)
            }
        })
    })
    let crypto_keys = await get_keys.catch((err) => { console.log(err)})
    for (let cfg_key of crypto_keys) {
        let config = await get_methods.getRedisJson(cfg_key).catch((err) => console.log(err))
        for (let action of config) {
            action['dss'] = new_dss
        }
        redisConn.set(cfg_key, JSON.stringify(config), function(err, res) {
            if (err) {
                console.log(`FAILED TO RESET PREFERENCE ${cfg_key} ${JSON.stringify(config)} IN REDIS`)
            } else {
                console.log(` ${cfg_key} reset dss to ${new_dss}`)
            }
        })
    }
}


async function change_strategy(userConfig, exchange, symbol, account, stype, sid,
                               strategyName, strategyNameAfter, strategyIdAfter, new_size,
                               need_relaunch, host_mapping, trigger, logger) {
    if (exchange.startsWith('BGS') || exchange.startsWith('MCS')) {
        stype = 711
        // let batch = strategyNameAfter.split('_')[0]
        // if (batch < '221222') {
        //     logger.info(`POST ERROR: BGS WRONG BATCH ${strategyNameAfter}`)
        //     return need_relaunch
        // }
    }
    let resp = await ApiConnector.update_strategy(account, symbol, strategyName, sid,'', new_size, strategyIdAfter, stype, trigger, false)
    if (resp.success) {
        let post_msg = `POST Strategy Successfully Sent: <${account}#${symbol}> changed from ${strategyName} to ${strategyNameAfter} with new size ${new_size}`
        logger.info(post_msg);
        let server = host_mapping(account, exchange, userConfig, logger)
        if (!need_relaunch.includes(server)) {
            need_relaunch.push(server)
        }
    } else {
        logger.info('fail msg: ', resp["msg"])
    }
    // const postNewStra =  new Promise((resolve, reject) => {
    //     request.post({
    //         url: userConfig['Url'] + '/strategy?jwt_token=' + userConfig['Token'],
    //         form: {'account': account, 'symCont': symbol, 'strategyName': strategyName, 'missileType': stype,
    //             'strategyIdAfter': strategyIdAfter, 'size_after': new_size, 'user': userConfig['User'], 'trigger': trigger}
    //     }, function(error, response, body) {
    //         if (!error && response.statusCode ===200) {
    //             let post_msg = `POST Strategy Successfully Sent: <${account}#${symbol}> changed from ${strategyName} to ${strategyNameAfter} with new size ${new_size}`
    //             logger.info(post_msg);
    //             body = JSON.parse(body);
    //             if (body["success"] === true) {
    //                 let server = host_mapping(account, exchange, userConfig, logger)
    //                 if (!need_relaunch.includes(server)) {
    //                     need_relaunch.push(server)
    //                 }
    //             } else {
    //                 logger.info('body msg: ', body["msg"])
    //             }
    //             resolve('yes')
    //         } else　{
    //             logger.info(`POST ERROR: ${error}`)
    //         }
    //     })
    // })
    // let msg = await postNewStra
    return need_relaunch
}

module.exports = {
    change_size,
    change_preference,
    change_dss,
    change_strategy,
    host_mapping,
    isInt,
    isFloat} // even: even_sizes,