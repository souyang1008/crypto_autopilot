const strategyRuntimeSerive = require("../services/StrategyRuntimeService");
const sendMail = require('../utils/mail');
const mailer = require("../utils/mail");
const request = require('request');
const {get_avg_size, get_avg_pnl, get_cumpnl} = require("../utils/get_methods");


async function list_tops(n=undefined) {
    let tops = {'time': Date()};
    let ranks = await strategyRuntimeSerive.getAllRanks();
    for (let ex in ranks) {
        let ex_tops = [];
        if (ex === 'weexl') { ex = 'weex'}
        for (let sym in ranks[ex]) {
            let info = ranks[ex][sym];
            let rank_no1 = {};
            if (info && info.length > 0) {
                for (let r of info) {
                    if (r['deals'] > 10) {
                        rank_no1 = r;
                        break;
                    }
                    if (ex === 'gate' && r['deals'] > 50) {
                        rank_no1 = r;
                        break;
                    }
                }
            }
            if (Object.keys(rank_no1).length > 0) {
                let avg_size = await get_avg_size(ex.toUpperCase(), sym);
                let avg_pnl = await get_cumpnl(ex.toUpperCase(), sym);
                if (avg_size === 0) {
                    if (avg_pnl === 0) {
                        avg_size = -1;
                    }
                }
                let push_item = {
                    'symbol': sym,
                    'raw_ir': ['mexc', 'bitget', 'coinw', 'weex', 'deepcoin', 'xt', 'lbank', 'ex3', 'hotcoin', 'bitmart'].includes(ex) ? Number(rank_no1['raw_score']).toFixed(2) : Number(rank_no1['ir']).toFixed(2) ,
                    'ir': ['mexc', 'bitget', 'coinw', 'weex', 'deepcoin', 'xt', 'lbank', 'ex3', 'hotcoin', 'bitmart'].includes(ex)  ? Number(rank_no1['ir']).toFixed(2) : Number(rank_no1['new_ir']).toFixed(2) ,
                    'size': avg_size,
                    'sid': rank_no1['id'],
                    'name': rank_no1['name'],
                    'deals': rank_no1['deals']
                }
                if (avg_size > 0) {
                    push_item['pnl'] = avg_pnl;
                }
                ex_tops.push(push_item);
            }
        }
        let [spot, fut] = ex_tops.reduce((res, l) => {
            res[l.symbol===l.symbol.toUpperCase() ? 0: 1].push(l);
            return res;
        },
            [[], []]);
        for (let arr of [fut, spot]) {
            if (arr.length > 0) {
                arr = arr.sort((a, b) => {
                    return b.raw_ir - a.raw_ir
                })
                let low_ex = ['ex3', 'hotcoin', 'bitget', 'bitmart'];
                low_ex.includes(ex) ? arr = arr.filter(l => Number(l.raw_ir) > 0.1): arr = arr.filter(l => Number(l.raw_ir) > 2);
                let low_deal_bound = 1500;
                if (ex==='coinw') {
                    low_deal_bound = 3000;
                }
                let arr2 = arr.filter(l => Number(l.deals) < low_deal_bound);
                if (n) {
                    arr = arr.slice(0,n);
                    arr2 = arr2.slice(0, n)
                }
                tops[ex] = arr
                tops[ex+'_lowdeal'] = arr2;
                ex += '_spot'
            }
        }
    }
    request.post({
        url: "http://47.57.5.75:28888/playback/tops",
        json: tops,
        headers: {'content-type': 'application/json'}
    });
    return tops;
}

list_tops(n=10).then((tops) => {
    console.log("done");
    console.log(tops);
});

module.exports = {list_tops: list_tops}