const request = require('request');
const fs = require('fs');
const ap_db = "/data/crypto_autopilot_logs/ap_db.json";
// const yaml = require('js-yaml');
const {midLogger, apLogger} = require('../services/LogService');
const {host_mapping, change_strategy} = require('./helper_functions');
const {ex_mapping, net_mapping, time_mapping, mid_mapping, making_accs,
    get_avg_pnl, get_avg_size, get_keys, get_symbol_rank, get_lowest_pb_strategy,
    get_dw_key, get_face_value, get_num_accs, get_size_from_face_value, get_pb_rank, get_pnl_rank, getRedisJson, get_ex} = require('../utils/get_methods');
const strategyRuntimeSerive = require("../services/StrategyRuntimeService");
const ApiConnector = require("../utils/api_connector");
const accountService = require("../services/AccountService");

function is_top2s_untried(pb_list, alive_cnt) {
    let [top1, top2] = [pb_list[0] || {'batch_tried': -1}, pb_list[1] || {'batch_tried': -1}]
    if (alive_cnt === 1) {
        return top1['batch_tried'] === 0
    } else {
        return top1['batch_tried'] === 0 || top2['batch_tried'] === 0;
    }

}

function filter_pb_list(pb_list) {
    let filtered_list = []
    let back_up = undefined;
    for (let pb of pb_list) {
        if (pb['tried_times'] === 0) {
            filtered_list.push(pb);
        } else if (!back_up && pb['tried_times'] < 2) {
            back_up = pb;
        }
    }
    if (filtered_list.length === 0) {
        filtered_list.push(back_up);
    }
    return filtered_list;
}

// let alert = account + '#' + symbol + '#' + strategyName + '#' +'warning'
// let last_5_cumpnl = 0
// try {
//     last_5_cumpnl = database[alert]['past_pnls'].slice(-1)[0] - database[alert]['past_pnls'].slice(-5)[0]
// } catch(err) {
//     last_5_cumpnl = 0
// }

// need to change strategies that ran about the same length of time but low pnl (API:get a list of runtime of each running strategy)
// else if (old_net < avg_pnl_threshold && avg_pnl_threshold > 0 && time_past > 3600 *1000) {
//     met_cond = 'lower than avgPNL'
// }
//if (avg_size === 0) {
//             avg_size = get_size_from_face_value(meta, symbol, exchange, snapshot[mid], snapshot['lastTradePrice'], 15, midLogger)
//         }
//    let pnl_list = await get_pnl_rank(exchange ,symbol, userConfig)
//key, account, symbol, avg_size, avg_pnl, strategyName, exchange, pb_list,
//                         userConfig, config, meta, database
// async function  loop_key(snap_key, snapshot, pb_candidates, switch_cnt){
//     let key_splits = snap_key.split('#');
//     let [account, symbol, strategyName] = [key_splits[1], key_splits[2], key_splits[3]];
//     let exchange = get_ex(account);
//     const old_size = parseInt(snapshot['size'])
//     let net = snapshot['is_fut'] ? net_mapping['fut'] : net_mapping['spot']
//     let time = snapshot['is_fut'] ? time_mapping['fut'] : time_mapping['spot']
//     let mid = snapshot['is_fut'] ? mid_mapping['fut'] : mid_mapping['spot']
//     const old_net = parseInt(snapshot[net])
//
//     let stype = snapshot['stype']
//     // let face_value = get_face_value(old_size, meta, symbol, account, snapshot[mid], snapshot['lastTradePrice'], midLogger)
//     let time_past = Date.now() - snapshot[time] // wrong time
//     let snap_info = {
//         'mid': snapshot[mid],
//         'last': snapshot['lastTradePrice'],
//         'key': `${account}-${symbol}`,
//         'size': old_size,
//         'net': snapshot[net]
//     }
// }

async function update_strategies(redisConn, userConfig, meta) {
    let database = {
        "time_posted": false
    };
    fs.readFile(ap_db, (err, data) => {
        if (err) {
            midLogger.info(`use default database in dbService`);
        } else {
            database = JSON.parse(data);
        }

    })
    let all_actions = {}
    let all_texts = '';
    let apRecCfg = accountService.get_apRecCfg();
    let threshold_key = 'AP_REC#THRESHOLD_INFO'
    const thresholdInfo = apRecCfg[threshold_key];
    let pnl_summary = await strategyRuntimeSerive.getLatestPnl().catch((err) => { apLogger.info(err)})
    let acc_rc_status = {}
    for (let ex in pnl_summary['account_detail']) {
        for (let acc in pnl_summary['account_detail'][ex]) {
            acc_rc_status[acc] = pnl_summary['account_detail'][ex][acc] ? pnl_summary['account_detail'][ex][acc]['riskcontrol'] : false;
        }
    }
    let all_snasp_pattern = `CRYPTO_SNAP[SR][^M]*`;
    let snaps = await get_keys(all_snasp_pattern, midLogger).catch((err) => { midLogger.info(err)});
    snaps = snaps.filter(snap => !snap.endsWith("NotFound00000") && !snap.endsWith("PASSTHROUGH"));
    let symbols = await get_keys('AP_REC#*', midLogger).catch((err) => { midLogger.info(err)})
    symbols = symbols.filter(sym => sym.includes('MCF') || sym.includes('CWF'));
    symbols.sort();
    // midLogger.info('getting symbols: ', symbols)
    let msg = ''
    for (let s_i = 0; s_i < symbols.length; s_i ++) {
        const cfg_key = symbols[s_i]
        const ap_rec_key = cfg_key.split("#");
        if (ap_rec_key.length !== 3) {
            midLogger.info('skip invalid ap_rec_key: ', cfg_key)
            continue
        }
        const acc_prefix = ap_rec_key[1]
        const symbol = ap_rec_key[2]
        let exName = ex_mapping[acc_prefix].toUpperCase();
        if (!['MEXC', 'COINW'].includes(exName)){
            continue
        }
        // let key_p1 = new RegExp(`CRYPTO_SNAPSHOT#${acc_prefix}.*#${symbol}.*`);
        let key_p2 = new RegExp(`CRYPTO_SNAPR#${acc_prefix}.*#${symbol}.*`);
        let sym_snaps = snaps.filter(snap => key_p2.test(snap) ); //|| key_p1.test(snap)
        if (sym_snaps.length > 0) {
            console.log('symbol in loop', acc_prefix, symbol)
            let need_switch_snaps = {};
            let avg_size = await get_avg_size(exName, symbol);
            // let avg_pnl = await get_avg_pnl(exName, symbol);
            let config = apRecCfg[`${acc_prefix}#${symbol}`]
            if (!config) {
                config = await getRedisJson(cfg_key, midLogger).catch((err) => midLogger.info(err))
                console.log('getting ap config from redis', cfg_key);
                await accountService.LoadAllRecCfg();
            }
            let pb_list = await get_pb_rank(acc_prefix, symbol, userConfig, config[1]['preference'] === 'm', 'algo', midLogger);
            let [alive_cnt, [off_strategies, last_strategy_offtime], inactive] = await get_num_accs(acc_prefix, symbol, midLogger);
            let load_free = off_strategies <= Math.max(2, alive_cnt);
            let pb_untried = is_top2s_untried(pb_list, alive_cnt);
            let alive_unaction_snaps = [];
            let snap_info_map = {}
            for (let k_i = 0; k_i< sym_snaps.length; k_i++) {
                let key = sym_snaps[k_i];
                let account = key.split("#")[1];
                if (making_accs.includes(account) || acc_rc_status[account]) {
                    // skip making skip riskcontrol
                    continue;
                }
                let snapshot = await getRedisJson(key, midLogger)
                if (snapshot === undefined || snapshot['enabled'] !== true) {
                    continue;
                }
                let old_size = parseInt(snapshot['size']);
                let net = snapshot['is_fut'] ? net_mapping['fut'] : net_mapping['spot'];
                if (old_size === 0 && snapshot[net] !== 0) {
                    need_switch_snaps[key] = 'closed';
                } else {
                    alive_unaction_snaps.push(key);
                }
                snap_info_map[key] = snapshot;
                // let values = await loop_key(key, account, symbol, avg_size, avg_pnl, strategyName, pb_list,
                //     userConfig, config, meta, database);
                // if (values !== undefined) {
                //     if (values.length === 10) {
                //         values = values.slice(0,-1)
                //         last_card += 1
                //     }
                // }
            }
            let pb_score_map = await get_symbol_rank(exName.toLowerCase(), symbol);
            if (load_free && pb_untried) {
                if (alive_unaction_snaps.length > 0) {
                    let action_snap = get_lowest_pb_strategy(alive_unaction_snaps, pb_score_map);
                    need_switch_snaps[action_snap] = 'pb_untried';
                } else {
                    for (let k in need_switch_snaps) {
                        need_switch_snaps[k] += '/pb_untried';
                    }
                }
            }

            if (Object.keys(need_switch_snaps).length > 0) {
                let pb_candidates= filter_pb_list(pb_list);
                let switch_cnts = 0
                for (let snap_key in need_switch_snaps) {
                    let met_cond = need_switch_snaps[snap_key];
                    let candidate = pb_candidates[switch_cnts];
                    if (candidate) {
                        let strat = snap_key.split('#')[3].split('_').slice(0,3).join('_');
                        let this_pb_score = pb_score_map[strat];
                        if (candidate.ir > this_pb_score * 1.1 && candidate.raw_score > 6) {
                            let snap = snap_info_map[snap_key];
                            let skey = candidate['key'];
                            let pnl_id = get_dw_key(skey, acc_prefix, 'id')
                            let pnl_name = get_dw_key(skey, acc_prefix, 'name')
                            let action_info = [snap, acc_prefix, symbol, snap['account'], snap['stype'], avg_size, snap['strategy'], pnl_id, pnl_name];
                            msg += `\n [SUCCESS]COND:${met_cond}: ${snap['account']}-${symbol} changed from ${snap['strategy']} to ${pnl_name} with IR ${candidate.ir}/${this_pb_score}/${candidate.raw_score} and switch_cnt: ${switch_cnts}`
                            switch_cnts += 1
                        } else {
                            msg += `\n [FAILED]COND:${met_cond}: ${snap_key} failed to change with candidate low IR ${JSON.stringify(candidate)} with this IR:${this_pb_score} and switch_cnt: ${switch_cnts}`
                        }
                    } else {
                        msg += `\n [FAILED]COND:${met_cond}: ${snap_key} failed to change with no candidate left: ${JSON.stringify(pb_list)} and switch_cnt: ${switch_cnts}`
                    }
                    // all_actions[exchange] ? all_actions[exchange].push(values) : all_actions[exchange] = [values];
                }
            }

        }
    }
    if (msg.length > 0) {
        console.log(msg);
    }

    // alternate code
    // let need_relaunch  = []
    // console.log('all actions: ', all_actions)
    // for (let exchange in all_actions) {
    //     for (let action of all_actions[exchange]) {
    //         let [snap_info, exchange, symbol, account, stype, avg_size, strategyName, strategyIdAfter, strategyNameAfter] = action
    //         if (strategyNameAfter.split('_')[0] < '230117' && exchange.startsWith('BG')) {
    //             console.log('skip changing old bg strats: ', action)
    //             continue;
    //         }
    //         if (strategyNameAfter === strategyName) {
    //             console.log('do not start shutdown strategy: ', action)
    //             continue;
    //         }
    //         let least_reopen_size = Math.max(1 ,
    //             get_size_from_face_value(meta, symbol, exchange, snap_info['mid'], snap_info['last'], 50, midLogger));
    //         let new_size = Math.ceil(avg_size * 0.6)
    //         new_size =  Math.ceil(Math.max(new_size, least_reopen_size))
    //         if (exchange === 'BGS') {
    //             new_size = await strategyRuntimeSerive.getApSize('BGS', symbol);
    //         }
    //         all_texts += `\n ${exchange}-${account}-${symbol} changed from ${strategyName} to ${strategyNameAfter} with size ${new_size}`
    //         need_relaunch = await change_strategy(userConfig, exchange, symbol, account, stype,
    //             strategyName, strategyNameAfter, strategyIdAfter, new_size, need_relaunch, host_mapping, 'mid_update', midLogger)
    //     }
    // }
    // await new Promise(resolve => setTimeout(resolve, 60*1000));
    //
    // for (let host of need_relaunch) {
    //     let res = await ApiConnector.relaunch(host);
    // }
    //
    // return [all_texts, JSON.stringify(all_actions)]
}

module.exports = {update_strategies: update_strategies}
